<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_websites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stripe_acc_id')->nullable();
            $table->string('name');
            $table->string('url');
            $table->string('api_consumer_key');
            $table->string('api_consumer_secret');
            $table->string('webhook_secret');
            $table->softDeletes('deactivated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_websites');
    }
}
