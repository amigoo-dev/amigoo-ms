<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooStripeAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_stripe_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_default')->default(false);
            $table->string('owner_name');
            $table->string('email');
            $table->string('country_code');
            $table->string('status');
            $table->string('public_key');
            $table->string('secret_key');
            $table->float('daily_max_received_amount');
            $table->boolean('should_wait_for_payout');
            $table->text('note')->nullable();
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_stripe_accounts');
    }
}
