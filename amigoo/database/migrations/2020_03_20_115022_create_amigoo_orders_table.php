<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stripe_acc_id');
            $table->unsignedBigInteger('website_id');

            // Data from Woo site
            //
            $table->unsignedBigInteger('woo_order_id');
            $table->string('status')->nullable();
            $table->float('total')->nullable();
            $table->string('payment_method')->nullable();
            $table->dateTime('created_at')->nullable();

            $table->unique(['website_id', 'woo_order_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_orders');
    }
}
