<?php

use Amigoo\Database\Models\WooOrder;
use Amigoo\Database\Models\Constants\OrderStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooOrdersTableAddPaidAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_orders', function (Blueprint $table) {
            $table->dateTime('paid_at')->nullable();
        });

        WooOrder::where('status', OrderStatus::PROCESSING)
            ->update(['paid_at' => DB::raw('created_at')]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
