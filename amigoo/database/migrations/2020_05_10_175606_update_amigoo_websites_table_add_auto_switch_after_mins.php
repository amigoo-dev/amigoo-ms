<?php

use Amigoo\Database\Models\Website;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooWebsitesTableAddAutoSwitchAfterMins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {
            $table->integer('auto_switch_after_mins')->nullable();
        });

        Website::where('id', '!=', 0)
            ->update(['auto_switch_after_mins' => 30]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {
            $table->dropColumn('auto_switch_after_mins');
        });
    }
}
