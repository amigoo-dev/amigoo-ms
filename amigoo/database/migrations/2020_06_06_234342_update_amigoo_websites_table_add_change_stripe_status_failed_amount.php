<?php

use Amigoo\Database\Models\Website;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooWebsitesTableAddChangeStripeStatusFailedAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {
            $table->integer('change_stripe_status_failed_amount')->default(Website::DEFAULT_CHANGE_STRIPE_STATUS_FAILED_AMOUNT);
        });

        Website::where('id', '!=', 0)
            ->update(['change_stripe_status_failed_amount' => Website::DEFAULT_CHANGE_STRIPE_STATUS_FAILED_AMOUNT]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {
            $table->dropColumn('change_stripe_status_failed_amount');
        });
    }
}
