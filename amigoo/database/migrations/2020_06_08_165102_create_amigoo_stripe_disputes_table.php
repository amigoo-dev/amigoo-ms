<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooStripeDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_stripe_disputes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('website_id')->nullable();
            $table->unsignedBigInteger('stripe_acc_id');
            $table->string('stripe_payment_id')->unique();
            $table->float('amount');
            $table->string('currency_code');
            $table->string('reason', 1024);
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_stripe_disputes');
    }
}
