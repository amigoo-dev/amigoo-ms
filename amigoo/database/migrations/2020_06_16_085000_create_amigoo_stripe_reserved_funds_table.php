<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooStripeReservedFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_stripe_reserved_funds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stripe_acc_id');

            $table->float('amount');
            $table->string('currency_code');
            $table->dateTime('available_on');
            $table->string('reporting_category');
            $table->string('status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_stripe_reserved_funds');
    }
}
