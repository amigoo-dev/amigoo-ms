<?php

use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\Constants\WebsiteType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooWebsitesTableAddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {
            $table->string('type');
        });

        Website::withTrashed()
            ->where('id', '!=', 0)
            ->update(['type' => WebsiteType::WOO]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
