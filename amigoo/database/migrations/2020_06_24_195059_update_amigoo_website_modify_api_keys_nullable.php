<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooWebsiteModifyApiKeysNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {

            $table->string('api_consumer_key')
                ->nullable()
                ->change();

            $table->string('api_consumer_secret')
                ->nullable()
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amigoo_websites', function (Blueprint $table) {
            //
        });
    }
}
