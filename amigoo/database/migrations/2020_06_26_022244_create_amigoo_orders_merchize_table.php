<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooOrdersMerchizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_orders_merchize', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stripe_acc_id');
            $table->unsignedBigInteger('website_id');

            // Data from Merchize site
            //
            $table->string('order_code');
            $table->string('customer_email');
            $table->float('total');
            $table->string('currency_code');
            $table->string('status');
            $table->timestamp('created_at')->useCurrent();

            $table->unique(['website_id', 'order_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_orders_merchize');
    }
}
