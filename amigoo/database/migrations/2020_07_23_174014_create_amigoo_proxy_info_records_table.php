<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooProxyInfoRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_proxy_info_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stripe_acc_id')->unique(); // One-to-one relationship;

            $table->string('ip');
            $table->integer('port')->nullable();
            $table->string('user')->nullable();
            $table->string('password')->nullable();
            $table->string('type')->nullable();
            $table->string('provider')->nullable();
            $table->timestamp('last_proc_at')->nullable();
            $table->boolean('has_last_proc_failed')->nullable()->default(false);

            $table->timestamps();

            $table->unique(['ip', 'port']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_proxy_info_records');
    }
}
