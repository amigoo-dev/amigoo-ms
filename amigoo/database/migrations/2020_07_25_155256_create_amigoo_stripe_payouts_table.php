<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmigooStripePayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigoo_stripe_payouts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stripe_acc_id');

            $table->string('stripe_po_id')->unique();
            $table->string('type');
            $table->string('status');
            $table->float('amount');
            $table->string('currency_code');
            $table->timestamp('arrival_date');
            $table->timestamp('created_at')->nullable(); // Apply NULLABLE to pass issue MYSQL 1067
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigoo_stripe_payouts');
    }
}
