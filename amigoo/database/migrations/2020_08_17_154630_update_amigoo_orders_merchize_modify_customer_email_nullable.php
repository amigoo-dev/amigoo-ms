<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooOrdersMerchizeModifyCustomerEmailNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_orders_merchize', function (Blueprint $table) {

            $table->string('customer_email')
                ->nullable()
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amigoo_orders_merchize', function (Blueprint $table) {

            $table->string('customer_email')
                ->change();
        });
    }
}
