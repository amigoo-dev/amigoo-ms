<?php

use Amigoo\Database\Models\Constants\NotificationType;
use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Database\Repos\WebsiteUserModeratablesRepo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooWebsiteUserModeratablesAddEnabledNotificationsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_website_user_moderatables', function (Blueprint $table) {

            $table->string('enabled_notifications')->nullable();
        });


        /*
         * Update enabled_notifications for all site-moderators of ONLY active-sites;
         */
        $moderatablesRepo = app(WebsiteUserModeratablesRepo::class);
        $websitesRepo     = app(WebsitesRepo::class);

        $activeSites = $websitesRepo->getSitesHaveLinkingStripe();
        foreach ($activeSites as $site) {
            $siteMods = $site->moderators;
            foreach ($siteMods as $moderator) {
                $moderatablesRepo->updateEnabledNotifications($moderator, $site, [
                    NotificationType::STRIPE_ACC_ISSUE_STATUS,
                    NotificationType::STRIPE_DISPUTE,
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amigoo_website_user_moderatables', function (Blueprint $table) {

            $table->dropColumn('enabled_notifications');
        });
    }
}
