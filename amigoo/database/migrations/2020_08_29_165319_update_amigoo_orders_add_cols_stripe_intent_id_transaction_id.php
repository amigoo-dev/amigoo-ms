<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAmigooOrdersAddColsStripeIntentIdTransactionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amigoo_orders', function (Blueprint $table) {
            $table->string('stripe_intent_id')->nullable();
            $table->string('stripe_charge_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('amigoo_orders', function (Blueprint $table) {
            $table->dropColumn('stripe_intent_id');
            $table->dropColumn('stripe_charge_id');
        });
    }
}
