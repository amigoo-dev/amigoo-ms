<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleStripeAccountModeratorSeeder extends Seeder
{
    public function run()
    {
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

        $stripeAccModeratorRole = Role::create(['name' => 'stripe-account-moderator'])
            ->givePermissionTo('view backend');
    }
}
