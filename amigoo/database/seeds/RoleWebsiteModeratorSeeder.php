<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleWebsiteModeratorSeeder extends Seeder
{
    public function run()
    {
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

        $siteModeratorRole = Role::create(['name' => 'website-moderator'])
            ->givePermissionTo('view backend');
    }
}
