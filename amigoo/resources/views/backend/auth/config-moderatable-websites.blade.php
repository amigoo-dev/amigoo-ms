@extends('backend.layouts.app')

@section('title', "Config Moderatable Websites")

@section('content')
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-0">Moderatable Websites Configuration</h4>
                    <small>
                        For <a href="{{ route('admin.auth.user.show', $user) }}">{{ $user->name }} ({{ $user->email }})</a>
                    </small>
                </div>

                <form method="POST" action="{{ route('admin.auth.user.config-moderatable-websites.update', $user) }}">
                    {{ csrf_field() }}

                    <table class="table table-hover border-bottom m-0">
                        <tbody>
                            @foreach($websites as $site)
                                <tr>
                                    <td class="p-0">
                                        <label class="d-inline-block w-100 m-0 p-2 px-4">
                                            @if($curModeratableSitesIds->contains($site->id))
                                                <input type="checkbox" name="moderatable_websites_ids[]" value="{{ $site->id }}" class="mr-2" checked>
                                            @else
                                                <input type="checkbox" name="moderatable_websites_ids[]" value="{{ $site->id }}" class="mr-2">
                                            @endif

                                            <a href="{{ route('admin.websites.get.form.update', $site) }}" target="_blank">{{ $site->name }}</a>
                                            <br>
                                            <small>
                                                <a href="{{ $site->url }}" target="_blank">{{ $site->url }}</a>
                                            </small>
                                        </label>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="card-body text-center">
                        <a href="{{ route('admin.auth.user.index') }}" class="btn btn-light btn-sm">Back</a>
                        <button class="btn btn-primary btn-sm">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
