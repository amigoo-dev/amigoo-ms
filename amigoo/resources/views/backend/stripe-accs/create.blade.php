@extends('backend.layouts.app')

@section('title', 'Stripe Account Creation Form')

@section('content')
    @include('amigoo::backend.stripe-accs.partials.form-create')
@endsection
