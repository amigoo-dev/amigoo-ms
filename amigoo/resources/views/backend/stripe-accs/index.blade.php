@extends('backend.layouts.app')

@section('title', "Stripe Accounts")

@section('content')
<div class="card">
    <div class="card-body">
        @can('create', Amigoo\Database\Models\StripeAccount::class)
            <div class="btn-toolbar float-right">
                <a href="{{ route('admin.stripe-accs.get.form.create') }}" class="btn btn-success btn-sm ml-1" data-toggle="tooltip" title="" data-original-title="Add New">
                    <i class="fas fa-plus-circle mr-1"></i> <strong>Add New Account</strong>
                </a>
            </div><!--btn-toolbar-->
        @endcan

        <h4 class="card-title mb-4">Stripe Accounts</h4>

        <p>
            <div class="row">
                <div class="col-lg-11 col-xl-9">
                    <h6 class="text-secondary">Filter by Status</h6>
                    <div class="row">
                        @foreach (StripeAccountStatus::getAvailableStatuses() as $status => $label)
                            <div class="col-6 col-sm-2">
                                <a href="javascript:void(0);" class="btn-filter-by-status" data-status-code="{{ $status }}">
                                    @include('amigoo::backend.stripe-accs.partials.status-count-badge', [
                                        'status' => $status,
                                        'count'  => $stripeAccs->filter(fn($acc) => ($acc->status == $status))->count(),
                                    ])
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </p>

        <p>
            <form id="frm-search" class="form-inline">
                <h6 class="text-secondary">
                    Search by
                    <select name="search_field" class="form-control form-control-sm mr-2">
                        <option value="email">Email</option>
                        <option value="owner">Owner</option>
                        <option value="proxy">Proxy</option>
                    </select>
                    with keyword
                    <input name="keyword" type="text" class="form-control form-control-sm mr-2">
                    <button class="btn btn-sm btn-light">Search</button>
                </h6>
            </form>
        </p>
    </div>

    @include('amigoo::backend.stripe-accs.partials.stripe-accs-table')
</div>
@endsection
