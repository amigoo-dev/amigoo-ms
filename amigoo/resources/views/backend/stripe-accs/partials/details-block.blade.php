<div class="card">
    <div class="card-body">

        @if(isset($title))
            <h4 class="card-title mb-0">
                {{ $title }}
            </h4>
            <hr>
        @endif

        <div class="row mt-2">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="form-control-label">Status</label>
                    @include('amigoo::backend.stripe-accs.partials.status-header', ['status' => $acc->status])
                </div>
            </div>
            @if($acc->exists)
                <div class="col-md-9">
                    <hr class="d-md-none">
                    <div class="row">
                        <div class="col-4 text-right border-left d-none d-md-block">
                            <label class="form-control-label">Today Received</label>
                            <h2>{{ $todayReceived }}$</h2>
                        </div>
                        <div class="col-4 text-right d-md-none">
                            <label class="form-control-label">Today Received</label>
                            <h2>{{ $todayReceived }}$</h2>
                        </div>
                        <div class="col-4 border-left">
                            <label class="form-control-label">Daily Limit</label>
                            <h2>{{ $acc->daily_max_received_amount }}$</h2>
                        </div>
                        <div class="col-4 border-left">
                            <label class="form-control-label">Total Received</label>
                            <h2>{{ $totalReceived }}$</h2>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <hr>
        <div class="row mt-4">
            <div class="col">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">Owner Name</label>
                            <h4><span class="badge badge-light">{{ $acc->owner_name }}</span></h4>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Email</label>
                            <h4><span class="badge badge-light">{{ $acc->email }}</span></h4>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Country Code</label>
                            <h4><span class="badge badge-light">{{ $acc->country_code }}</span></h4>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Daily Max Received Amount</label>
                            <h4><span class="badge badge-light">{{ $acc->daily_max_received_amount }}</span></h4>

                            @if ($acc->should_wait_for_payout)
                            <h6>
                                <em class="badge badge-dark">Should wait for payout</em>
                            </h6>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">Note</label>
                            <h4>
                                @foreach (explode('\n', $acc->note) as $noteLine)
                                <span class="badge badge-light">{{ $noteLine }}</span>
                                @endforeach
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label">Linkable Website</label>
                            @if ($curLinkedSite)
                                <div class="alert alert-info">
                                    <p>
                                        This Stripe account is currently linked to
                                        <strong><a href="{{ route('admin.websites.get.form.update', $curLinkedSite) }}" target="_blank">
                                                {{ $curLinkedSite->name }}</a></strong>.
                                    </p>
                                    <p class="mb-0">
                                        Go to this site's details page to link the site to another Stripe account.
                                    </p>
                                </div>
                            @elseif ($isCurLinkableSiteDefaultAcc)
                                <div class="alert alert-info">
                                    <p>
                                        This Stripe account is currently configured as default account for
                                        <strong><a href="{{ route('admin.websites.get.form.update', $curLinkableSite) }}" target="_blank">
                                                {{ $curLinkableSite->name }}</a></strong>.
                                    </p>
                                    <p class="mb-0">
                                        Go to
                                        <a href="{{ route('admin.websites.get.form.config-linkables', $curLinkableSite) }}" target="_blank">
                                            Linkable Stripe Accounts Configuration</a>
                                        of this site to set its default account.
                                    </p>
                                </div>
                            @else
                                <h4>
                                    <a href="{{ route('admin.websites.get.form.update', $curLinkableSite) }}" class="badge badge-light">{{ $curLinkableSite->name }}</a>
                                </h4>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                <a class="btn btn-secondary btn-sm" href="{{ route('admin.stripe-accs.get.index') }}">Back</a>
            </div><!--col-->
        </div><!--row-->
    </div>
</div>
