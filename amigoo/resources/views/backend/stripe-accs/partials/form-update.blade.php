<form action="{{ route('admin.stripe-accs.post.form.update', compact('acc')) }}" method="POST"
      class="form-horizontal">
    @csrf

    <div class="card">
        <div class="card-body">

            <div class="row mt-2">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="form-control-label">Status</label>
                        <select class="form-control" name="status">
                            @foreach (StripeAccountStatus::getAvailableStatuses() as $value => $label)
                                @if (old('status', $acc->status) == $value)
                                    <option value="{{ $value }}" selected>{{ $label }}</option>
                                @else
                                    <option value="{{ $value }}">{{ $label }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                @if($acc->exists && Auth::user()->can('viewOrderPaymentsSummaryAndList', Amigoo\Database\Models\StripeAccount::class))
                    <div class="col-md-9">
                        <hr class="d-md-none">
                        <div class="row">
                            <div class="col-4 text-right border-left d-none d-md-block">
                                <label class="form-control-label">Today Received</label>
                                <h2>{{ $todayReceived }}$</h2>
                            </div>
                            <div class="col-4 text-right d-md-none">
                                <label class="form-control-label">Today Received</label>
                                <h2>{{ $todayReceived }}$</h2>
                            </div>
                            <div class="col-4 border-left">
                                <label class="form-control-label">Daily Limit</label>
                                <h2>{{ $acc->daily_max_received_amount }}$</h2>
                            </div>
                            <div class="col-4 border-left">
                                <label class="form-control-label">Total Received</label>
                                <h2>{{ $totalReceived }}$</h2>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <hr>
            <div class="row mt-4">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">Owner Name</label>
                        <input name="owner_name" type="text" value="{{ old('owner_name', $acc->owner_name) }}"
                                class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Email</label>
                        <input name="email" type="text" value="{{ old('email', $acc->email) }}"
                                class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Country Code</label>
                        <input name="country_code" type="text" value="{{ old('country_code', $acc->country_code) ?: 'US' }}"
                                class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Daily Max Received Amount</label>
                        <input name="daily_max_received_amount" type="text" value="{{ old('daily_max_received_amount', $acc->daily_max_received_amount) ?: 50 }}"
                                class="form-control">
                        <label class="form-control-label mt-2">
                            <input name="should_wait_for_payout" type="checkbox" value="1"
                                    {{ old('should_wait_for_payout', $acc->should_wait_for_payout) ? 'checked' : '' }}>
                            <em>Should wait for payout</em>
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label">Public Key</label>
                        <input name="public_key" type="text" value="{{ old('public_key', $acc->public_key) }}"
                                class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Secret Key</label>
                        <input name="secret_key" type="text" value="{{ old('secret_key', $acc->secret_key) }}"
                                class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Note</label>
                        <textarea name="note" rows="10"
                                    class="form-control">{{ old('note', $acc->note) }}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        @if (Auth::user()->can('updateLinkableWebsite', Amigoo\Database\Models\StripeAccount::class))
                            <label class="form-control-label">Linkable Website</label>
                            @if ($curLinkedSite)
                                <div class="alert alert-info">
                                    <p>
                                        This Stripe account is currently linked to
                                        <strong><a href="{{ route('admin.websites.get.form.update', $curLinkedSite) }}" target="_blank">
                                                {{ $curLinkedSite->name }}</a></strong>.
                                    </p>
                                    <p class="mb-0">
                                        Go to this site's details page to link the site to another Stripe account.
                                    </p>
                                </div>
                            @elseif ($isCurLinkableSiteDefaultAcc)
                                <div class="alert alert-info">
                                    <p>
                                        This Stripe account is currently configured as default account for
                                        <strong><a href="{{ route('admin.websites.get.form.update', $curLinkableSite) }}" target="_blank">
                                                {{ $curLinkableSite->name }}</a></strong>.
                                    </p>
                                    <p class="mb-0">
                                        Go to
                                        <a href="{{ route('admin.websites.get.form.config-linkables', $curLinkableSite) }}" target="_blank">
                                            Linkable Stripe Accounts Configuration</a>
                                        of this site to set its default account.
                                    </p>
                                </div>
                            @else
                                <select name="linkable_site_id" class="form-control">
                                    <option value>-- NONE --</option>
                                    @foreach ($websites as $site)
                                        @if ($site->id == $curLinkableSiteId)
                                            <option value="{{ $site->id }}" selected>{{ $site->name }}</option>
                                        @else
                                            <option value="{{ $site->id }}">{{ $site->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @if ($acc->exists)
            <div class="card-body">
                <label class="form-control-label">Last Update</label>
                <h5><span class="badge badge-light">{{ $acc->updated_at->copy()->addHours(7)->toDateTimeString() }}</span></h5>
            </div>
        @endif

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    <a class="btn btn-secondary btn-sm" href="{{ route('admin.stripe-accs.get.index') }}">Cancel</a>
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right">Save</button>
                </div><!--col-->
            </div><!--row-->
        </div>
    </div>
</form>
