<div class="card">
    <div class="card-body">
        <h4 class="card-titl mb-4">Merchize Orders</h4>

        <div class="table-responsive overflow-auto mt-4">
            <table class="table table-sm border border-secondary" id="tbl-merchize-orders"
                   data-list-action="{{ route('admin.merchize-orders.ajax.query-by-stripe-acc', compact('acc')) }}">
                <thead class="thead-light">
                    <tr>
                        <th>Website</th>
                        <th>Order Code</th>
                        <th>Customer Email</th>
                        <th class="text-right">Total (USD)</th>
                        <th>Created At</th>
                    </tr>
            </table>
        </div>
    </div>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;
        const $tblOrders = $('#tbl-merchize-orders');

        const table = $tblOrders.DataTable({
            processing : true,
            serverSide : true,
            ajax       : $tblOrders.data('list-action'),
            searchDelay: 750,

            order: [[4, 'desc']],

            autoWidth: false,
            columns: [
                {
                    name: 'website_name',
                    data: 'website',
                    orderable: false,
                    className: 'align-middle',

                    render (website, type, row, meta) {
                        return `<a href="${website._links.details}">${website.name}</a>`;
                    }
                },
                {
                    name: 'order_code',
                    data: 'order_code',
                    className: 'align-middle',
                },
                {
                    name: 'customer_email',
                    data: 'customer_email',
                    className: 'align-middle',
                },
                {
                    name: 'total',
                    data: 'total',
                    className: 'align-middle text-right',

                    render (total, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return total;
                        }

                        const formatted = total.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
                        return `<h5 class="m-0"><code class="badge text-dark">${formatted}</code></h5>`;
                    }
                },
                {
                    name: 'created_at',
                    data: 'created_at',
                    className: 'align-middle',

                    render (createdAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return createdAtStr;
                        }

                        const datetime = moment.utc(createdAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                },
            ],
        });
    });
</script>
@endpush