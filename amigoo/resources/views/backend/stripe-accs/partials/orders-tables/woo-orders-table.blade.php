<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-0">Woo Orders</h4>

        <div class="table-responsive overflow-auto mt-4">
            <table class="table table-sm border border-secondary" id="tbl-woo-orders"
                   data-list-action="{{ route('admin.woo-orders.ajax.query-by-stripe-acc', compact('acc')) }}">
                <thead class="thead-light">
                    <tr>
                        <th>From site</th>
                        <th class="text-right">Order ID</th>
                        <th class="text-center">Status</th>
                        <th>Total (USD)</th>
                        <th>Created At</th>
                        <th>Paid At</th>
                    </tr>
            </table>
        </div>
    </div>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;
        const $tblOrders = $('#tbl-woo-orders');

        const table = $tblOrders.DataTable({
            processing : true,
            serverSide : true,
            ajax       : $tblOrders.data('list-action'),
            searchDelay: 750,

            columns: [
                {
                    name: 'website_name',
                    data: 'website',
                    orderable: false,

                    render (site, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return site.name;
                        }

                        return `<a href="${site._links.details}" target="_blank">${site.name}</a>`;
                    }
                },
                {
                    name : 'woo_order_id',
                    data : 'woo_order_id',
                    class: 'text-right',
                },
                {
                    name : 'status',
                    data : 'status',
                    class: 'text-center',

                    render (status, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return status;
                        }

                        switch (status) {
                            case 'processing':
                            case 'completed':
                                const statusLabel = status[0].toUpperCase() + status.substring(1);
                                return `<h5 class="m-0"><span class="badge badge-success">${statusLabel}</span></h5>`;
                            case 'failed':
                                return `<h5 class="m-0"><span class="badge badge-danger">Failed</span></h5>`;
                        }
                    }
                },
                {
                    name: 'total',
                    data: 'total',
                    class: 'text-right',

                    render (total, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return total;
                        }

                        const formatted = total.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
                        return `<h5 class="m-0"><code class="badge text-dark">${formatted}</code></h5>`;
                    }
                },
                {
                    name: 'created_at',
                    data: 'created_at',

                    render (createdAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return createdAtStr;
                        }

                        const datetime = moment.utc(createdAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                },
                {
                    name: 'paid_at',
                    data: 'paid_at',

                    render (paidAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return paidAtStr;
                        }

                        if (! paidAtStr)
                            return '';

                        const datetime = moment.utc(paidAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                }
            ],

            order: [[4, 'desc']],
        });
    });
</script>
@endpush