<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-4">
            Payouts
            <span class="ml-2">
                <button id="btn-run-cmd-retrieve-all" class="btn btn-sm btn-light"
                        data-action="{{ route('admin.stripe-payouts.ajax.run-cmd-retrieve-all', $acc) }}">
                    Update DB for payout (retrieve all)
                </button>
            </span>
        </h4>

        <div class="table-responsive overflow-auto mt-4">
            <table class="table table-sm border border-secondary" id="tbl-payouts-orders"
                   data-list-action="{{ route('admin.stripe-payouts.ajax.query-by-stripe-acc', $acc) }}">
                <thead class="thead-light">
                    <tr>
                        <th>PO ID</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th class="text-right">Amount</th>
                        <th>Arrival Date</th>
                        <th>Created At</th>
                    </tr>
            </table>
        </div>
    </div>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;
        const $tblPayouts = $('#tbl-payouts-orders');
        const btnRetrieveAll = document.querySelector('#btn-run-cmd-retrieve-all');

        const table = $tblPayouts.DataTable({
            processing : true,
            serverSide : true,
            ajax       : $tblPayouts.data('list-action'),
            searchDelay: 750,

            order: [[4, 'desc']],

            autoWidth: false,
            columns: getColumnsDefinitions(),
        });

        btnRetrieveAll.addEventListener('click', async function (evt) {
            const defaultText = btnRetrieveAll.innerText;
            btnRetrieveAll.disabled  = true;
            btnRetrieveAll.innerText = 'Running...';

            const url  = btnRetrieveAll.dataset.action;
            try {
                const resp = await fetch(url, {
                    method: 'POST',
                    headers: {
                        'X-CSRF-Token': CSRF_TOKEN
                    },
                });

                const { ok, message } = await resp.json();

                if (ok)
                    alert('All payouts have been retrieved. DB has been updated.');
                else
                    alert(message || 'Failed to update DB');

            } catch ($err) {
                console.error($err);
                alert('Failed to update DB');

            } finally {
                btnRetrieveAll.disabled  = false;
                btnRetrieveAll.innerText = defaultText;
            }
        });


        function getColumnsDefinitions () {
            return [
                {
                    name: 'stripe_po_id',
                    data: 'stripe_po_id',
                    className: 'align-middle',
                },
                {
                    name: 'type',
                    data: 'type',
                    className: 'align-middle text-center',

                    render (type) {
                        return codeToLabel(type);
                    }
                },
                {
                    name: 'status',
                    data: 'status',
                    className: 'align-middle text-center',

                    render (status) {
                        return codeToLabel(status);
                    }
                },
                {
                    name: 'amount',
                    data: 'amount',
                    className: 'align-middle text-right',

                    render (amount, type, row, meta) {
                        const formatted = amount.toLocaleString('en-US', { style: 'currency', currency: row.currency_code });
                        return `<h5 class="m-0"><code class="badge text-dark">${formatted}</code></h5>`;
                    }
                },
                {
                    name: 'arrival_date',
                    data: 'arrival_date',
                    className: 'align-middle text-right',

                    render (arrivalDateStr, type, row, meta) {
                        const datetime = moment.utc(arrivalDateStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY');

                        return `${datetime} (Hanoi)`;
                    }
                },
                {
                    name: 'created_at',
                    data: 'created_at',
                    className: 'align-middle text-right',

                    render (createdAtStr, type, row, meta) {
                        const datetime = moment.utc(createdAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                },
            ];
        }

        function codeToLabel (code) {
            return code
                .replace(/_/g, ' ')
                .replace(/\b\S/g, match => match.toUpperCase())
                ;
        }
    });
</script>
@endpush
