<form action="{{ route('admin.stripe-accs.post.form.proxy-config', $acc) }}" method="POST"
      class="form-horizontal">
    @csrf

    <div class="card">
        <div class="card-body">

            <h4 class="card-title mb-0">
                Proxy Configuration

                @if ($proxyInfo && $proxyInfo->last_proc_at)
                    <small class="d-block mt-2">
                    @if ($proxyInfo->has_last_proc_failed)
                        <span class="badge badge-danger">Last proc.:{{ $proxyInfo->last_proc_at->toDateTimeString() }} (GMT)</span>
                    @else
                        <span class="badge badge-success">Last proc.:{{ $proxyInfo->last_proc_at->toDateTimeString() }} (GMT)</span>
                    @endif
                    </small>
                @endif
            </h4>
            <hr>

            <div class="row mt-4">
                <div class="col-6 col-sm-3 col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">IP</label>
                        <input name="ip" type="text" value="{{ old('ip', $proxyInfo->ip) }}"
                                class="form-control">
                    </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Port</label>
                        <input name="port" type="number" min="0" value="{{ old('port', $proxyInfo->port) }}"
                                class="form-control">
                    </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">User</label>
                        <input name="user" type="text" value="{{ old('user', $proxyInfo->user) }}"
                                class="form-control">
                    </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Password</label>
                        <input name="password" type="text" value="{{ old('password', $proxyInfo->password) }}"
                                class="form-control">
                    </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Type</label>
                        <input name="type" type="text" value="{{ old('type', $proxyInfo->type) }}"
                                class="form-control">
                    </div>
                </div>
                <div class="col-6 col-sm-3 col-lg-12">
                    <div class="form-group">
                        <label class="form-control-label">Provider</label>
                        <input name="provider" type="text" value="{{ old('provider', $proxyInfo->provider) }}"
                                class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right">Save</button>
                </div><!--col-->
            </div><!--row-->
        </div>
    </div>
</form>
