<h5 class="m-0" data-value="{{ $status }}">
    @if($status == StripeAccountStatus::READY)
        <span class="badge badge-success">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::PENDING)
        <span class="badge badge-light">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::REACHED_DAILY_MAX)
        <span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_PAYOUT)
        <span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_PHONE)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_ID)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_IDENTITY)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_TAX)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_SITE)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_SUBMIT_W8)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::NEED_UPDATE_BANK)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::FAILED_TOO_MUCH)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::NEED_CHECK)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_DOC)
        <span class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_DOMAIN)
        <span class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::ENDING)
        <span class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::REFUNDED_ALL)
        <span class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::HOLD_90)
        <span class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}</span>
    @elseif($status == StripeAccountStatus::HOLD_120)
        <span class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}</span>
    @endif
</h5>