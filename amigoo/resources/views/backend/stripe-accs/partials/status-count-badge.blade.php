<h5>
    @if($status == StripeAccountStatus::READY)
        <span data-value="{{ $status }}" class="badge badge-success">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::PENDING)
        <span data-value="{{ $status }}" class="badge badge-secondary">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::REACHED_DAILY_MAX)
        <span data-value="{{ $status }}" class="badge" style="background-color: #f2e6ff; color: #8c1aff;">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_PAYOUT)
        <span data-value="{{ $status }}" class="badge" style="background-color: #f2e6ff; color: #8c1aff;">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_PHONE)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_ID)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_IDENTITY)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_TAX)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_SITE)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_SUBMIT_W8)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::NEED_UPDATE_BANK)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::FAILED_TOO_MUCH)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::NEED_CHECK)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_DOC)
        <span data-value="{{ $status }}" class="badge badge-warning">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::WAITING_VERIFY_DOMAIN)
        <span data-value="{{ $status }}" class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::ENDING)
        <span data-value="{{ $status }}" class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::REFUNDED_ALL)
        <span data-value="{{ $status }}" class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::HOLD_90)
        <span data-value="{{ $status }}" class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @elseif($status == StripeAccountStatus::HOLD_120)
        <span data-value="{{ $status }}" class="badge badge-dark">{{ StripeAccountStatus::getLabel($status) }}: {{ $count }}</span>
    @endif
</h5>