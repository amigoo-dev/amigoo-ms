<div class="table-responsive overflow-auto">
    <table id="tbl-stripe-accs" class="table table-sm border-bottom"
           data-list-action="{{ route('admin.stripe-accs.ajax.query') }}">
        <thead class="thead-light">
            <th>Email</th>
            <th class="text-center">Region</th>
            <th class="text-center">Status</th>
            @if ($canViewOrderPaymentsSummary)
                <th id="col-head-today">Today</th>
            @endif
            <th class="text-center">W4P</th>
            @if ($canViewOrderPaymentsSummary)
                <th id="col-head-total" class="text-right">Total</th>
            @endif
            @if ($canViewPayout)
                <th id="col-po-amount-last2days" class="text-right">PO2Days</th>
            @endif
            @if ($canViewReservedFund)
                <th id="col-head-reserved-fund" class="text-right">ReservedFund</th>
            @endif
            @if (! Auth::user()->hasRole(Role::STRIPE_ACC_MODERATOR))
                <th id="col-websites">Websites</th>
            @endif
            <th>Note</th>
            <th>Proxy</th>
            <th>Latest Order</th>
            <th>Last Update</th>
        </thead>
    </table>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;

        const frmSearch      = document.getElementById('frm-search');
        const tblStripeAccs  = document.getElementById('tbl-stripe-accs');
        const $tblStripeAccs = $(tblStripeAccs);

        const columnsConfigs  = getColumnsConfigs();
        const emailColIdx     = columnsConfigs.findIndex(colConfig => colConfig.name == 'email');
        const proxyColIdx     = columnsConfigs.findIndex(colConfig => colConfig.name == 'proxy_info');
        const statusColIdx    = columnsConfigs.findIndex(colConfig => colConfig.name == 'status');

        const table = $tblStripeAccs.DataTable({
            serverSide: true,
            ajax      : $tblStripeAccs.data('list-action'),

            processing: true,
            language: {
                loadingRecords: '&nbsp;',
                processing    : 'Loading...',
            },

            searching: true,
            info     : true,
            autoWidth: false,
            dom      : 't',

            columns: columnsConfigs,
        });

        $('.btn-filter-by-status').on('click', function () {
            const btn = this;
            const statusCode = btn.dataset.statusCode;
            const statusCol  = table.columns(statusColIdx);

            if (statusCol.search() == statusCode)
                return;

            table
                .columns()
                .search('');

            statusCol
                .search(statusCode)
                .draw();
        });

        $(frmSearch).on('submit', function (e) {
            e.preventDefault();

            const keyword     = this.keyword.value.trim();
            const searchField = this.search_field.value;

            const colForSearch = (
                () => {
                    switch (searchField) {
                        case 'email':
                            return table.columns(emailColIdx);
                        case 'proxy':
                            return table.columns(proxyColIdx);
                    }
                }
            )();

            if (colForSearch.search() == keyword);

            table
                .columns()
                .search('');

            colForSearch
                .search(keyword)
                .draw();
        });

        // Filtered by Status [Ready] is the default.
        document.querySelector('.btn-filter-by-status').click();

        function getColumnsConfigs () {
            let columnsConfigs = [
                {
                    name: 'email',
                    data: 'email',
                    class: 'align-middle',

                    render (email, type, row, meta) {
                        var html = '';

                        const linkToStripeDetails = document.createElement('a');
                        linkToStripeDetails.href      = row._meta.href;
                        linkToStripeDetails.innerHTML = `<strong>${email}</strong>`;
                        html += linkToStripeDetails.outerHTML;

                        const siteConfiguredAsDefaultAcc = row.linkable_websites.find(site => site.linkable.is_default);
                        if (siteConfiguredAsDefaultAcc) {
                            const link = document.createElement('a');
                            link.href      = siteConfiguredAsDefaultAcc._meta.href;
                            link.innerHTML = `<strong>${siteConfiguredAsDefaultAcc.name}</strong>`;
                            html += `<br><small>Default for: ${link.outerHTML}</small>`;
                        }

                        return html;
                    }
                },
                {
                    name: 'country_code',
                    data: 'country_code',
                    width: 50,
                    class: 'align-middle text-center',

                    render (countryCode, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return countryCode;
                        }

                        if (countryCode == 'UK')
                            countryCode = 'gb';

                        countryCode = countryCode.toLowerCase();

                        return `<h5 class="m-0"><span class="flag-icon flag-icon-${countryCode}"></span></h5>`;
                    }
                },
                {
                    name     : 'status',
                    data     : 'status',
                    width    : 75,
                    class    : 'align-middle text-center',

                    render (statusCode) {
                        return `<h5>${renderStripeAccStatusBadge(statusCode)}</h5>`;
                    }
                },
                {
                    name: 'should_wait_for_payout',
                    data: 'should_wait_for_payout',
                    width: 50,
                    class: 'align-middle text-center',

                    render (shouldWaitForPayout) {
                        if (shouldWaitForPayout)
                            return '<i class="far fa-check-circle"></i>';

                        return null;
                    }
                },
                {
                    name: 'linkable_websites',
                    data: 'linkable_websites',
                    width: 120,
                    class: 'align-middle',
                    orderable: false,

                    render (linkableWebsites, type, row) {
                        const linksToSite = [];
                        for (const site of linkableWebsites) {
                            const linked     = (site.stripe_acc_id == row.id);
                            const badgeColor = linked ? 'success'  :  'light';
                            const link = document.createElement('a');
                            link.href      = site._meta.href;
                            link.innerText = site.name;
                            link.className = `badge badge-${badgeColor}`;

                            const badge = document.createElement('h5');
                            badge.className = 'm-0';
                            badge.appendChild(link);

                            linksToSite.push(badge);
                        }

                        return linksToSite.map(link => link.outerHTML).join('<br>');
                    }
                },
                {
                    name: 'note',
                    data: 'note',
                    width: 400,
                    class: 'align-middle',

                    render (note) {
                        return note
                            && note.replace(/\n/g, '<br>');
                    }
                },
                {
                    name: 'proxy_info',
                    data: 'proxy_info',
                    class: 'align-middle',

                    render (proxyInfo) {
                        if (! proxyInfo)
                            return null;

                        const proxyAddr =
                            proxyInfo.port
                                ? `${proxyInfo.ip}:${proxyInfo.port}`
                                : proxyInfo.ip;

                        const badgeColor = proxyInfo.has_last_proc_failed ? 'danger' : 'success';
                        const badge = document.createElement('span')
                        badge.className = `badge badge-${badgeColor}`;
                        badge.innerText = proxyAddr;

                        return `<h5 class="m-0">${badge.outerHTML}</h5>`;
                    }
                },
                {
                    name: 'latest_order_created_at',
                    data: 'latest_order',
                    width: 75,
                    class: 'align-middle text-right',
                    orderable: false,

                    render (latestOrder, type, row, meta) {
                        const datetime = latestOrder ? moment(latestOrder.created_at) : null;

                        if (! datetime)
                            return '';

                        const notActive = (row.status_code == -1);

                        const formatted = (() => {
                            const now = moment();
                            const duration = moment.duration(now.diff(datetime)).asMinutes();

                            return (duration > 1440)
                                ? datetime.utcOffset('+0700').format('DD/MM/YYYY HH:mm:ss')
                                : moment.duration(-duration, 'minutes').humanize(true);
                        })();

                        if (notActive)
                            return `<h5 class="m-0"><span class="badge text-secondary">${formatted}</span></h5>`;

                        return `<h5 class="m-0"><span class="badge badge-light">${formatted}</span></h5>`;
                    }
                },
                {
                    name: 'updated_at',
                    data: 'updated_at',
                    width: 75,
                    class: 'align-middle text-right',

                    render (updatedAt, type, row, data) {
                        const datetime = moment(updatedAt);

                        const notActive = (row.status_code == -1);

                        const formatted = (() => {
                            const now = moment();
                            const duration = moment.duration(now.diff(datetime)).asMinutes();

                            return (duration > 1440)
                                ? datetime.utcOffset('+0700').format('DD/MM/YYYY HH:mm:ss')
                                : moment.duration(-duration, 'minutes').humanize(true);
                        })();

                        if (notActive)
                            return `<h5 class="m-0"><span class="badge text-secondary">${formatted}</span></h5>`;

                        return `<h5 class="m-0"><span class="badge badge-light">${formatted}</span></h5>`;
                    }
                }
            ];

            const colHeaderElmts = [...tblStripeAccs.querySelectorAll('thead th')];
            for (colIdx in colHeaderElmts) {
                const colHeader = colHeaderElmts[colIdx];

                switch (colHeader.id) {
                    case 'col-head-today':
                        columnsConfigs.splice(colIdx, 0, {
                            name: 'today_received',
                            data: 'today_received',
                            class: 'align-middle text-right',

                            render (todayReceived, type, row, meta) {
                                const formattedTodayReceived = todayReceived.toLocaleString('en-US', { style: 'currency', currency: 'USD'});
                                const formattedDailyLimit    = row.daily_max_received_amount.toLocaleString('en-US', { style: 'currency', currency: 'USD'});
                                const txtColor               = todayReceived ? 'black' : 'secondary';

                                return `<code><h5 class="m-0"><span class="badge badge-light text-${txtColor}">${formattedTodayReceived}</span><br><span class="badge text-secondary">${formattedDailyLimit}</span></h5></code>`;
                            }
                        });
                        continue;

                    case 'col-head-total':
                        columnsConfigs.splice(colIdx, 0, {
                            name: 'total_received',
                            data: 'total_received',
                            width: 75,
                            class: 'align-middle text-right',

                            render (totalReceived, type, row, meta) {
                                const notActive    = (row.status_code == -1);
                                const badgeStyling = notActive ? 'text-secondary' : 'badge-light';
                                const currencyFormatted =
                                    totalReceived.toLocaleString('en-US', {
                                        style: 'currency',
                                        currency: 'USD',
                                    });
                                const txtColor = totalReceived ? 'black' : 'secondary';

                                return `<code><h5 class="m-0"><span class="badge ${badgeStyling} text-${txtColor}">${currencyFormatted}</span></h5></code>`;
                            }
                        });
                        continue;

                    case 'col-head-reserved-fund':
                        columnsConfigs.splice(colIdx, 0, {
                            name: 'reserved_fund.amount',
                            data: 'reserved_fund',
                            width: 75,
                            orderable: false,
                            class: 'align-middle text-right',

                            render (reservedFund, type, row, meta) {
                                const reservedFundNotAvailable = !reservedFund;
                                if (reservedFundNotAvailable)
                                    return null;

                                const notActive    = (row.status_code == -1);
                                const badgeStyling = notActive ? 'text-secondary' : 'badge-light';
                                const currencyFormatted =
                                    reservedFund.amount.toLocaleString('en-US', {
                                        style   : 'currency',
                                        currency: reservedFund.currency_code,
                                    });

                                return `<h5 class="m-0"><span class="badge ${badgeStyling}">${currencyFormatted}</span></h5>`;
                            }
                        });
                        continue;

                    case 'col-po-amount-last2days':
                        columnsConfigs.splice(colIdx, 0, {
                            name: 'po_amount_last2days',
                            data: 'po_amount_last2days',
                            class: 'align-middle text-right',
                            orderable: false,

                            render (po2days, type, row, meta) {
                                if (! po2days)
                                    return null;

                                const notActive    = (row.status_code == -1);
                                const badgeStyling = notActive ? 'text-secondary' : 'badge-light';
                                const currencyFormatted =
                                    po2days.toLocaleString('en-US', {
                                        style   : 'currency',
                                        currency: 'USD',
                                    });

                                if (notActive)
                                    return `<h5 class="m-0"><span class="badge text-secondary">${currencyFormatted}</span></h5>`;

                                return `<h5 class="m-0"><span class="badge badge-light">${currencyFormatted}</span></h5>`;
                            }
                        });
                        continue;
                }
            }

            const colWebsitesNotFound = (! tblStripeAccs.querySelector('thead th#col-websites'));
            if (colWebsitesNotFound) {
                columnsConfigs = columnsConfigs.filter(col => col.name != 'linkable_websites');
            }

            return columnsConfigs;
        }


        function renderStripeAccStatusBadge (statusCode) {
            switch (statusCode) {
                case 'READY':
                    return '<span class="badge badge-success">Ready</span>';
                case 'PENDING':
                    return '<span class="badge badge-light">Pending</span>';
                case 'REACHED_DAILY_MAX':
                    return '<span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">Reached daily</span>';
                case 'WAITING_PAYOUT':
                    return '<span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">Waiting payout</span>';
                case 'WAITING_VERIFY_PHONE':
                    return '<span class="badge badge-warning">Phone ver</span>';
                case 'WAITING_VERIFY_ID':
                    return '<span class="badge badge-warning">ID ver</span>';
                case 'WAITING_VERIFY_IDENTITY':
                    return '<span class="badge badge-warning">Identity ver</span>';
                case 'WAITING_VERIFY_TAX':
                    return '<span class="badge badge-warning">Tax ver</span>';
                case 'WAITING_VERIFY_SITE':
                    return '<span class="badge badge-warning">Site ver</span>';
                case 'WAITING_SUBMIT_W8':
                    return '<span class="badge badge-warning">W-8 Submit</span>';
                case 'NEED_UPDATE_BANK':
                    return '<span class="badge badge-warning">Need update bank</span>';
                case 'FAILED_TOO_MUCH':
                    return '<span class="badge badge-warning">Failed too much</span>';
                case 'NEED_CHECK':
                    return '<span class="badge badge-warning">Need check</span>';
                case 'WAITING_VERIFY_DOC':
                    return '<span class="badge badge-warning">Waiting verify doc</span>';
                case 'WAITING_VERIFY_DOMAIN':
                    return '<span class="badge badge-dark">Domain ver</span>';
                case 'ENDING':
                    return '<span class="badge badge-dark">Ending</span>';
                case 'REFUNDED_ALL':
                    return '<span class="badge badge-dark">Refunded All</span>';
                case 'HOLD_90':
                    return '<span class="badge badge-dark">90 days</span>';
                case 'HOLD_120':
                    return '<span class="badge badge-dark">120 days</span>';
                default:
                    return `(${statusCode})`;
            }
        }
    });
</script>
@endpush
