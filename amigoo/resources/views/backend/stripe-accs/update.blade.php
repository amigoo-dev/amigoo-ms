@extends('backend.layouts.app')

@section('title', "Stripe Account #{$acc->id}: {$acc->email} ({$acc->owner_name})")

@section('content')
    <div class="row">
        <div class="col">
            @if (auth()->user()->can('update', $acc))
                @include('amigoo::backend.stripe-accs.partials.form-update')
            @else
                @include('amigoo::backend.stripe-accs.partials.details-block')
            @endif
        </div>

        @if (auth()->user()->can('viewProxyConfig', Amigoo\Database\Models\StripeAccount::class))
        <div class="col-md-12 col-lg-4 col-xl-3">
            @include('amigoo::backend.stripe-accs.partials.proxy-info-form', [
                'proxyInfo' => $acc->proxyInfo ?: new Amigoo\Database\Models\ProxyInfo(),
            ])
        </div>
        @endif
    </div>

    @if (Auth::user()->can('viewOrderPaymentsSummaryAndList', Amigoo\Database\Models\StripeAccount::class))
        @include('amigoo::backend.stripe-accs.partials.orders-tables.woo-orders-table')
        @include('amigoo::backend.stripe-accs.partials.orders-tables.merchize-orders-table')
    @endif

    @if (Auth::user()->can('viewStripePayouts', $acc))
        @include('amigoo::backend.stripe-accs.partials.payouts-table')
    @endif
@endsection
