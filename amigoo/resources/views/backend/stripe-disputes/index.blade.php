@extends('backend.layouts.app')

@section('title', "Stripe Disputes")

@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-4">Stripe Disputes</h4>

        <p>
            <div class="row">
                <div class="col-lg-11 col-xl-9">
                    <h6 class="text-secondary">Filter by Status</h6>
                    @foreach (StripeDisputeStatus::getAvailableStatuses() as $status => $label)
                        <div class="float-left mr-2">
                            <a href="javascript:void(0);" class="btn-filter-by-status" data-status-code="{{ $status }}">
                                @include('amigoo::backend.stripe-disputes.partials.status-count-badge', [
                                    'status' => $status,
                                    'count'  => $disputes->filter(fn($dispute) => ($dispute->status == $status))->count(),
                                ])
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </p>

        <p>
            <form id="frm-search" class="form-inline">
                <h6 class="text-secondary">
                    Search by
                    <select name="search_field" class="form-control form-control-sm mr-2">
                        <option value="website">Website</option>
                        <option value="stripe-acc">Stripe Account</option>
                    </select>
                    with keyword
                    <input name="keyword" type="text" class="form-control form-control-sm mr-2">
                    <button class="btn btn-sm btn-light">Search</button>
                </h6>
            </form>
        </p>
    </div>

    @include('amigoo::backend.stripe-disputes.partials.disputes-table')
</div>
@endsection
