<div class="table-responsive overflow-auto">
    <table id="tbl-disputes" class="table table-sm border-bottom">
        <thead class="thead-light">
            <th>Website</th>
            <th>Stripe acc.</th>
            <th>Stripe Payment ID</th>
            <th class="text-right">Amount</th>
            <th>Reason</th>
            <th class="text-center">Status</th>
            <th class="text-right">Created</th>
            <th class="text-right">Updated</th>
            @can('update', Amigoo\Database\Models\StripeDispute::class)
                <th id="col-header-actions" class="text-right"></th>
            @endcan
        </thead>
        <tbody>
            @forelse ($disputes as $dispute)
                <tr>
                    <td class="align-middle">
                        @if ($dispute->website)
                            @unless ($dispute->website->trashed())
                                <a href="{{ route('admin.websites.get.form.update', ['website' => $dispute->website]) }}" target="_blank">
                                    <strong>{{ $dispute->website->name }}</strong>
                                </a>
                            @else
                                <span>{{ $dispute->website->name }}</span>
                            @endunless
                        @endif
                    </td>
                    <td class="align-middle">
                        <div>
                            <a href="{{ route('admin.stripe-accs.get.form.update', ['acc' => $dispute->stripeAccount]) }}"
                               target="_blank" class="link-to-stripe">
                                {{ $dispute->stripeAccount->email }}</a>

                            <div>
                                <span class="small text-secondary">Current linkable:</span>
                                @foreach ($dispute->stripeAccount->linkableWebsites as $linkableSite)
                                <a href="{{ route('admin.websites.get.form.update', $linkableSite) }}"
                                   target="_blank" class="link-to-current-linked-site">
                                    <span class="badge badge-primary">{{ $linkableSite->name }}</a></span>
                                @endforeach
                            </div>
                        </div>
                    </td>
                    <td class="align-middle">
                        {{ $dispute->stripe_payment_id }}
                    </td>
                    <td class="align-middle text-right">
                        {{ $dispute->amount }} {{ $dispute->currency_code }}
                    </td>
                    <td class="align-middle">
                        {{ $dispute->reason }}
                    </td>
                    <td class="align-middle text-center">
                        {{ $dispute->status }}
                    </td>
                    <td class="align-middle text-right">
                        {{ $dispute->created_at->timestamp }}
                    </td>
                    <td class="align-middle text-right">
                        {{ $dispute->updated_at->timestamp }}
                    </td>
                    @can('update', Amigoo\Database\Models\StripeDispute::class)
                        <td class="align-middle text-right">
                            <a href="{{ route('admin.stripe-disputes.get.form.update', $dispute) }}" class="btn btn-sm btn-light">Update</a>
                        </td>
                    @endcan
                </tr>
            @empty
                <tr>
                    <td colspan="10">There is no dispute.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;

        const tblDisputes  = document.getElementById('tbl-disputes');
        const frmSearch    = document.getElementById('frm-search');
        const $tblDisputes = $(tblDisputes);

        const columnsConfigs = getColumnsConfigs();
        const colIdxCreatedAt = columnsConfigs.findIndex(colConfig => colConfig.name == 'created_at');
        const colIdxWebsite   = columnsConfigs.findIndex(colConfig => colConfig.name == 'website');
        const colIdxStripeAcc = columnsConfigs.findIndex(colConfig => colConfig.name == 'stripe_acc');
        const colIdxStatus    = columnsConfigs.findIndex(colConfig => colConfig.name == 'status');

        const table = $tblDisputes.DataTable({
            paging   : false,
            info     : false,
            autoWidth: false,
            dom      : 't',

            order: [colIdxCreatedAt, 'desc'],

            columns: columnsConfigs,
        });

        $('.btn-filter-by-status').on('click', function () {
            const btn = this;
            const statusCode = btn.dataset.statusCode;
            const statusCol  = table.columns(colIdxStatus);

            if (statusCol.search() == statusCode)
                return;

            table
                .columns()
                .search('');

            statusCol
                .search(statusCode)
                .draw();
        });

        $(frmSearch).on('submit', function (e) {
            e.preventDefault();

            const keyword     = this.keyword.value.trim();
            const searchField = this.search_field.value;

            const colForSearch = (
                () => {
                    switch (searchField) {
                        case 'website':
                            return table.column(colIdxWebsite);
                        case 'stripe-acc':
                            return table.column(colIdxStripeAcc);
                    }
                }
            )();

            console.log(colForSearch);
            if (colForSearch.search() == keyword);

            table
                .columns()
                .search('');

            colForSearch
                .search(keyword)
                .draw();
        });


        function getColumnsConfigs () {
            let columnsConfigs = [
                {
                    name: 'website',

                    render (html, type, row, meta) {
                        const $html     = $(html);
                        const innerText = $html.text().trim();

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return innerText;
                        }

                        return html;
                    }
                },
                {
                    name: 'stripe_acc',
                },
                {
                    name: 'stripe_payment_id',
                },
                {
                    name: 'amount',

                    render (amountWithCurrency, type, row, meta) {
                        const [amountStr, currencyCode] = amountWithCurrency.split(' ');
                        const amount                    = parseFloat(amountStr);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return amount;
                        }

                        const formatted = parseInt(amount).toLocaleString('en-US', { style: 'currency', currency: currencyCode });
                        return `<h5 class="m-0"><code class="badge text-dark">${formatted}</code></h5>`;
                    }
                },
                {
                    name: 'reason',

                    render: codeToLabel
                },
                {
                    name: 'status',

                    render: codeToLabel
                },
                {
                    name: 'created_at',

                    render (timestampStr, type, row, meta) {
                        const datetime = timestampStr ? moment.unix(timestampStr) : null;

                        switch (type) {
                            case 'sort':
                            case 'type':
                                const timestamp = (datetime ? datetime.unix() : 0);
                                return timestamp;
                        }

                        if (! datetime)
                            return '';

                        const formatted = (() => {
                            const now = moment();
                            const duration = moment.duration(now.diff(datetime)).asMinutes();

                            return (duration > 1440)
                                ? datetime.format('DD/MM/YYYY HH:mm:ss')
                                : moment.duration(-duration, 'minutes').humanize(true);
                        })();

                        return `<h5 class="m-0"><span class="badge badge-light">${formatted}</span></h5>`;
                    }
                },
                {
                    name: 'updated_at',

                    render (timestampStr, type, row, meta) {
                        const datetime = timestampStr ? moment.unix(timestampStr) : null;

                        switch (type) {
                            case 'sort':
                            case 'type':
                                const timestamp = (datetime ? datetime.unix() : 0);
                                return timestamp;
                        }

                        if (! datetime)
                            return '';

                        const formatted = (() => {
                            const now = moment();
                            const duration = moment.duration(now.diff(datetime)).asMinutes();

                            return (duration > 1440)
                                ? datetime.format('DD/MM/YYYY HH:mm:ss')
                                : moment.duration(-duration, 'minutes').humanize(true);
                        })();

                        return `<h5 class="m-0"><span class="badge badge-light">${formatted}</span></h5>`;
                    }
                },
                {
                    name     : 'actions',
                    orderable: false,
                }
            ];

            const shouldHideActionsCol = ! document.getElementById('col-header-actions');
            if (shouldHideActionsCol)
                columnsConfigs = columnsConfigs.filter(col => col.name != 'actions');

            return columnsConfigs;
        }

        function codeToLabel (code) {
            return code
                .toLowerCase()
                .replace(/_/g, ' ')
                .replace(/\b\S/g, match => match.toUpperCase())
                ;
        }
    });
</script>
@endpush
