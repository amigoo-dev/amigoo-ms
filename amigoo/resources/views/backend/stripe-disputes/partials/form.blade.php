<form action="{{ $action }}" method="POST">
    @csrf

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label">Status</label>
                        <select class="form-control" name="status">
                            @foreach (StripeDisputeStatus::getAvailableStatuses() as $value => $label)
                                @if (old('status', $dispute->status) == $value)
                                    <option value="{{ $value }}" selected>{{ $label }}</option>
                                @else
                                    <option value="{{ $value }}">{{ $label }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Website</label>
                        <select name="website_id" class="form-control">
                            <option value>-- NOT YET SPECIFIED --</option>
                            @foreach ($websites as $site)
                                @if ($site->id == $dispute->website_id)
                                    <option value="{{ $site->id }}" selected>{{ $site->name }}</option>
                                @else
                                    <option value="{{ $site->id }}">{{ $site->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Stripe Account</label>
                        <h5 class="m-0">
                            <span class="badge badge-light">{{ $dispute->stripeAccount->email }}</span>
                        </h5>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Stripe Payment ID</label>
                        <h5 class="m-0">
                            <span class="badge badge-light">{{ $dispute->stripe_payment_id }}</span>
                        </h5>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Amount</label>
                        <h5 class="m-0">
                            <span class="badge badge-light">{{ $dispute->currency_code }} {{ $dispute->amount }}</span>
                        </h5>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Reason</label>
                        <h5 class="m-0">
                            <span class="badge badge-danger">{{ $dispute->reason }}</span>
                        </h5>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Created At</label>
                        <h5 class="m-0">
                            <span class="badge badge-light">{{ $dispute->created_at }}</span>
                        </h5>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Updated At</label>
                        <h5 class="m-0">
                            <span class="badge badge-light">{{ $dispute->updated_at }}</span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    <a class="btn btn-secondary btn-sm" href="{{ route('admin.stripe-disputes.get.index') }}">Cancel</a>
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right">Save</button>
                </div><!--col-->
            </div><!--row-->
        </div>
    </div>
</form>