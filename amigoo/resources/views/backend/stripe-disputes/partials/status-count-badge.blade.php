<h5>
    <span data-value="{{ $status }}" class="badge badge-light">
        {{ StripeDisputeStatus::getLabel($status) }} ({{ $count }})
    </span>
</h5>