@extends('backend.layouts.app')

@section('title', "Dispute {$dispute->stripe_payment_id}")

@section('content')
    <div class="row">
        <div class="col-sm-8 col-md-6 col-lg-5">
            @include('amigoo::backend.stripe-disputes.partials.form', [
                'title'  => "Dispute {$dispute->stripe_payment_id}",
                'action' => route('admin.stripe-disputes.post.form.update', compact('dispute')),
            ])
        </div>
    </div>
@endsection
