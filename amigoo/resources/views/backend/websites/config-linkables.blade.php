@extends('backend.layouts.app')

@section('title', "Website #{$website->id}: {$website->name} - Linkable Stripe Accounts Configuration")

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-0">Linkable Stripe Accounts Configuration</h4>
            <small>
                For <a href="{{ route('admin.websites.get.form.update', $website) }}">{{ $website->name }}</a>
            </small>
        </div>

        <form id="frm-config" method="POST" action="{{ route('admin.websites.post.form.config-linkables', $website) }}">
            @csrf

            <div class="table-responsive overflow-auto">
                <table class="tbl-stripe-accs table table-sm mb-0 border-bottom border-secondary" style="width: 100%">
                    <thead class="thead-light">
                        <th class="text-center">Linkable?</th>
                        <th class="text-center">Current?</th>
                        <th class="text-center">Default?</th>
                        <th>Email</th>
                        <th class="text-center">Region</th>
                        <th class="text-center">Status</th>
                        <th>Today</th>
                        <th class="text-center">W4P</th>
                        <th class="text-right">Total</th>
                        <th class="text-right">Reserved Fund</th>
                        <th class="text-right">PO2Days</th>
                        <th>Note</th>
                        <th>Proxy</th>
                        <th>Latest Order</th>
                        <th>Last Update</th>
                    </thead>
                    <tbody>
                        @forelse ($stripeAccs as $acc)
                            <tr id="{{ $acc->id }}">
                                <td>{{ (int) $linkableStripeAccsIds->contains($acc->id) }}</td>
                                <td>{{ (int) ($acc->id === $website->stripe_acc_id) }}</td>
                                <td>{{ (int) (($defaultStripeAcc && ($defaultStripeAcc->id === $acc->id))) }}</td>
                                <td>
                                    <a href="{{ route('admin.stripe-accs.get.form.update', compact('acc')) }}">
                                        <strong>{{ $acc->email }}</strong>
                                    </a>
                                </td>
                                <td class="text-center">{{ $acc->country_code }}</td>
                                <td class="text-center">
                                    @include('amigoo::backend.stripe-accs.partials.status-badge', ['status' => $acc->status])
                                </td>
                                <td class="align-middle">{{ $acc->today_received }}/{{ $acc->daily_max_received_amount }}</td>
                                <td class="align-middle text-center">
                                    @if ($acc->should_wait_for_payout)
                                        <i class="far fa-check-circle"></i>
                                    @endif
                                </td>
                                <td class="align-middle text-right">{{ $acc->total_received }}</td>
                                <td class="align-middle text-right">
                                    @if ($acc->reservedFund)
                                        {{ $acc->reservedFund->amount }} {{ $acc->reservedFund->currency_code}}
                                    @endif
                                </td>
                                <td class="align-middle text-right">
                                    @if ($acc->po_amount_last2days)
                                    {{ $acc->po_amount_last2days }}
                                    @endif
                                </td>
                                <td>{!! nl2br(e($acc->note)) !!}</td>
                                <td>
                                    @if ($acc->proxyInfo)
                                        <h5 class="m-0">
                                            @if ($acc->proxyInfo->has_last_proc_failed)
                                            <span class="badge badge-danger">{{ $acc->proxyInfo->proxyAddress }}</span>
                                            @else
                                            <span class="badge badge-success">{{ $acc->proxyInfo->proxyAddress }}</span>
                                            @endif
                                        </h5>
                                    @endif
                                </td>
                                <td>
                                    @if ($acc->latest_order)
                                        <span>{{ $acc->latest_order->created_at->timestamp }}</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $acc->updated_at->timestamp }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7">No available Stripe accounts</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

            <div class="card-body text-center">
                <a href="{{ route('admin.websites.get.form.update', $website) }}" class="btn btn-light btn-sm">Back</a>
                <button class="btn btn-primary btn-sm">Save</button>
            </div>
        </form>
    </div>
@endsection


@push('after-scripts')
<script>
    $(() => {
        const frmConfig     = document.getElementById('frm-config');
        const tblStripeAccs = frmConfig.querySelector('table');

        const CSS_CLASS_BTN_TOGGLE_CURRENT  = '.btn-toggle-current';
        const CSS_CLASS_BTN_TOGGLE_LINKABLE = '.btn-toggle-linkable';
        const CSS_CLASS_BTN_TOGGLE_DEFAULT  = '.btn-toggle-default';


        const table = $(tblStripeAccs).DataTable({
            searching: false,
            paging   : false,
            info     : false,
            autoWidth: false,

            order:  [
                [1, 'desc'],
                [0, 'desc'],
                [2, 'desc'],
                [5, 'asc'],
            ],
            orderFixed: {
                pre: [
                    [1, 'desc'],
                    [0, 'desc'],
                    [2, 'desc'],
                    [5, 'asc'],
                ],
            },

            columns: [
                {
                    name     : 'isLinkable',
                    data     : 'isLinkable',
                    width    : 100,
                    className: 'align-middle text-center',
                    orderable: false,

                    render (isLinkableFlag, type, row, meta) {
                        const isLinkable = (isLinkableFlag == 1);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return isLinkable;
                        }

                        const stripeAccId = row.DT_RowId;
                        return createCheckboxForLinkableFlag(stripeAccId, isLinkable, row).outerHTML;
                    }
                },
                {
                    name: 'isCurrent',
                    data: 'isCurrent',
                    width: 100,
                    className: 'align-middle text-center',
                    orderable: false,

                    render (isCurrentFlag, type, row, meta) {
                        const isCurrent = (isCurrentFlag == 1);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return isCurrent;
                        }

                        const stripeAccId = row.DT_RowId;
                        return createCheckboxForCurrentFlag(stripeAccId, isCurrent, row).outerHTML;
                    }
                },
                {
                    name     : 'isDefault',
                    data     : 'isDefault',
                    width    : 100,
                    className: 'align-middle text-center',
                    orderable: false,

                    render (isDefaultFlag, type, row, meta) {
                        const isDefault = (isDefaultFlag == 1);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return isDefault;
                        }

                        const stripeAccId = row.DT_RowId;
                        return createRadioButtonForDefaultFlag(stripeAccId, isDefault, row).outerHTML;
                    }
                },
                {
                    name: 'email',
                    data: 'email',
                    width: 50,
                    className: 'align-middle',
                },
                {
                    name     : 'region',
                    data     : 'region',
                    width    : 50,
                    className: 'align-middle text-center',

                    render (countryCode, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return countryCode;
                        }

                        if (countryCode == 'UK')
                            countryCode = 'gb';

                        countryCode = countryCode.toLowerCase();

                        return `<h5 class="m-0"><span class="flag-icon flag-icon-${countryCode}"></span></h5>`;
                    }
                },
                {
                    name     : 'status',
                    data     : 'status',
                    width    : 75,
                    className: 'align-middle text-center',
                    render (html, type, row, meta) {
                        switch (type) {
                            case 'sort':
                                const $span     = $(html);
                                const statusVal = $span.data('value');
                                switch (statusVal) {
                                    case 'READY':
                                        return 1;
                                    case 'PENDING':
                                    case 'REACHED_DAILY_MAX':
                                    case 'WAITING_PAYOUT':
                                        return 2;
                                    case 'WAITING_VERIFY_PHONE':
                                    case 'WAITING_VERIFY_ID':
                                    case 'WAITING_VERIFY_IDENTITY':
                                    case 'WAITING_VERIFY_TAX':
                                    case 'WAITING_VERIFY_SITE':
                                    case 'WAITING_SUBMIT_W8':
                                    case 'NEED_UPDATE_BANK':
                                    case 'FAILED_TOO_MUCH':
                                    case 'NEED_CHECK':
                                    case 'WAITING_VERIFY_DOC':
                                        return 3;
                                    case 'WAITING_VERIFY_DOMAIN':
                                    case 'ENDING':
                                    case 'REFUNDED_ALL':
                                    case 'HOLD_90':
                                    case 'HOLD_120':
                                        return 4;
                                }
                            case 'type':
                                return html;
                        }

                        return html;
                    }
                },
                {
                    name: 'today_received',
                    data: 'today_received',
                    type: 'num',
                    className: 'align-middle',

                    render (todayReceivedByDailyLimit, type, row, meta) {
                        const [todayReceived, dailyLimit] = todayReceivedByDailyLimit.trim().split('/').map(parseFloat);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return todayReceived;
                        }

                        const formattedTodayReceived = todayReceived.toLocaleString('en-US', { style: 'currency', currency: 'USD'});
                        const formattedDailyLimit    = dailyLimit.toLocaleString('en-US', { style: 'currency', currency: 'USD'});

                        return `<h5 class="m-0"><span class="badge badge-light">${formattedTodayReceived}</span><span class="badge text-secondary">&sol; ${formattedDailyLimit}</span></h5>`;
                    }
                },
                {
                    name: 'should_wait_for_payout',
                    data: 'should_wait_for_payout',
                    type: 'boolean',
                    width: 50,
                    className: 'align-middle',
                },
                {
                    name: 'total_received',
                    data: 'total_received',
                    type: 'num',
                    width: 75,
                    className: 'align-middle',

                    render (totalReceived, type, row, meta) {
                        totalReceived = parseFloat(totalReceived);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return totalReceived;
                        }

                        const notActive         = (row.status_code == -1);
                        const currencyFormatted = totalReceived.toLocaleString('en-US', { style: 'currency', currency: 'USD'});

                        if (notActive)
                            return `<h5 class="m-0"><span class="badge text-secondary">${currencyFormatted}</span></h5>`;

                        return `<h5 class="m-0"><span class="badge badge-light">${currencyFormatted}</span></h5>`;
                    }
                },
                {
                    name: 'reserved_fund',
                    data: 'reserved_fund',
                    type: 'num',

                    render (reservedFundWithCurrencyCode, type, row, meta) {
                        if (! reservedFundWithCurrencyCode)
                            return null;

                        const [amountStr, currencyCode] = reservedFundWithCurrencyCode.split(' ');
                        const amount                    = parseFloat(amountStr);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return amount;
                        }

                        const formattedReservedFund = amount.toLocaleString('en-US', { style: 'currency', currency: currencyCode});

                        return `<h5 class="m-0"><span class="badge badge-light">${formattedReservedFund}</span>`;
                    }
                },
                {
                    name: 'po_amount_last2days',
                    data: 'po_amount_last2days',
                    type: 'num',

                    render (po2daysStr, type, row, meta) {
                        const po2daysNotAvailable = !po2daysStr;
                        if (po2daysNotAvailable)
                            return '';

                        const po2days = parseFloat(po2daysStr);

                        const notActive         = (row.status_code == -1);
                        const currencyFormatted = po2days.toLocaleString('en-US', { style: 'currency', currency: 'USD' });

                        if (notActive)
                            return `<h5 class="m-0"><span class="badge text-secondary">${currencyFormatted}</span></h5>`;

                        return `<h5 class="m-0"><span class="badge badge-light">${currencyFormatted}</span></h5>`;
                    }
                },
                {
                    name: 'note',
                    data: 'note',
                    width: 250,
                    className: 'align-middle',
                },
                {
                    name: 'proxy_address',
                    data: 'proxy_address',
                    className: 'align-middle',
                },
                {
                    name: 'latest_order_date',
                    data: 'latest_order_date',
                    width: 75,
                    className: 'align-middle',

                    render (html, type, row, meta) {
                        const innerText = $(html).text().trim();
                        const datetime  = innerText ? moment.unix(innerText) : null;

                        switch (type) {
                            case 'sort':
                            case 'type':
                                const timestamp = (datetime ? datetime.unix() : 0);
                                return timestamp;
                        }

                        if (! datetime)
                            return '';

                        const notActive = (row.status_code == -1);

                        const formatted = (() => {
                            const now = moment();
                            const duration = moment.duration(now.diff(datetime)).asMinutes();

                            return (duration > 1440)
                                ? datetime.format('DD/MM/YYYY HH:mm:ss')
                                : moment.duration(-duration, 'minutes').humanize(true);
                        })();


                        if (notActive)
                            return `<h5 class="m-0"><span class="badge text-secondary">${formatted}</span></h5>`;

                        return `<h5 class="m-0"><span class="badge badge-light">${formatted}</span></h5>`;
                    }
                },
                {
                    name: 'updated_at',
                    data: 'updated_at',
                    width: 75,
                    className: 'align-middle',

                    render (datetimeStr, type, row, data) {
                        const datetime = moment.unix(datetimeStr);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                const timestamp = datetime.unix();
                                return timestamp;
                        }

                        const notActive = (row.status_code == -1);

                        const formatted = (() => {
                            const now = moment();
                            const duration = moment.duration(now.diff(datetime)).asMinutes();

                            return (duration > 1440)
                                ? datetime.utcOffset('+0700').format('DD/MM/YYYY HH:mm:ss')
                                : moment.duration(-duration, 'minutes').humanize(true);
                        })();

                        if (notActive)
                            return `<h5 class="m-0"><span class="badge text-secondary">${formatted}</span></h5>`;

                        return `<h5 class="m-0"><span class="badge badge-light">${formatted}</span></h5>`;
                    }
                }
            ],
        });


        $(tblStripeAccs).on('change', CSS_CLASS_BTN_TOGGLE_LINKABLE, function () {
            const notLinkable = ! this.checked;

            if (notLinkable) {
                const tr = $(this).closest('tr')[0];

                tr.querySelector(CSS_CLASS_BTN_TOGGLE_CURRENT).checked = false;
                tr.querySelector(CSS_CLASS_BTN_TOGGLE_DEFAULT).checked = false;
            }
        });

        $(tblStripeAccs).on('change', CSS_CLASS_BTN_TOGGLE_CURRENT, function () {
            const isCurrent = this.checked;

            if (isCurrent) {
                const tr = $(this).closest('tr')[0];

                tblStripeAccs
                    .querySelectorAll(CSS_CLASS_BTN_TOGGLE_CURRENT)
                    .forEach(input => (input != this) && (input.checked = false));

                const btnToggleLinkable = tr.querySelector(CSS_CLASS_BTN_TOGGLE_LINKABLE);
                btnToggleLinkable.checked = true;
            }
        });

        $(tblStripeAccs).on('change', CSS_CLASS_BTN_TOGGLE_DEFAULT, function () {
            const isDefault = this.checked;

            if (isDefault) {
                const tr                = $(this).closest('tr')[0];
                const btnToggleLinkable = tr.querySelector(CSS_CLASS_BTN_TOGGLE_LINKABLE);

                btnToggleLinkable.checked = true;
            }
        });


        function createCheckboxForLinkableFlag (stripeAccId, isLinkable, rowData) {

            const checkbox = document.createElement('input');
            checkbox.type  = 'checkbox';
            checkbox.name  = 'linkable_stripe_accs_ids[]';
            checkbox.value = stripeAccId;
            checkbox.classList.add(CSS_CLASS_BTN_TOGGLE_LINKABLE.split('.')[1]);

            if (isLinkable)
                checkbox.setAttribute('checked', 'checked');

            return checkbox;
        }

        function createCheckboxForCurrentFlag (stripeAccId, isCurrent, rowData) {

            const checkbox = document.createElement('input');
            checkbox.type  = 'checkbox';
            checkbox.name  = 'current_stripe_acc_id';
            checkbox.value = stripeAccId;
            checkbox.classList.add(CSS_CLASS_BTN_TOGGLE_CURRENT.split('.')[1]);

            if (isCurrent)
                checkbox.setAttribute('checked', 'checked');

            return checkbox;
        }

        function createRadioButtonForDefaultFlag (stripeAccId, isDefaultAcc, rowData) {

            const radioInput = document.createElement('input');
            radioInput.type  = 'radio';
            radioInput.name  = 'default_stripe_acc_id';
            radioInput.value = stripeAccId;
            radioInput.classList.add(CSS_CLASS_BTN_TOGGLE_DEFAULT.split('.')[1]);

            if (isDefaultAcc)
                radioInput.setAttribute('checked', 'checked');

            return radioInput;
        }
    });
</script>
@endpush
