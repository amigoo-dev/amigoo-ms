@extends('backend.layouts.app')

@section('title', "Website #{$website->id}: {$website->name} - Website-moderators Notifications Configuration")

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-0">Website-moderators Notifications Configuration</h4>
            <small>
                For <a href="{{ route('admin.websites.get.form.update', $website) }}">{{ $website->name }}</a>
            </small>
        </div>

        <form id="frm-config" method="POST" action="{{ route('admin.websites.post.form.config-site-mods-notifications', $website) }}" class="card-body">
            @csrf
            <div class="row">
                @foreach ($website->moderators as $mod)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title mb-3">
                                {{ $mod->first_name }} {{ $mod->last_name }}<br>
                                <small>{{ $mod->email }}</small>
                            </h5>
                            @foreach (NotificationType::getAvailableTypes() as $value => $label)
                            <label class="d-block m-0">
                                @if ($mod->enabled_notifications->contains($value))
                                    <input type="checkbox" name="user_id_to_notifications_map[{{$mod->id}}][]" value="{{$value}}" checked>
                                @else
                                    <input type="checkbox" name="user_id_to_notifications_map[{{$mod->id}}][]" value="{{$value}}">
                                @endif
                                <span>{{ $label }}</span>
                            </label>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="text-center">
                <a href="{{ route('admin.websites.get.index', $website) }}" class="btn btn-light btn-sm">Back</a>
                <button class="btn btn-primary btn-sm">Save</button>
            </div>
        </form>
    </div>
@endsection
