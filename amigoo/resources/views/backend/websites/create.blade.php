@extends('backend.layouts.app')

@section('title', 'Website Creation Form'))

@section('content')
    <div class="row">
        <div class="col-8">
            @include('amigoo::backend.websites.partials.form', [
                'title'  => 'Add New Website',
                'action' => route('admin.websites.post.form.create'),
                'canUpdateType' => true,
            ])
        </div>
    </div>
@endsection
