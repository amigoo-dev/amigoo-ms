@extends('backend.layouts.app')

@section('title', 'Active Websites')

@section('content')
<div class="card">
    <div class="card-body">
        @can('create', Amigoo\Database\Models\Website::class)
            <div class="btn-toolbar float-right">
                <a href="{{ route('admin.websites.get.form.create') }}" class="btn btn-success btn-sm ml-1" data-toggle="tooltip" title="" data-original-title="Add New">
                    <i class="fas fa-plus-circle mr-1"></i> <strong>Add New Site</strong>
                </a>
            </div><!--btn-toolbar-->
        @endcan

        <h4 class="card-title mb-0">Active Sites</h4>
    </div>
    @include('amigoo::backend.websites.partials.websites-table', ['websites' => $activeSites])
</div>

<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-0">Non-active Sites</h4>
    </div>
    @include('amigoo::backend.websites.partials.websites-table', ['websites' => $nonActiveSites])
</div>
@endsection
