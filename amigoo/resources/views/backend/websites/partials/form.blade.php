<form action="{{ $action }}" method="POST"
      class="form-horizontal">
    @csrf

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label">Type</label>
                        @if ($canUpdateType)
                            <select class="form-control" name="type">
                                @foreach (WebsiteType::getAvailableStatuses() as $value => $label)
                                    @if (old('type', $website->type) == $value)
                                        <option value="{{ $value }}" selected>{{ $label }}</option>
                                    @else
                                        <option value="{{ $value }}">{{ $label }}</option>
                                    @endif
                                @endforeach
                            </select>
                        @else
                            <h5 class="m-0">
                                <span class="badge badge-light">{{ WebsiteType::getLabel($website->type) }}</span>
                            </h5>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Name</label>
                        <input name="name" type="text" value="{{ old('name', $website->name) }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">URL</label>
                        <input name="url" type="text" value="{{ old('url', $website->url) }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">API Consumer Key</label>
                        <input name="api_consumer_key" type="text" value="{{ old('api_consumer_key', $website->api_consumer_key) }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">API Consumer Secret</label>
                        <input name="api_consumer_secret" type="text" value="{{ old('api_consumer_secret', $website->api_consumer_secret) }}"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Webhook Secret</label>
                        <input name="webhook_secret" type="text" value="{{ old('webhook_secret', $website->webhook_secret) }}"
                               class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    <a class="btn btn-secondary btn-sm" href="{{ route('admin.websites.get.index') }}">Cancel</a>
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right">Save</button>
                </div><!--col-->
            </div><!--row-->
        </div>
    </div>
</form>