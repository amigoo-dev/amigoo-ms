<div class="card">
    <div class="card-body">
        <h4 class="card-titl mb-4">Order payments from this site</h4>

        <h5>
            <span class="d-block mb-1">Summary</span>
            <span class="badge badge-success">
                Total: ${{ number_format($summary['total'], 2) }}
            </span>
        </h5>

        <div class="table-responsive overflow-auto mt-4">
            <table class="table table-sm border border-secondary" id="tbl-orders"
                   data-list-action="{{ route('admin.merchize-orders.ajax.query-by-website', compact('website')) }}">
                <thead class="thead-light">
                    <tr>
                        <th>For Stripe Account</th>
                        <th>Order Code</th>
                        <th>Customer Email</th>
                        <th class="text-right">Total (USD)</th>
                        <th>Created At</th>
                    </tr>
            </table>
        </div>
    </div>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;
        const $tblOrders = $('#tbl-orders');

        const table = $tblOrders.DataTable({
            processing : true,
            serverSide : true,
            ajax       : $tblOrders.data('list-action'),
            searchDelay: 750,

            order: [[4, 'desc']],

            autoWidth: false,
            columns: [
                {
                    name: 'stripe_acc_email',
                    data: 'stripe_account',
                    orderable: false,
                    className: 'align-middle',

                    render (stripeAcc, type, row, meta) {
                        const statusHtml = buildStripeAccStatusHtml(stripeAcc.status);
                        return `<a href="${stripeAcc._links.details}">${stripeAcc.email}</a> ${statusHtml}`;
                    }
                },
                {
                    name: 'order_code',
                    data: 'order_code',
                    className: 'align-middle',
                },
                {
                    name: 'customer_email',
                    data: 'customer_email',
                    className: 'align-middle',
                },
                {
                    name: 'total',
                    data: 'total',
                    className: 'align-middle text-right',

                    render (total, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return total;
                        }

                        const formatted = total.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
                        return `<h5 class="m-0"><code class="badge text-dark">${formatted}</code></h5>`;
                    }
                },
                {
                    name: 'created_at',
                    data: 'created_at',
                    className: 'align-middle',

                    render (createdAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return createdAtStr;
                        }

                        const datetime = moment.utc(createdAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                },
            ],
        });

        function buildStripeAccStatusHtml(statusCode) {
            switch (statusCode) {
                case 'READY':
                    return '<span class="badge badge-success">Ready</span>';
                case 'PENDING':
                    return '<span class="badge badge-light">Pending</span>';
                case 'REACHED_DAILY_MAX':
                    return '<span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">Reached daily</span>';
                case 'WAITING_PAYOUT':
                    return '<span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">Waiting payout</span>';
                case 'WAITING_VERIFY_PHONE':
                    return '<span class="badge badge-warning">Phone ver</span>';
                case 'WAITING_VERIFY_ID':
                    return '<span class="badge badge-warning">ID ver</span>';
                case 'WAITING_VERIFY_IDENTITY':
                    return '<span class="badge badge-warning">Identity ver</span>';
                case 'WAITING_VERIFY_TAX':
                    return '<span class="badge badge-warning">Tax ver</span>';
                case 'WAITING_VERIFY_SITE':
                    return '<span class="badge badge-warning">Site ver</span>';
                case 'WAITING_SUBMIT_W8':
                    return '<span class="badge badge-warning">W-8 Submit</span>';
                case 'NEED_UPDATE_BANK':
                    return '<span class="badge badge-warning">Need update bank</span>';
                case 'FAILED_TOO_MUCH':
                    return '<span class="badge badge-warning">Failed too much</span>';
                case 'NEED_CHECK':
                    return '<span class="badge badge-warning">Need check</span>';
                case 'WAITING_VERIFY_DOC':
                    return '<span class="badge badge-warning">Waiting verify doc</span>';
                case 'WAITING_VERIFY_DOMAIN':
                    return '<span class="badge badge-dark">Domain ver</span>';
                case 'ENDING':
                    return '<span class="badge badge-dark">Ending</span>';
                case 'REFUNDED_ALL':
                    return '<span class="badge badge-dark">Refunded All</span>';
                case 'HOLD_90':
                    return '<span class="badge badge-dark">90 days</span>';
                case 'HOLD_120':
                    return '<span class="badge badge-dark">120 days</span>';
                default:
                    return `(${statusCode})`;
            }
        }

    });
</script>
@endpush