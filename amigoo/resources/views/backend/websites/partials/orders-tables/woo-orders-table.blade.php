<div class="card">
    <div class="card-body">
        <h4 class="card-titl mb-4">Order payments from this site</h4>

        <div>
            <div>
                <h5>Summary</h5>
            </div>
            <table class="table table-sm border border-secondary" style="width: auto;">
                <thead class="thead-light">
                    <tr>
                        <th></th>
                        <th class="text-right">Count</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <h5 class="m-0"><span class="badge badge-success">Processing/Completed</span></h5>
                        </td>
                        <td class="text-right">{{ $summary[OrderStatus::PROCESSING]['count'] + $summary[OrderStatus::COMPLETED]['count'] }}</td>
                        <td class="text-right">${{ number_format($summary[OrderStatus::PROCESSING]['total'] + $summary[OrderStatus::COMPLETED]['total'], 2) }}</td>
                    </tr>
                    <tr>
                        <td>
                            <h5 class="m-0"><span class="badge badge-danger">Failed</span></h5>
                        </td>
                        <td class="text-right">{{ $summary[OrderStatus::FAILED]['count'] }}</td>
                        <td class="text-right">${{ number_format($summary[OrderStatus::FAILED]['total'], 2) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="table-responsive overflow-auto mt-4">
            <table class="table table-sm border border-secondary" id="tbl-orders"
                   data-list-action="{{ route('admin.woo-orders.ajax.query-by-website', compact('website')) }}">
                <thead class="thead-light">
                    <tr>
                        <th>For Stripe Account</th>
                        <th class="text-right">Order ID</th>
                        <th>Order Number</th>
                        <th class="text-center">Status</th>
                        <th class="text-right">Total (USD)</th>
                        <th>Created At</th>
                        <th>Paid At</th>
                    </tr>
            </table>
        </div>
    </div>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;
        const $tblOrders = $('#tbl-orders');

        const colDefs         = getColumnDefinitions();
        const colIdxCreatedAt = colDefs.findIndex(colDef => colDef.name == 'created_at');

        const table = $tblOrders.DataTable({
            processing : true,
            serverSide : true,
            ajax       : $tblOrders.data('list-action'),
            searchDelay: 750,

            order: [[colIdxCreatedAt, 'desc']],

            autoWidth: false,
            columns: colDefs,
        });


        function getColumnDefinitions() {
            return [
                {
                    name     : 'stripe_acc_email',
                    data     : 'stripe_account',
                    orderable: false,
                    className: 'align-middle',

                    render (stripeAcc, type, row, meta) {
                        const statusHtml = buildStripeAccStatusHtml(stripeAcc.status);
                        return `<a href="${stripeAcc._links.details}">${stripeAcc.email}</a> ${statusHtml}`;
                    }
                },
                {
                    name     : 'woo_order_id',
                    data     : 'woo_order_id',
                    className: 'align-middle text-right',
                },
                {
                    name: 'order_number',
                    data: 'order_number',
                    className: 'align-middle',
                },
                {
                    name     : 'status',
                    data     : 'status',
                    className: 'align-middle text-center',

                    render (status, type, row, meta) {
                        switch (status) {
                            case 'completed':
                                return `<h5 class="m-0"><span class="badge badge-success">Completed</span></h5>`;
                            case 'processing':
                                return `<h5 class="m-0"><span class="badge badge-success">Processing</span></h5>`;
                            case 'failed':
                                return `<h5 class="m-0"><span class="badge badge-danger">Failed</span></h5>`;
                        }
                    }
                },
                {
                    name: 'total',
                    data: 'total',
                    className: 'align-middle text-right',

                    render (total, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return total;
                        }

                        const formatted = total.toLocaleString('en-US', {  style: 'currency', currency: 'USD'});
                        return `<h5 class="m-0"><code class="badge text-dark">${formatted}</code></h5>`;
                    }
                },
                {
                    name: 'created_at',
                    data: 'created_at',
                    className: 'align-middle',

                    render (createdAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return createdAtStr;
                        }

                        const datetime = moment.utc(createdAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                },
                {
                    name: 'paid_at',
                    data: 'paid_at',
                    className: 'align-middle',

                    render (paidAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return paidAtStr;
                        }

                        if (! paidAtStr)
                            return '';

                        const datetime = moment.utc(paidAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                }
            ];
        }

        function buildStripeAccStatusHtml(statusCode) {
            switch (statusCode) {
                case 'READY':
                    return '<span class="badge badge-success">Ready</span>';
                case 'PENDING':
                    return '<span class="badge badge-light">Pending</span>';
                case 'REACHED_DAILY_MAX':
                    return '<span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">Reached daily</span>';
                case 'WAITING_PAYOUT':
                    return '<span class="badge" style="background-color: #f2e6ff; color: #8c1aff;">Waiting payout</span>';
                case 'WAITING_VERIFY_PHONE':
                    return '<span class="badge badge-warning">Phone ver</span>';
                case 'WAITING_VERIFY_ID':
                    return '<span class="badge badge-warning">ID ver</span>';
                case 'WAITING_VERIFY_IDENTITY':
                    return '<span class="badge badge-warning">Identity ver</span>';
                case 'WAITING_VERIFY_TAX':
                    return '<span class="badge badge-warning">Tax ver</span>';
                case 'WAITING_VERIFY_SITE':
                    return '<span class="badge badge-warning">Site ver</span>';
                case 'WAITING_SUBMIT_W8':
                    return '<span class="badge badge-warning">W-8 Submit</span>';
                case 'NEED_UPDATE_BANK':
                    return '<span class="badge badge-warning">Need update bank</span>';
                case 'FAILED_TOO_MUCH':
                    return '<span class="badge badge-warning">Failed too much</span>';
                case 'NEED_CHECK':
                    return '<span class="badge badge-warning">Need check</span>';
                case 'WAITING_VERIFY_DOC':
                    return '<span class="badge badge-warning">Waiting verify doc</span>';
                case 'WAITING_VERIFY_DOMAIN':
                    return '<span class="badge badge-dark">Domain ver</span>';
                case 'ENDING':
                    return '<span class="badge badge-dark">Ending</span>';
                case 'REFUNDED_ALL':
                    return '<span class="badge badge-dark">Refunded All</span>';
                case 'HOLD_90':
                    return '<span class="badge badge-dark">90 days</span>';
                case 'HOLD_120':
                    return '<span class="badge badge-dark">120 days</span>';
                default:
                    return `(${statusCode})`;
            }
        }

    });
</script>
@endpush