<div class="card">
    <div class="card-body">
        <h4 class="card-titl mb-4">Payment method sessions</h4>

        <div class="table-responsive overflow-auto mt-4">
            <table class="table table-sm border border-secondary" id="tbl-payment-method-sessions"
                   data-list-action="{{ route('admin.websites.ajax.payment-method-sessions.get', compact('website')) }}">
                <thead class="thead-light">
                    <tr>
                        <th>Payment Method</th>
                        <th>Payment Account</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
            </table>
        </div>
    </div>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;
        const $tblOrders = $('#tbl-payment-method-sessions');

        const table = $tblOrders.DataTable({
            processing : true,
            serverSide : true,
            ajax       : $tblOrders.data('list-action'),

            order: [[2, 'desc']],
            orderFixed: {
                pre: [2, 'desc'],
            },
            ordering: false,

            searching: false,

            columns: [
                {
                    name: 'payment_method',
                    data: 'payment_method',
                    width: 100,
                    className: 'align-middle',

                    render (paymentMethod, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return paymentMethod;
                        }

                        return '<i class="fab fa-cc-stripe fa-2x"></i>';
                    }
                },
                {
                    name: 'payment_account',
                    data: 'payment_account',
                    className: 'align-middle',

                    render (paymentMethod, type, row, meta) {
                        if (row.payment_method != 'stripe')
                            return 'NOT Stripe';

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return paymentMethod.email;
                        }

                        return `<a href="${paymentMethod._links.details}" target="_blank">${paymentMethod.email}</a>`;
                    }
                },
                {
                    name: 'start_at',
                    data: 'start_at',
                    width: 100,
                    className: 'align-middle',

                    render (startAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return startAtStr;
                        }

                        const datetime = moment.utc(startAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                },
                {
                    name: 'end_at',
                    data: 'end_at',
                    width: 100,
                    className: 'align-middle',

                    render (endAtStr, type, row, meta) {
                        switch (type) {
                            case 'sort':
                            case 'type':
                                return endAtStr;
                        }

                        if (! endAtStr)
                            return '';

                        const datetime = moment.utc(endAtStr)
                            .add(7, 'hours')
                            .format('DD/MM/YYYY HH:mm:ss');

                        return `${datetime} (Hanoi)`;
                    }
                }
            ],
        });
    });

</script>
@endpush