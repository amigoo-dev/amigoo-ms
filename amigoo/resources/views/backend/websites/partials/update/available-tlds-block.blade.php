<div class="card">
    <div class="card-body">
        <h4 class="card-title">Available TLDs</h4>

        <form action="{{ route('admin.websites.ajax.available-tlds.post', $website) }}" method="POST"
              id="frm-website-tld" class="form-inline mb-2">

            <input name="tld" type="text" class="form-control form-control-sm">
            <button class="btn btn-light btn-sm ml-2">
                <i class="fas fa-plus"></i>
            </button>
        </form>

        <table id="tbl-available-tlds">
            <tbody>
                @foreach ($website->availableTlds as $tldRecord)
                <tr>
                    <td>
                        <a href-delete={{ route('admin.websites.ajax.available-tlds.delete', compact('website', 'tldRecord')) }}
                           href="javascript:void(0)" class="btn-delete-site-tld mr-2">
                            <i class="fas fa-trash"></i>
                        </a>
                        <span class="badge badge-light">{{ $tldRecord->tld }}</span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;

        const frmWebsiteTld         = document.querySelector('#frm-website-tld');
        const tblAvailableTlds      = document.querySelector('#tbl-available-tlds');
        const URL_ADD_AVAILABLE_TLD = frmWebsiteTld.action;

        $(frmWebsiteTld).submit(
            function () {
                callAjaxCreateSiteTld();
                return false; // Stop form submit
            }
        );

        $(tblAvailableTlds).on('click', '.btn-delete-site-tld',
            function () {
                const anchorElmt = this;
                callAjaxDeleteSiteTld(anchorElmt);
            }
        );

        function callAjaxCreateSiteTld () {
            $.ajax({
                url : URL_ADD_AVAILABLE_TLD,
                type: 'POST',
                data: {
                    _token: CSRF_TOKEN,
                    tld   : frmWebsiteTld.tld.value
                },
                success (payload) {
                    insertNewRow(
                        payload.data.tld,
                        payload.data.deleteUrl
                    );
                },
                error (jqxhr) {
                    const json   = jqxhr.responseJSON;
                    const reason = json.reason;
                    alert(`Could not add new TLD: ${reason}`);
                },
                complete () {
                    frmWebsiteTld.reset();
                }
            });
        }

        function callAjaxDeleteSiteTld (anchorElmt) {
            const endpoint = anchorElmt.getAttribute('href-delete');
            const rowElmt  = $(anchorElmt).closest('tr')[0];

            $.ajax({
                url : endpoint,
                type: 'DELETE',
                data: {
                    _token: CSRF_TOKEN
                },
                success (payload) {
                    removeRow(rowElmt);
                },
                error (jqxhr) {
                    const json   = jqxhr.responseJSON;
                    const reason = json.reason;
                    alert(`Could not add new TLD: ${reason}`);
                }
            });
        }

        function insertNewRow (tld, deleteUrl) {
            const row = document.createElement('tr');
            tblAvailableTlds.querySelector('tbody').appendChild(row);

            const cell = document.createElement('td');
            row.appendChild(cell);

            const linkForDelete = document.createElement('a');
            linkForDelete.setAttribute('href-delete', deleteUrl);
            linkForDelete.href      = 'javascript:void(0)';
            linkForDelete.className = 'btn-delete-site-tld mr-2';
            linkForDelete.innerHTML = '<i class = "fas fa-trash"></i>';
            cell.appendChild(linkForDelete);

            const linkForSite = document.createElement('span');
            linkForSite.className = 'badge badge-light';
            linkForSite.innerText = tld;
            cell.appendChild(linkForSite);
        }

        function removeRow (rowElmt) {
            const $tbody = $(rowElmt).closest('tbody');
            $tbody[0].removeChild(rowElmt);
        }
    });
</script>
@endpush
