<!-- Deactivation Form -->
<form action="{{ route('admin.websites.post.deactivate', compact('website')) }}" method="POST"
      onsubmit="return prompt('Type &quot;DEACTIVATE&quot; to deactivate this account') == 'DEACTIVATE'">
    @csrf
    <div class="card">
        <div class="card-body">
            <button class="btn btn-sm btn-light float-right">Deactivate</button>
            <h4 class="card-title mb-0">Deactivation</h4>
        </div>
    </div>
</form>