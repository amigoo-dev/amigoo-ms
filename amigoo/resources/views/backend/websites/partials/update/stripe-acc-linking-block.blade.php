<div class="card">
    <div class="card-body">
        <form name="frm-link"
              action="{{ route('admin.websites.post.link-to-stripe-acc', compact('website')) }}" method="POST">
            @csrf

            @can('configLinkableStripeAccs', $website)
                <div class="float-right">
                    <a href="{{ route('admin.websites.get.form.config-linkables', $website) }}" class="btn btn-sm btn-light">Config Linkables</a>
                </div>
            @endcan

            <h4 class="card-title mb">Current Stripe Account</h4>

            <p>
                <select name="stripe_acc_id" class="form-control">
                    <option disabled selected value>-- None --</option>
                    @forelse ($linkableStripeAccs as $stripeAcc)
                        @if ($stripeAcc->id == $website->stripe_acc_id)
                            <option value="{{ $stripeAcc->id }}" selected>
                                {{ $stripeAcc->email }} ({{ $stripeAcc->owner_name }})
                            </option>
                        @else
                            <option value="{{ $stripeAcc->id }}">
                                {{ $stripeAcc->email }} ({{ $stripeAcc->owner_name }})
                            </option>
                        @endif
                    @empty
                        <option disabled>No available Stripe accounts</option>
                    @endforelse
                </select>
            </p>

            <div class="clearfix">
                <button form="frm-unlink" class="btn btn-sm btn-light">Unlink</button>

                <div class="float-right">
                    <button class="btn btn-sm btn-warning">Link</button>
                </div>
            </div>
        </form>

        <hr class="mx-n3">

        <form action="{{ route('admin.websites.post.update-auto-switch-period', compact('website')) }}" method="POST">
            @csrf

            <div class="form-group">
                <label>Auto-switch after (minutes)</label>
                <input name="auto_switch_after_mins" type="number" value="{{ old('auto_switch_after_mins', $website->auto_switch_after_mins) }}"
                       min="0"
                       class="form-control">
            </div>

            <div class="text-right">
                <button class="btn btn-sm btn-warning">Save</button>
            </div>
        </form>

        <form action="{{ route('admin.websites.post.update-change-stripe-status-failed-amount', compact('website')) }}" method="POST">
            @csrf

            <div class="form-group">
                <label>Change-Stripe-status failed amount</label>
                <input name="change_stripe_status_failed_amount" type="number" value="{{ old('change_stripe_status_failed_amount', $website->change_stripe_status_failed_amount) }}"
                       min="1"
                       class="form-control">
            </div>

            <div class="text-right">
                <button class="btn btn-sm btn-warning">Save</button>
            </div>
        </form>

    </div>
</div>

<!-- This form is NOT VISIBLE & USED BY the "Unlink" button, which is in the form above -->
<form id="frm-unlink"
      action="{{ route('admin.websites.post.unlink-from-stripe-acc', compact('website')) }}" method="post"
      onsubmit="return prompt('Type &quot;UNLINK&quot; to unlink') == 'UNLINK'">
    @csrf
</form>