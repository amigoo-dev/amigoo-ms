<?php $randomCode = rand() ?>

<div class="table-responsive overflow-auto">
    <table id="tbl-websites-{{$randomCode}}" class="table table-sm border-bottom">
        <thead class="thead-light">
            <th>Name</th>
            <th class="text-center">Ready Accs.</th>
            <th>Today</th>
            <th>Latest Order</th>
            <th class="text-center">Orders Count</th>
            <th>Total Received</th>
            <th>Reserved Fund</th>
            <th class="text-center">Status</th>
        </thead>
        <tbody>
            @forelse ($websites as $website)
                <tr {!! $website->trashed() ? 'class="table-secondary"' : '' !!}>
                    <td class="align-middle">
                        @unless ($website->trashed())
                            <a href="{{ route('admin.websites.get.form.update', compact('website')) }}">
                                <strong>{{ $website->name }}</strong>
                            </a>
                            <br>
                            @can('configSiteModsNotifications', Amigoo\Database\Models\Website::class)
                                <a href="{{ route('admin.websites.get.form.config-site-mods-notifications', $website) }}"
                                    target="_blank"
                                    class="badge badge-light">Config Notifications</a>
                            @endcan
                            @can('configLinkableStripeAccs', Amigoo\Database\Models\Website::class)
                                <a href="{{ route('admin.websites.get.form.config-linkables', $website) }}"
                                    target="_blank"
                                    class="badge badge-light">Config Linkables</a>
                            @endcan
                        @else
                            <span>{{ $website->name }}</span>
                        @endunless
                    </td>
                    <td class="align-middle text-center">
                        <h5 class="m-0">
                            @if ($website->trashed())
                                <span class="badge text-secondary">{{ $website->linkableStripeAccounts->filter(fn($acc) => ($acc->status == StripeAccountStatus::READY))->count() }}</span>
                            @else
                                <span class="badge badge-primary">{{ $website->linkableStripeAccounts->filter(fn($acc) => ($acc->status == StripeAccountStatus::READY))->count() }}</span>
                            @endif
                        </h5>
                    </td>
                    <td class="align-middle">{{ $website->todayReceivedAmount }}/{{ $website->totalDailyMaxReceivedAmount }}</td>
                    <td class="align-middle">
                        @if ($website->latestOrder)
                            @if ($website->trashed())
                                <span class="text-secondary">
                                    {{ $website->latestOrder->created_at->timestamp }}
                                </span>
                            @else
                                <span>
                                    {{ $website->latestOrder->created_at->timestamp }}
                                </span>
                            @endif
                        @endif
                    </td>
                    <td class="align-middle text-center">
                        <h5 class="m-0">
                            @if ($website->type == WebsiteType::WOO)
                                <span class="processing badge badge-{{ $website->trashed() ? 'secondary' : 'success' }} text-right" style="width: 40px">
                                    {{ $website->summary[OrderStatus::PROCESSING]['count'] + $website->summary[OrderStatus::COMPLETED]['count']  }}</span>
                                <span class="failed badge badge-{{ $website->trashed() ? 'secondary' : 'danger' }} text-left" style="width: 40px">
                                    {{ $website->summary[OrderStatus::FAILED]['count'] }}</span>

                            @elseif ($website->type == WebsiteType::MERCHIZE)
                                <span class="processing badge badge-{{ $website->trashed() ? 'secondary' : 'success' }} text-right" style="width: 40px">
                                    {{ $website->summary['count'] }}</span>
                                <span class="invisible badge" style="width: 40px">&nbsp;</span>
                            @endif
                        </h5>
                    </td>
                    <td class="align-middle">
                        @if ($website->type == WebsiteType::WOO)
                            {{ $website->summary[OrderStatus::PROCESSING]['total'] + $website->summary[OrderStatus::COMPLETED]['total'] }}
                        @elseif ($website->type == WebsiteType::MERCHIZE)
                            {{ $website->summary['total'] }}
                        @endif
                    </td>
                    <td class="align-middle">
                        @if ($website->totalReservedFund)
                            {{ json_encode($website->totalReservedFund) }}
                        @endif
                    </td>
                    <td class="align-middle text-center">
                        @if ($website->trashed())
                            -1
                        @elseif ($website->stripeAccount)
                            1
                        @else
                            0
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">No available websites</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>


@push('after-scripts')
<script>
    $(() => {
        const CSRF_TOKEN   = document.querySelector('meta[name = csrf-token]').content;
        const $tblWebsites = $('#tbl-websites-{{$randomCode}}');

        const columnsConfigs = getColumnsConfigs();
        const colNameIdx     = columnsConfigs.findIndex(colConfig => colConfig.name == 'site_name');
        const colStatusIdx   = columnsConfigs.findIndex(colConfig => colConfig.name == 'status');

        const table = $tblWebsites.DataTable({
            searching: false,
            paging   : false,
            info     : false,
            autoWidth: false,

            order: [
                [colStatusIdx, 'desc'],
                [colNameIdx, 'asc']
            ],
            orderFixed: {
                pre: [colStatusIdx, 'desc'],
            },

            columns: columnsConfigs,
        });


        function getColumnsConfigs () {
            return [
                {
                    name: 'site_name',
                    data: 'site_anchor',

                    render (siteAnchor, type, row, meta) {
                        const siteName = $(siteAnchor).text().trim();

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return siteName;
                        }

                        return siteAnchor;
                    }
                },
                {
                    name: 'ready_accs_count',
                    data: 'ready_accs_count',
                },
                {
                    name: 'today_received',
                    data: 'today_received',
                    class: 'text-right',

                    render (todayReceivedByDailyLimit, type, row, meta) {
                        const [todayReceived, dailyLimit] = todayReceivedByDailyLimit.trim().split('/').map(parseFloat);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return todayReceived;
                        }

                        const notActive              = (row.status_code == -1);
                        const formattedTodayReceived = todayReceived.toLocaleString('en-US', { style: 'currency', currency: 'USD'});
                        const formattedDailyLimit    = dailyLimit.toLocaleString('en-US', { style: 'currency', currency: 'USD'});

                        if (notActive)
                            return `<code><h5 class="m-0"><span class="badge text-secondary">${formattedTodayReceived}</span></h5></code>`;

                        return `<code><h5 class="m-0"><span class="badge badge-light">${formattedTodayReceived}</span><br><span class="badge text-secondary">${formattedDailyLimit}</span></h5></code>`;
                    }
                },
                {
                    name: 'latest_order_date',
                    data: 'latest_order_date',
                    class: 'text-right',

                    render (html, type, row, meta) {
                        const innerText = $(html).text().trim();
                        const datetime  = innerText ? moment.unix(innerText) : null;

                        switch (type) {
                            case 'sort':
                            case 'type':
                                const timestamp = (datetime ? datetime.unix() : 0);
                                return timestamp;
                        }

                        if (! datetime)
                            return '';

                        const notActive = (row.status_code == -1);

                        const formatted = (() => {
                            const now = moment();
                            const duration = moment.duration(now.diff(datetime)).asMinutes();

                            return (duration > 1440)
                                ? datetime.format('DD/MM/YYYY HH:mm:ss')
                                : moment.duration(-duration, 'minutes').humanize(true);
                        })();


                        if (notActive)
                            return `<h5 class="m-0"><span class="badge text-secondary">${formatted}</span></h5>`;

                        return `<h5 class="m-0"><span class="badge badge-light">${formatted}</span></h5>`;
                    }
                },
                {
                    name: 'order_results_counts',
                    data: 'order_results_counts_html',

                    render (html, type, row, meta) {
                        const successBadge = $(html).children()[0];
                        const successCount = parseInt(successBadge.innerText);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return successCount;
                        }

                        return html;
                    }
                },
                {
                    name: 'total_received',
                    data: 'total_received',
                    class: 'text-right',

                    render (totalReceived, type, row, meta) {
                        totalReceived = parseFloat(totalReceived);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return totalReceived;
                        }

                        const notActive         = (row.status_code == -1);
                        const currencyFormatted = totalReceived.toLocaleString('en-US', { style: 'currency', currency: 'USD'});

                        if (notActive)
                            return `<code><h5 class="m-0"><span class="badge text-secondary">${currencyFormatted}</span></h5></code>`;

                        return `<code><h5 class="m-0"><span class="badge badge-light">${currencyFormatted}</span></h5></code>`;
                    }
                },
                {
                    name: 'reserved_fund',
                    data: 'reserved_fund',
                    class: 'text-right',
                    orderable: false,

                    render (reservedFundMapJson, type, row, meta) {
                        const reservedFundNotAvailable = !reservedFundMapJson;
                        if (reservedFundNotAvailable)
                            return '';

                        const reservedFundMap = JSON.parse(reservedFundMapJson);

                        let content = '';
                        for (const currencyCode in reservedFundMap) {
                            const amount            = reservedFundMap[currencyCode];
                            const notActive         = (row.status_code == -1);
                            const currencyFormatted = amount.toLocaleString('en-US', { style: 'currency', currency: currencyCode });

                            if (notActive) {
                                content += `<h5 class="m-0"><span class="badge text-secondary">${currencyFormatted}</span></h5>`;
                            } else {
                                content += `<h5 class="m-0"><span class="badge badge-light">${currencyFormatted}</span></h5>`;
                            }
                        }

                        return `<code>${content}</code>`;
                    }
                },
                {
                    name: 'status',
                    data: 'status_code',

                    render (statusCode, type, row, meta) {
                        statusCode = parseInt(statusCode);
                        const label = getStatusLabel(statusCode);

                        switch (type) {
                            case 'sort':
                            case 'type':
                                return statusCode;
                        }

                        switch (statusCode) {
                            case -1:
                                return "<h5 class='m-0'><span class='badge text-secondary'>In-active</span></h5>";
                            case 0:
                                return "<h5 class='m-0'><span class='badge badge-dark'>Un-linked</span></h5>";
                            case 1:
                                return "<h5 class='m-0'><span class='badge badge-primary'>Linked</span></h5>";
                        }
                        return statusCode;
                    }
                },
            ];
        }

        function getStatusLabel (statusCode) {
            switch (statusCode) {
                case -1:
                    return 'In-active';
                case 0:
                    return 'Un-linked';
                case 1:
                    return 'Linked';
            }

            return statusCode;
        }
    });
</script>
@endpush