@extends('backend.layouts.app')

@section('title', "Website #{$website->id}: {$website->name}")

@section('content')
    <div class="row">
        <div class="col-md-8">
            @include('amigoo::backend.websites.partials.form', [
                'title'  => "Website #{$website->id}: {$website->name}",
                'action' => route('admin.websites.post.form.update', compact('website')),
                'canUpdateType' => false,
            ])
        </div>

        <div class="col-md-4">
            <div class="row">
                <div class="col-sm col-lg-12">
                    @include('amigoo::backend.websites.partials.update.stripe-acc-linking-block')
                </div>
                <div class="col-sm col-lg-12">
                    @include('amigoo::backend.websites.partials.update.available-tlds-block')
                </div>
            </div>
        </div>
    </div>

    @if ($website->type == WebsiteType::WOO)
        @include('amigoo::backend.websites.partials.orders-tables.woo-orders-table', [
            'orders' => $website->wooOrders,
        ])
    @elseif ($website->type == WebsiteType::MERCHIZE)
        @include('amigoo::backend.websites.partials.orders-tables.merchize-orders-table', [
            'orders' => $website->merchizeOrders,
        ])
    @endif

    @include('amigoo::backend.websites.partials.payment-method-sessions-table', [
        'orders' => $website->wooOrders,
    ])

    @include('amigoo::backend.websites.partials.update.deactivation-block')

@endsection
