<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8" />
    </head>
    <body>
        <ul>
            <li>
                <strong>Stripe</strong>
                {{ $stripeAcc->email }}
            </li>
            <li>
                <strong>Status</strong>
                {{ $stripeAcc->status }}
            </li>
            <li>
                <strong>Website</strong>
                {{ $website->name }}
            </li>
            <li>
                <strong>Today Received</strong>
                {{ $formattedTodayReceived }}
            </li>
            <li>
                <strong>Total Received</strong>
                {{ $formattedTotalReceived }}
            </li>
            <li>
                <strong>Latest Order</strong>
                {{ $latestOrderAt ?: 'N/A' }} (GMT)
            </li>
        </ul>
    </body>
</html>
