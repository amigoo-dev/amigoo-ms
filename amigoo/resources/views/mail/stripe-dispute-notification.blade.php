<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8" />
    </head>
    <body>
        <ul>
            <li>
                <strong>Status</strong>
                {{ StripeDisputeStatus::getLabel($dispute->status) }}
            </li>
            <li>
                <strong>Website</strong>
                {{ $dispute->website->name }}
            </li>
            <li>
                <strong>Stripe Account</strong>
                {{ $dispute->stripeAccount->email }} ({{ $dispute->stripeAccount->owner_name }})
            </li>
            <li>
                <strong>Amount</strong>
                {{ $formattedAmount }}
            </li>
            <li>
                <strong>Reason</strong>
                {{ $dispute->reason }}
            </li>
        </ul>
    </body>
</html>
