<?php

use Amigoo\Http\Controllers\Api\MerchizeApiController;

Route::group([
    'prefix' => 'merchize',
    'as'     => 'merchize.',
], function() {

    Route::get('current-payment-gateway', [MerchizeApiController::class, 'getCurrentPaymentGateway'])
        ->name('current-payment-gateway');

    Route::get('orders/{orderCode}', [MerchizeApiController::class, 'getOrderDetailsByOrderCode'])
        ->name('order-details');
});