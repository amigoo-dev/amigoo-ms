<?php

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Http\Controllers\Backend\WooOrdersAjaxController;
use Amigoo\Http\Controllers\Backend\MerchizeOrdersAjaxController;

Route::group([
    'prefix' => 'woo-orders',
    'as'     => 'woo-orders.',
], function() {

    Route::group([
        'prefix' => 'ajax',
        'as'     => 'ajax.',
    ], function() {

        Route::get('/query-by-website/{website}', [WooOrdersAjaxController::class, 'getOrdersByWebsite'])
            ->name('query-by-website')
            ->middleware('can:viewOrderPaymentsSummaryAndList,'.StripeAccount::class);

        Route::get('/query-by-stripe-acc/{acc}', [WooOrdersAjaxController::class, 'getOrdersByStripeAcc'])
            ->name('query-by-stripe-acc')
            ->middleware('can:viewOrderPaymentsSummaryAndList,'.StripeAccount::class);
    });
});

Route::group([
    'prefix' => 'merchize-orders',
    'as'     => 'merchize-orders.',
], function() {

    Route::group([
        'prefix' => 'ajax',
        'as'     => 'ajax.',
    ], function() {

        Route::get('/query-by-website/{website}', [MerchizeOrdersAjaxController::class, 'getOrdersByWebsite'])
            ->name('query-by-website')
            ->middleware('can:viewOrderPaymentsSummaryAndList,'.StripeAccount::class);

        Route::get('/query-by-stripe-acc/{acc}', [MerchizeOrdersAjaxController::class, 'getOrdersByStripeAcc'])
            ->name('query-by-stripe-acc')
            ->middleware('can:viewOrderPaymentsSummaryAndList,'.StripeAccount::class);
    });
});
