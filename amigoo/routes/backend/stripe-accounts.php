<?php

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Http\Controllers\Backend\StripeAccsAjaxController;
use Amigoo\Http\Controllers\Backend\StripeAccsController;
use Amigoo\Http\Controllers\Backend\StripeAccProxyConfigController;

Route::group([
    'prefix' => 'stripe-accs',
    'as' => 'stripe-accs.'
], function () {

    // FOR ADMIN

    Route::get('/new', [StripeAccsController::class, 'getCreationForm'])
        ->name('get.form.create')
        ->middleware('can:create,'.StripeAccount::class);

    Route::post('/new', [StripeAccsController::class, 'postCreationForm'])
        ->name('post.form.create')
        ->middleware('can:create,'.StripeAccount::class);

    Route::post('/{acc}/proxy-config', [StripeAccProxyConfigController::class, 'saveConfig'])
        ->name('post.form.proxy-config')
        ->middleware('can:saveProxyConfig,'.StripeAccount::class);


    // FOR MODERATOR

    Route::get('/', [StripeAccsController::class, 'getIndex'])
        ->name('get.index')
        ->middleware('can:viewIndex,'.StripeAccount::class);

    Route::get('/{acc}', [StripeAccsController::class, 'getUpdationForm'])
        ->name('get.form.update')
        ->middleware('can:view,acc');

    Route::post('/{acc}', [StripeAccsController::class, 'postUpdationForm'])
        ->name('post.form.update')
        ->middleware('can:update,acc');

    // AJAX

    Route::group([
        'prefix' => 'ajax',
        'as'     => 'ajax.',
    ], function () {

        Route::get('query', [StripeAccsAjaxController::class, 'getStripeAccounts'])
            ->name('query')
            ->middleware('can:viewIndex,'.StripeAccount::class);
    });
});
