<?php

use Amigoo\Database\Models\StripeDispute;
use Amigoo\Http\Controllers\Backend\StripeDisputesController;

Route::group([
    'prefix' => 'stripe-disputes',
    'as' => 'stripe-disputes.'
], function () {

    // FOR MODERATOR

    Route::get('/', [StripeDisputesController::class, 'getIndex'])
        ->name('get.index')
        ->middleware('can:viewIndex,'.StripeDispute::class);

    Route::get('/{dispute}', [StripeDisputesController::class, 'getUpdationForm'])
        ->name('get.form.update')
        ->middleware('can:view,'.StripeDispute::class);

    Route::post('/{dispute}', [StripeDisputesController::class, 'postUpdationForm'])
        ->name('post.form.update')
        ->middleware('can:update,'.StripeDispute::class);
});
