<?php

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Http\Controllers\Backend\StripePayoutsAjaxController;

Route::group([
    'prefix' => 'stripe-payouts',
    'as'     => 'stripe-payouts.',
], function() {

    Route::group([
        'prefix' => 'ajax',
        'as'     => 'ajax.',
    ], function() {

        Route::get('/query-by-stripe-acc/{acc}', [StripePayoutsAjaxController::class, 'getPayoutsByStripeAcc'])
            ->name('query-by-stripe-acc')
            ->middleware('can:viewStripePayouts,acc')
            ;

        Route::post('/run-cmd-retrieve-all/{acc}', [StripePayoutsAjaxController::class, 'runCommandRetrieveAllPayouts'])
            ->name('run-cmd-retrieve-all')
            ->middleware('can:viewStripePayouts,acc')
            ;
    });
});