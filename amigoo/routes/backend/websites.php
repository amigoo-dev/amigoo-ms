<?php

use Amigoo\Database\Models\Website;
use Amigoo\Http\Controllers\Backend\ConfigSiteModsNotificationsController;
use Amigoo\Http\Controllers\Backend\WebsitesController;
use Amigoo\Http\Controllers\Backend\WebsitesAjaxController;

Route::group([
    'prefix' => 'websites',
    'as' => 'websites.'
], function () {

    // FOR ADMIN

    Route::get('/new', [WebsitesController::class, 'getCreationForm'])
        ->name('get.form.create')
        ->middleware('can:create,'.Website::class);

    Route::post('/new', [WebsitesController::class, 'postCreationForm'])
        ->name('post.form.create')
        ->middleware('can:create,'.Website::class);

    Route::get('/{website}/config-site-mods-notifications', [ConfigSiteModsNotificationsController::class, 'show'])
        ->name('get.form.config-site-mods-notifications')
        ->middleware('can:configSiteModsNotifications');

    Route::post('/{website}/config-site-mods-notifications', [ConfigSiteModsNotificationsController::class, 'submit'])
        ->name('post.form.config-site-mods-notifications')
        ->middleware('can:configSiteModsNotifications');


    // FOR MODERATOR

    Route::get('/{website}/config-linkables', [WebsitesController::class, 'getConfigLinkablesForm'])
        ->name('get.form.config-linkables')
        ->middleware('can:configLinkableStripeAccs,website');

    Route::post('/{website}/config-linkables', [WebsitesController::class, 'postConfigLinkablesForm'])
        ->name('post.form.config-linkables')
        ->middleware('can:configLinkableStripeAccs,website');

    Route::get('/', [WebsitesController::class, 'getIndex'])
        ->name('get.index')
        ->middleware('can:viewIndex,'.Website::class);

    Route::get('/{website}', [WebsitesController::class, 'getUpdationForm'])
        ->name('get.form.update')
        ->middleware('can:update,website');

    Route::post('/{website}', [WebsitesController::class, 'postUpdationForm'])
        ->name('post.form.update')
        ->middleware('can:update,website');

    Route::post('/{website}/activate', [WebsitesController::class, 'activate'])
        ->name('post.activate')
        ->middleware('can:restore,website');

    Route::post('/{website}/deactivate', [WebsitesController::class, 'deactivate'])
        ->name('post.deactivate')
        ->middleware('can:delete,website');

    Route::post('/{website}/link-to-stripe-acc', [WebsitesController::class, 'linkToStripeAcc'])
        ->name('post.link-to-stripe-acc')
        ->middleware('can:update,website');

    Route::post('/{website}/unlink-from-stripe-acc', [WebsitesController::class, 'unlinkFromStripeAcc'])
        ->name('post.unlink-from-stripe-acc')
        ->middleware('can:update,website');

    Route::post('/{website}/update-auto-switch-period', [WebsitesController::class, 'updateAutoSwitchPeriod'])
        ->name('post.update-auto-switch-period')
        ->middleware('can:update,website');

    Route::post('/{website}/update-change-stripe-status-failed-amount', [WebsitesController::class, 'updateChangeStripeStatusFailedAmount'])
        ->name('post.update-change-stripe-status-failed-amount')
        ->middleware('can:update,website');

    // AJAX

    Route::post('/{website}/available-tlds', [WebsitesAjaxController::class, 'addAvailableTld'])
        ->name('ajax.available-tlds.post')
        ->middleware('can:update,website');

    Route::delete('/{website}/available-tlds/{tldRecord}', [WebsitesAjaxController::class, 'removeAvailableTld'])
        ->name('ajax.available-tlds.delete')
        ->middleware('can:update,website');

    Route::get('/{website}/payment-method-sessions', [WebsitesAjaxController::class, 'getPaymentMethodSessions'])
        ->name('ajax.payment-method-sessions.get')
        ->middleware('can:update,website');
});
