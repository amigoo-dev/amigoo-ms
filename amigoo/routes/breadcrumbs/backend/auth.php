<?php

Breadcrumbs::for('admin.auth.user.config-moderatable-websites.show', function ($trail) {
    $trail->parent('admin.auth.user.index');
    $trail->push('Moderatable Websites Configuration', route('admin.stripe-accs.get.index'));
});
