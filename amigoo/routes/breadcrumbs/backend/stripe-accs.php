<?php

Breadcrumbs::for('admin.stripe-accs.get.index', function ($trail) {
    $trail->push('Stripe Accounts', route('admin.stripe-accs.get.index'));
});

Breadcrumbs::for('admin.stripe-accs.get.form.create', function ($trail) {
    $trail->parent('admin.stripe-accs.get.index');
    $trail->push('Stripe Account Creation Form', route('admin.stripe-accs.get.form.create'));
});

Breadcrumbs::for('admin.stripe-accs.get.form.update', function ($trail, $acc) {
    $trail->parent('admin.stripe-accs.get.index');
    $trail->push("{$acc->email} ({$acc->owner_name})", route('admin.stripe-accs.get.form.update', compact('acc')));
});
