<?php

Breadcrumbs::for('admin.stripe-disputes.get.index', function ($trail) {
    $trail->push('Stripe Disputes', route('admin.stripe-disputes.get.index'));
});

Breadcrumbs::for('admin.stripe-disputes.get.form.update', function ($trail, $dispute) {
    $trail->parent('admin.stripe-disputes.get.index');
    $trail->push($dispute->stripe_payment_id, route('admin.stripe-disputes.get.form.update', compact('dispute')));
});
