<?php

Breadcrumbs::for('admin.websites.get.index', function ($trail) {
    $trail->push('Active Websites', route('admin.websites.get.index'));
});

Breadcrumbs::for('admin.websites.get.form.create', function ($trail) {
    $trail->parent('admin.websites.get.index');
    $trail->push('Website Creation Form', route('admin.websites.get.form.create'));
});

Breadcrumbs::for('admin.websites.get.form.update', function ($trail, $website) {
    $trail->parent('admin.websites.get.index');
    $trail->push($website->name, route('admin.websites.get.form.update', compact('website')));
});

Breadcrumbs::for('admin.websites.get.form.config-linkables', function ($trail, $website) {
    $trail->parent('admin.websites.get.form.update', $website);
    $trail->push('Linkable Stripe Accounts Configuration', route('admin.websites.get.form.config-linkables', compact('website')));
});

Breadcrumbs::for('admin.websites.get.form.config-site-mods-notifications', function ($trail, $website) {
    $trail->parent('admin.websites.get.form.update', $website);
    $trail->push('Website-moderator Notifications Configuration', route('admin.websites.get.form.config-site-mods-notifications', compact('website')));
});
