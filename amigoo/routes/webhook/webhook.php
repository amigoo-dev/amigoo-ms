<?php

use Amigoo\Http\Controllers\Webhook\Mailgun\InboundWebhookControllerForStripeAccStatus;
use Amigoo\Http\Controllers\Webhook\Mailgun\InboundWebhookControllerForStripeDispute;
use Amigoo\Http\Controllers\Webhook\Mailgun\InboundWebhookControllerForStripeOrder;
use Amigoo\Http\Controllers\Webhook\WooCommerce\WebhookController as WooCommerceWebhook;

Route::group([
    'prefix' => 'webhook',
    'as'     => 'webhook.',
], function() {

    Route::post('/mailgun', [InboundWebhookControllerForStripeAccStatus::class, '__invoke'])
        ->name('mailgun');

    Route::post('/mailgun/stripe-dispute', [InboundWebhookControllerForStripeDispute::class, '__invoke'])
        ->name('mailgun.stripe-dispute');

    Route::post('/mailgun/stripe-order', [InboundWebhookControllerForStripeOrder::class, '__invoke'])
        ->name('mailgun.stripe-order');

    Route::post('/woo', [WooCommerceWebhook::class, '__invoke'])
        ->name('woo');
});
