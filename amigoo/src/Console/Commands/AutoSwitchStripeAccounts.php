<?php

namespace Amigoo\Console\Commands;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\WebsitePaymentMethodSession;
use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\WebsitePaymentMethodSessionRepo;
use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Services\WooCommerce\Client as WooApiClient;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AutoSwitchStripeAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amigoo:auto-switch-stripe-accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto-switch Stripe accounts as configured';


    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var WebsitePaymentMethodSessionRepo */
    private $sessionsRepo;

    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
            WebsitesRepo $websitesRepo,
            WebsitePaymentMethodSessionRepo $sessionsRepo,
            StripeAccountsRepo $stripeAccsRepo,
            WooOrdersRepo $wooOrdersRepo
        ) {
        parent::__construct();

        $this->websitesRepo   = $websitesRepo;
        $this->sessionsRepo   = $sessionsRepo;
        $this->stripeAccsRepo = $stripeAccsRepo;
        $this->ordersRepo     = $wooOrdersRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $websites = $this->websitesRepo->all()
                ->filter(fn($site) => ($site->stripe_acc_id != null));

            $siteIndexCurSessionMap = [];
            foreach ($websites as $i => $site) {
                $siteIndexCurSessionMap[$i] = $this->sessionsRepo->getCurrentSessionForSite($site);
            }

            foreach ($websites as $i => $site) {
                $curSession           = $siteIndexCurSessionMap[$i];

                if ($this->shouldBeSwitched($site, $curSession)) {
                    Log::info("Auto-switching for website [{$site->name}]");

                    $readyLinkableStripeAccs =
                        $site->linkableStripeAccounts()
                            ->where('status', StripeAccountStatus::READY)
                            ->where('amigoo_stripe_accounts.id', '!=', $site->stripe_acc_id)
                            ->get();

                    if ($readyLinkableStripeAccs->isEmpty()) {
                        $defaultAcc = $this->stripeAccsRepo->getDefaultAccount($site);

                        $shouldBeSkipped = ($site->stripe_acc_id == $defaultAcc->id);
                        if ($shouldBeSkipped) {
                            Log::info("Skip auto-switch for website [{$site->name}] for no account available for switching");
                            continue;
                        }

                        $newAcc = $defaultAcc;

                    } else {
                        $newAcc = $readyLinkableStripeAccs->random();
                    }

                    $ok = $this->_linkToStripeAcc($site, $newAcc);
                    if ($ok)
                        Log::info("Auto-switched website [{$site->name}] with new Stripe account [{$newAcc->email}]");
                }
            }
        } catch (\Exception $e) {
            Log::error('Failed to auto-switch Stripe accounts for websites', compact('e'));
        }
    }

    private function shouldBeSwitched(Website $site, WebsitePaymentMethodSession $curSession)
    {
        if (! $site->auto_switch_after_mins) {
            return false;
        }

        $latestOrder = $this->ordersRepo->getLatestOrderForSite($site); // TODO check
        if ($latestOrder) {
            $latestOrderTilNowPeriod = Carbon::now()->diffInMinutes($latestOrder->created_at);
            if ($latestOrderTilNowPeriod < $site->auto_switch_after_mins) {
                return false;
            }
        }

        $curSessionLivePeriod = Carbon::now()->diffInMinutes($curSession->start_at);
        if ($curSessionLivePeriod < $site->auto_switch_after_mins) {
            return false;
        }

        return true;
    }

    private function _linkToStripeAcc(Website $website, StripeAccount $newAcc)
    {
        DB::beginTransaction();

        try {
            $this->websitesRepo->unlinkFromStripeAccount($website);
            $this->sessionsRepo->endCurrentSessionForSite($website);

            $this->websitesRepo->linkToStripeAccount($website, $newAcc);
            $this->sessionsRepo->createSessionWithStripe($website, $newAcc);

        } catch (\Exception $e) {
            Log::error("Could not create new session for website [{$website->name}]", compact('e'));
            DB::rollBack();

            return false;
        }

        // CURRENTLY, ONLY WooCommerce sites need API call;
        if ($website->needApiCall()) {
            try {
                WooApiClient::fromWebsite($website)
                    ->updatePaymentGatewayStripe(
                        $newAcc->public_key,
                        $newAcc->secret_key
                    );

            } catch (\Exception $e) {
                Log::error('Could not update website\'s Stripe account', compact('e'));
                DB::rollBack();

                return false;
            }
        }

        DB::commit();

        return true;
    }
}
