<?php

namespace Amigoo\Console\Commands;

use Amigoo\Database\Models\Constants\WebsiteType;
use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Services\WooCommerce\OrderDataCollector;

class CollectOrderDataForSite extends \Illuminate\Console\Command
{
    protected $signature = 'amigoo:collect-order-data-for-site {siteId}';

    protected $description = 'Collect meta-data for processing/completed orders which belong to a Woo-site';

    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /** @var OrderDataCollector */
    private $orderDataCollector;

    public function __construct(
            WebsitesRepo $websitesRepo,
            WooOrdersRepo $wooOrdersRepo,
            OrderDataCollector $orderDataCollector
            ) {
        parent::__construct();

        $this->websitesRepo       = $websitesRepo;
        $this->wooOrdersRepo      = $wooOrdersRepo;
        $this->orderDataCollector = $orderDataCollector;
    }

    public function handle()
    {
        $siteId = $this->argument('siteId');
        $site   = $this->websitesRepo->getById($siteId);

        if ($site->type != WebsiteType::WOO)
            return $this->error('This site is not a Woo-site');

        $progBar = null;
        $this->orderDataCollector->setBeforeCollectingHandler(function ($orders) use (&$progBar) {
            $progBar = $this->output->createProgressBar($orders->count());
            $progBar->start();
        });
        $this->orderDataCollector->setAfterSingleQueryDoneHandler(function ($order) use (&$progBar) {
            $progBar->advance();
            $this->info("\nCollected meta-data for order #{$order->id}");
        });

        $this->orderDataCollector->collectForSite($site);
        $progBar->finish();
    }
}
