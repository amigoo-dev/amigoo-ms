<?php

namespace Amigoo\Console\Commands;

use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\StripeReservedFundRepo;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Stripe\StripeClient;

class CollectReservedFundsFor90And120DaysHoldStripeAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amigoo:collect-reserved-funds-for-90-and-120-days-hold-stripe-accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect reserved funds for 90/120 days hold Stripe accounts';

    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var StripeReservedFundRepo */
    private $stripeReservedFundRepo;

    /** @var StripeClient */
    private $stripe;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
            StripeAccountsRepo $stripeAccsRepo,
            StripeReservedFundRepo $stripeReservedFundRepo
            ) {
        parent::__construct();

        $this->stripeAccsRepo         = $stripeAccsRepo;
        $this->stripeReservedFundRepo = $stripeReservedFundRepo;
        $this->stripe                 = new StripeClient;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stripeAccs = $this->stripeAccsRepo->getAccountsNeedRetrievingReservedFunds();

        if ($stripeAccs->isEmpty()) {
            Log::info('No Stripe accounts need retrieving reserved funds for today');
            return;
        }

        Log::info("{$stripeAccs->count()} Stripe accounts need retrieving reserved funds for today");
        foreach ($stripeAccs as $acc) {
            try {
                Log::info("Retrieving reserved fund for Stripe acc. [{$acc->email}]");
                $reservedFunds = $this->retrieveReservedFundsFromStripeApi($acc->secret_key);
                if (empty($reservedFunds)) {
                    Log::info("No reserved fund available for Stripe acc. [{$acc->email}]");
                    continue;
                }

                $pendingReservedFunds = $this->filterPendingReservedFunds($reservedFunds);
                if (empty($pendingReservedFunds)) {
                    Log::info("No PENDING reserved fund available for Stripe acc. [{$acc->email}]");
                    continue;
                }

                $latestPendingReservedFund = $pendingReservedFunds[0];
                $totalNetAmount            = $this->calculateTotalReservedFundsNetAmount($pendingReservedFunds);
                $availableOn               = new Carbon($latestPendingReservedFund['available_on']);

                $firstTimeRetrieval = ! $acc->reservedFund;
                if ($firstTimeRetrieval) {
                    $this->stripeReservedFundRepo->insert(
                        $acc->id,
                        $totalNetAmount,
                        $latestPendingReservedFund['currency'],
                        $availableOn,
                        $latestPendingReservedFund['reporting_category'],
                        $latestPendingReservedFund['status']
                    );
                } else {
                    $this->stripeReservedFundRepo->update($acc->id, $totalNetAmount, $availableOn);
                }

            } catch (\Exception $e) {
                Log::error("Failed to retrieve reserved fund for Stripe acc. [{$acc->email}]", compact('e'));
            }
        }
    }

    private function retrieveReservedFundsFromStripeApi(string $secretKey)
    {
        $response =
            $this->stripe->balanceTransactions->all(
                [
                    'type'  => 'reserved_funds',
                    'limit' => 10,
                ],
                [
                    'api_key' => $secretKey,
                ]
            );

        if (empty($response['data']))
            return null;

        return $response['data'];
    }

    private function filterPendingReservedFunds(array $reservedFunds)
    {
        return array_filter($reservedFunds, fn($reserved) => $reserved['status'] == 'pending');
    }

    private function calculateTotalReservedFundsNetAmount(array $reservedFunds)
    {
        $totalNet = 0;
        foreach ($reservedFunds as $reserved) {
            if ($reserved['net'] < 0)
                continue;

            $totalNet += $reserved['net'];
        }

        $realTotalNet = $totalNet / 100;
        return $realTotalNet;
    }
}
