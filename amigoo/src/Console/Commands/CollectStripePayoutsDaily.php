<?php

namespace Amigoo\Console\Commands;

use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\StripePayoutsRepo;
use Amigoo\Services\Stripe\StripePayoutsCollector;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CollectStripePayoutsDaily extends \Illuminate\Console\Command
{
    protected $signature = 'amigoo:collect-stripe-payouts-daily';

    protected $description = 'Collect payouts info from Stripe API for still-alive accounts with rule of limitation for daily collecting';

    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var StripePayoutsRepo */
    private $payoutsRepo;

    /** @var StripePayoutsCollector */
    private $stripePayoutsCollector;

    public function __construct(
            StripeAccountsRepo $stripeAccsRepo,
            StripePayoutsRepo $stripePayoutsRepo,
            StripePayoutsCollector $stripePayoutsCollector
            ) {
        parent::__construct();

        $this->stripeAccsRepo         = $stripeAccsRepo;
        $this->payoutsRepo            = $stripePayoutsRepo;
        $this->stripePayoutsCollector = $stripePayoutsCollector;
    }

    public function handle()
    {
        $stripeAccs              = $this->stripeAccsRepo->getAccountsForCollectingPayouts();
        $queryOptionCreatedAfter = Carbon::today()->subtract(5, 'days');

        foreach ($stripeAccs as $acc) {
            try {
                $shouldLimit = ($this->payoutsRepo->countForStripeAcc($acc) > 0);
                if ($shouldLimit)
                    $this->stripePayoutsCollector->collect($acc, $queryOptionCreatedAfter);
                else
                    $this->stripePayoutsCollector->collect($acc);

            } catch (\Exception $e) {
                Log::error("Failed to collect payouts daily for {$acc->email}", compact('e'));
            }
        }
    }
}
