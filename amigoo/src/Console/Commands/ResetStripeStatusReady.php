<?php

namespace Amigoo\Console\Commands;

use Amigoo\Database\Repos\StripeAccountsRepo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ResetStripeStatusReady extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amigoo:reset-stripe-status-ready';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Stripe accounts which has status REACHED_DAILY_MAX';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(StripeAccountsRepo $stripeAccsRepo)
    {
        try {
            $haveBeenResetAccs = $stripeAccsRepo->resetStatusReadyForAccsReachedDailyMax();

            if ($haveBeenResetAccs == null) {
                Log::info('No accounts need Ready-status daily reset currenttly');
            } else {
                $count = $haveBeenResetAccs->count();
                $emails = $haveBeenResetAccs->pluck('email')->join(', ');
                Log::info("{$count} Stripe account(s) have been reset: {$emails}");
            }
        } catch (\Exception $e) {
            Log::error('Failed to reset Ready-status daily');
        }
    }
}
