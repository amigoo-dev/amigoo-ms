<?php

namespace Amigoo\Database\Models\Constants;

class NotificationType
{
    const STRIPE_ACC_ISSUE_STATUS = 'stripe_acc_issue_status';
    const STRIPE_DISPUTE          = 'stripe_dispute';

    private static $valueToLabelMap = [
        self::STRIPE_ACC_ISSUE_STATUS => 'Stripe-account issue status',
        self::STRIPE_DISPUTE          => 'Stripe dispute',
    ];

    public static function getAvailableTypes()
    {
        return self::$valueToLabelMap;
    }

    public static function getAvailableValues()
    {
        return array_keys(self::$valueToLabelMap);
    }

    public static function getLabel(string $value)
    {
        return self::$valueToLabelMap[$value];
    }
}
