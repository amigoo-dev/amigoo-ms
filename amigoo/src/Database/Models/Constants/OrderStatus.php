<?php

namespace Amigoo\Database\Models\Constants;

interface OrderStatus
{
    const FAILED     = 'failed';
    const PROCESSING = 'processing';
    const COMPLETED  = 'completed';
}
