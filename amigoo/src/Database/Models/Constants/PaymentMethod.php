<?php

namespace Amigoo\Database\Models\Constants;

abstract class PaymentMethod
{
    const CASH   = 'cod';
    const PAYPAL = 'paypal';
    const STRIPE = 'stripe';
}
