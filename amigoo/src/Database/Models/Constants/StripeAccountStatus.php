<?php

namespace Amigoo\Database\Models\Constants;

abstract class StripeAccountStatus
{
    const READY                   = 'READY';

    const PENDING                 = 'PENDING';

    const REACHED_DAILY_MAX       = 'REACHED_DAILY_MAX';
    const WAITING_PAYOUT          = 'WAITING_PAYOUT';

    const WAITING_VERIFY_PHONE    = 'WAITING_VERIFY_PHONE';
    const WAITING_VERIFY_ID       = 'WAITING_VERIFY_ID';
    const WAITING_VERIFY_IDENTITY = 'WAITING_VERIFY_IDENTITY';
    const WAITING_VERIFY_TAX      = 'WAITING_VERIFY_TAX';
    const WAITING_VERIFY_SITE     = 'WAITING_VERIFY_SITE';
    const WAITING_SUBMIT_W8       = 'WAITING_SUBMIT_W8';
    const NEED_UPDATE_BANK        = 'NEED_UPDATE_BANK';

    const FAILED_TOO_MUCH         = 'FAILED_TOO_MUCH';
    const NEED_CHECK              = 'NEED_CHECK';
    const WAITING_VERIFY_DOC      = 'WAITING_VERIFY_DOC';

    const WAITING_VERIFY_DOMAIN   = 'WAITING_VERIFY_DOMAIN';
    const HOLD_90                 = 'HOLD_90';
    const HOLD_120                = 'HOLD_120';
    const ENDING                  = 'ENDING';
    const REFUNDED_ALL            = 'REFUNDED_ALL';


    private static $valueLabelMap = [
        self::READY                   => 'Ready',

        self::PENDING                 => 'Pending',
        self::REACHED_DAILY_MAX       => 'Reached daily',
        self::WAITING_PAYOUT          => 'Waiting payout',

        self::WAITING_VERIFY_PHONE    => 'Phone ver',
        self::WAITING_VERIFY_ID       => 'ID ver',
        self::WAITING_VERIFY_IDENTITY => 'Identity ver',
        self::WAITING_VERIFY_TAX      => 'Tax ver',
        self::WAITING_VERIFY_SITE     => 'Site ver',
        self::WAITING_SUBMIT_W8       => 'W-8 Submit',
        self::NEED_UPDATE_BANK        => 'Need update bank',

        self::FAILED_TOO_MUCH         => 'Failed too much',
        self::NEED_CHECK              => 'Need check',
        self::WAITING_VERIFY_DOC      => 'Doc ver',

        self::WAITING_VERIFY_DOMAIN   => 'Domain ver',
        self::HOLD_90                 => '90 days',
        self::HOLD_120                => '120 days',
        self::ENDING                  => 'Ending',
        self::REFUNDED_ALL            => 'Refunded All',
    ];


    public static function getAvailableStatuses()
    {
        return self::$valueLabelMap;
    }

    public static function getAvailableStatusesValues()
    {
        return array_keys(self::$valueLabelMap);
    }

    public static function getLabel(string $value)
    {
        return self::$valueLabelMap[$value];
    }
}
