<?php

namespace Amigoo\Database\Models\Constants;

abstract class StripeDisputeStatus
{
    const PENDING   = 'PENDING';
    const SUBMITTED = 'SUBMITTED';
    const IGNORED   = 'IGNORED';
    const WON       = 'WON';
    const LOST      = 'LOST';

    private static $valueLabelMap = [
        self::PENDING   => 'Pending',
        self::SUBMITTED => 'Submitted',
        self::IGNORED   => 'Ignored',
        self::WON       => 'Won',
        self::LOST      => 'Lost',
    ];


    public static function getAvailableStatuses()
    {
        return self::$valueLabelMap;
    }

    public static function getAvailableStatusesValues()
    {
        return array_keys(self::$valueLabelMap);
    }

    public static function getLabel(string $value)
    {
        return self::$valueLabelMap[$value];
    }
}
