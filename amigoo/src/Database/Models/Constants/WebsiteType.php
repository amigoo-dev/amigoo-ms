<?php

namespace Amigoo\Database\Models\Constants;

abstract class WebsiteType
{
    const WOO      = 'WOO';
    const MERCHIZE = 'MERCHIZE';

    private static $valueLabelMap = [
        self::WOO      => 'Woo',
        self::MERCHIZE => 'Merchize',
    ];

    private static $typesNeedApiCall = [self::WOO];


    public static function getTypesNeedApiCall()
    {
        return self::$typesNeedApiCall;
    }


    public static function getAvailableStatuses()
    {
        return self::$valueLabelMap;
    }

    public static function getAvailableStatusesValues()
    {
        return array_keys(self::$valueLabelMap);
    }

    public static function getLabel(string $value)
    {
        return self::$valueLabelMap[$value];
    }
}
