<?php

namespace Amigoo\Database\Models;

class MerchizeOrder extends \Illuminate\Database\Eloquent\Model
{
    // `created_at` timestamp only;
    public const UPDATED_AT = null;

    protected $table = 'amigoo_orders_merchize';

    protected $fillable = [
        'order_code',
        'customer_email',
        'total',
        'currency_code',
    ];

    protected $attributes = [
        'status' => 'PAID',
    ];


    public function stripeAccount()
    {
        return $this->belongsTo(StripeAccount::class, 'stripe_acc_id');
    }

    public function website()
    {
        return $this
            ->belongsTo(Website::class, 'website_id')
            ->withTrashed();
    }
}
