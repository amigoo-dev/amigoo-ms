<?php

namespace Amigoo\Database\Models;

class ProxyInfo extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'amigoo_proxy_info_records';

    protected $fillable = [
        'ip',
        'port',
        'user',
        'password',
        'type',
        'provider',
    ];

    protected $dates = [
        'last_proc_at',
    ];

    public function getProxyAddressAttribute()
    {
        return "{$this->ip}:{$this->port}";
    }
}
