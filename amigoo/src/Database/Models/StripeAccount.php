<?php

namespace Amigoo\Database\Models;

class StripeAccount extends \Illuminate\Database\Eloquent\Model
{
    // `updated_at` timestamp only;
    public const CREATED_AT = null;

    protected $table = 'amigoo_stripe_accounts';

    protected $fillable = [
        'status',
        'owner_name',
        'email',
        'country_code',
        'public_key',
        'secret_key',
        'daily_max_received_amount',
        'should_wait_for_payout',
        'note',
    ];

    protected $attributes = [
        'status'                 => Constants\StripeAccountStatus::READY,
        'is_default'             => false,
        'should_wait_for_payout' => true,
    ];


    public function linkedWebsites()
    {
        return $this->hasMany(Website::class, 'stripe_acc_id');
    }

    public function linkableWebsites()
    {
        return $this
            ->belongsToMany(Website::class, 'amigoo_website_stripe_linkables', 'stripe_acc_id', 'website_id')
            ->withPivot('is_default')
            ->as('linkable');
    }

    public function paymentMethodSessions()
    {
        return $this->morphMany(WebsitePaymentMethodSession::class, null, 'payment_method', 'payment_acc_id');
    }

    public function wooOrders()
    {
        return $this
            ->hasMany(WooOrder::class, 'stripe_acc_id')
            ->where('payment_method', Constants\PaymentMethod::STRIPE)
        ;
    }

    public function merchizeOrders()
    {
        return $this->hasMany(MerchizeOrder::class, 'stripe_acc_id');
    }

    public function reservedFund()
    {
        return $this->hasOne(StripeReservedFund::class, 'stripe_acc_id');
    }

    public function proxyInfo()
    {
        return $this->hasOne(ProxyInfo::class, 'stripe_acc_id');
    }

    public function payouts()
    {
        return $this->hasMany(StripePayout::class, 'stripe_acc_id');
    }
}