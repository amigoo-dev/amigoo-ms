<?php

namespace Amigoo\Database\Models;

class StripeDispute extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'amigoo_stripe_disputes';

    protected $fillable = [
        'website_id',
        'status',
    ];

    protected $attributes = [
        'status' => Constants\StripeDisputeStatus::PENDING,
    ];


    protected static function booted()
    {
        static::addGlobalScope('relationship_eagerload_auto', fn($q) => $q->with('stripeAccount', 'website'));
    }

    public function stripeAccount()
    {
        return $this->belongsTo(StripeAccount::class, 'stripe_acc_id');
    }

    public function website()
    {
        return $this->belongsTo(Website::class, 'website_id')
            ->withTrashed();
    }
}