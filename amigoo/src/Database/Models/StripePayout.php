<?php

namespace Amigoo\Database\Models;

class StripePayout extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'amigoo_stripe_payouts';

    public $timestamps = false;

    protected $fillable = [
        'stripe_po_id',
        'type',
        'status',
        'amount',
        'currency_code',
        'arrival_date',
        'created_at',
    ];
}
