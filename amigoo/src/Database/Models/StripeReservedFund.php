<?php

namespace Amigoo\Database\Models;

class StripeReservedFund extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'amigoo_stripe_reserved_funds';

    protected $fillable = [
        'amount',
        'currency_code',
        'available_on',
        'reporting_category',
        'status',
    ];

    protected $dates = [
        'available_on',
    ];

    public function stripeAccount()
    {
        return $this->belongsTo(StripeAccount::class, 'stripe_acc_id');
    }
}
