<?php

namespace Amigoo\Database\Models;

use App\Models\Auth\User;

class Website extends \Illuminate\Database\Eloquent\Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    const DELETED_AT = 'deactivated_at';


    const DEFAULT_AUTO_SWITCH_AFTER_MINS             = 30;
    const DEFAULT_CHANGE_STRIPE_STATUS_FAILED_AMOUNT = 2;


    protected $table = 'amigoo_websites';

    protected $fillable = [
        'name',
        'url',
        'api_consumer_key',
        'api_consumer_secret',
        'webhook_secret',
        'auto_switch_after_mins',
        'change_stripe_status_failed_amount',
    ];

    protected $dates = [
        self::DELETED_AT,
    ];

    public $timestamps = false;


    public function needApiCall()
    {
        return in_array($this->type, Constants\WebsiteType::getTypesNeedApiCall());
    }

    public function notNeedApiCall()
    {
        return ! $this->needApiCall();
    }

    public function isWooSite()
    {
        return ($this->type == Constants\WebsiteType::WOO);
    }


    public function stripeAccount()
    {
        return $this->belongsTo(StripeAccount::class, 'stripe_acc_id');
    }

    public function linkableStripeAccounts()
    {
        return $this
            ->belongsToMany(StripeAccount::class, 'amigoo_website_stripe_linkables', 'website_id', 'stripe_acc_id')
            ->withPivot('is_default')
            ->as('linkable');
    }

    public function wooOrders()
    {
        return $this->hasMany(WooOrder::class, 'website_id');
    }

    public function merchizeOrders()
    {
        return $this->hasMany(MerchizeOrder::class, 'website_id');
    }

    public function availableTlds()
    {
        return $this->hasMany(WebsiteTld::class, 'website_id');
    }

    public function paymentMethodSessions()
    {
        return $this->hasMany(WebsitePaymentMethodSession::class, 'website_id');
    }

    public function moderators()
    {
        return $this->belongsToMany(User::class, 'amigoo_website_user_moderatables', 'website_id', 'user_id')
            ->withPivot('enabled_notifications');
    }
}
