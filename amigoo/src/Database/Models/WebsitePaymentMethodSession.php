<?php

namespace Amigoo\Database\Models;

class WebsitePaymentMethodSession extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'amigoo_website_payment_method_sessions';

    protected $fillable = [
        'website_id',
        'payment_method',
        'payment_acc_id',
        'start_at',
        'end_at',
    ];

    protected $dates = [
        'start_at',
        'end_at',
    ];

    public $timestamps = false;

    public function website()
    {
        return $this->belongsTo(Website::class, 'website_id');
    }

    public function paymentAccount()
    {
        return $this->morphTo(null, 'payment_method', 'payment_acc_id');
    }
}
