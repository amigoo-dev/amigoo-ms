<?php

namespace Amigoo\Database\Models;

class WebsiteTld extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'amigoo_website_tlds';

    protected $fillable = [
        'tld',
    ];

    public $timestamps = false;

    public function website()
    {
        return $this->belongsTo(Website::class, 'website_id');
    }
}
