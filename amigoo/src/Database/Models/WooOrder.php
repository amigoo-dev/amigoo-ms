<?php

namespace Amigoo\Database\Models;

class WooOrder extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'amigoo_orders';

    protected $fillable = [
        'woo_order_id',
        'total',
        'created_at',
        'paid_at',
        'order_number',
        'stripe_intent_id',
        'stripe_charge_id',
    ];

    protected $dates = [
        'created_at',
        'paid_at',
    ];

    public $timestamps = false;

    public function stripeAccount()
    {
        return $this->belongsTo(StripeAccount::class, 'stripe_acc_id');
    }

    public function website()
    {
        return $this
            ->belongsTo(Website::class, 'website_id')
            ->withTrashed();
    }

    public function dispute()
    {
        return $this->hasOne(StripeDispute::class, 'stripe_payment_id', 'stripe_intent_id');
    }

    public function getHasStatusFailedAttribute()
    {
        return ($this->status == Constants\OrderStatus::FAILED);
    }
}
