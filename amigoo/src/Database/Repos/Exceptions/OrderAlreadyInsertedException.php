<?php

namespace Amigoo\Database\Repos\Exceptions;

use Amigoo\Database\Models\Website;

class OrderAlreadyInsertedException extends \Exception
{
    /** @var Website */
    public $site;

    /** @var int */
    public $wooOrderId;

    public function __construct(Website $site, int $wooOrderId)
    {
        parent::__construct("Order #{$wooOrderId} from {$site->url} has already been inserted");

        $this->site       = $site;
        $this->wooOrderId = $wooOrderId;
    }
}
