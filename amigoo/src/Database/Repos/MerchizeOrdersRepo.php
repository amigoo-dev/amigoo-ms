<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\MerchizeOrder;
use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use App\Models\Auth\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MerchizeOrdersRepo extends \App\Repositories\BaseRepository
{
    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    public function __construct(MerchizeOrder $model, StripeAccountsRepo $stripeAccsRepo)
    {
        $this->model = $model;

        $this->stripeAccsRepo = $stripeAccsRepo;
    }

    public function insert(
            $siteId,
            $stripeAccId,
            $orderCode,
            $customerEmail,
            $total,
            $currencyCode
            ) {

        $order = new MerchizeOrder;
        $order->website_id     = $siteId;
        $order->stripe_acc_id  = $stripeAccId;
        $order->order_code     = $orderCode;
        $order->customer_email = $customerEmail;
        $order->total          = $total;
        $order->currency_code  = $currencyCode;

        $order->save();

        return $order;
    }



    public function getOrdersBySite(Website $site, QueryOptions $queryOptions, User $moderator = null): QueryResult
    {
        $keyword = trim($queryOptions->getFilteringKeyword());
        $query   = $site->merchizeOrders();

        if ($moderator) {
            $moderatableStripeAccsIds = $this->stripeAccsRepo->getAccountsByModerator($moderator)->pluck('id');
            $query->whereIn('stripe_acc_id', $moderatableStripeAccsIds);
        }

        $totalCount = $query->count();

        if ($keyword !== '')
            $query->where('order_code', 'LIKE', "%$keyword%");

        $filteredCount = $query->count();

        $query
            ->with('stripeAccount')
            ->offset($queryOptions->getPagingStart())
            ->limit($queryOptions->getPagingLength())
        ;

        foreach ($queryOptions->getOrderings() as $ordering) {
            $query->orderBy($ordering['field'], $ordering['direction']);
        }

        $data = $query->get();

        return new QueryResult($totalCount, $filteredCount, $data);
    }

    public function getOrderByOrderCode(string $orderCode): ?MerchizeOrder
    {
        return $this->model->newQuery()
            ->where('order_code', $orderCode)
            ->with('website', 'stripeAccount')
            ->first();
    }

    public function getLatestOrderForSite(Website $site): ?MerchizeOrder
    {
        return $site->merchizeOrders()
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function getTodayReceivedAmountForSite(int $siteId): float
    {
        $todayVn = $this->getStartAtEndAtUtc7(Carbon::now());

        return $this->model->newQuery()
            ->where('website_id', $siteId)
            ->whereBetween('created_at', [
                $todayVn['startAt'],
                $todayVn['endAt']
            ])
            ->sum('total');
    }

    public function sumTotalForSite(Website $site): float
    {
        $query = $site->merchizeOrders();

        return $query->sum('total');
    }

    public function countOrdersBySite(Website $site): int
    {
        $query = $site->merchizeOrders();

        return $query->count();
    }



    public function getOrdersByStripeAcc(StripeAccount $acc, QueryOptions $queryOptions, User $moderator = null): QueryResult
    {
        $keyword = trim($queryOptions->getFilteringKeyword());
        $query   = $acc->merchizeOrders();

        if ($moderator) {
            $moderatableSitesIds = $moderator->moderatableWebsites->pluck('id');
            $query->whereIn('website_id', $moderatableSitesIds);
        }

        $totalCount = $acc->merchizeOrders()->count();

        if ($keyword !== '')
            $query->where('order_code', 'LIKE', "%{$keyword}%");

        $filteredCount = $query->count();

        $query
            ->with('website')
            ->offset($queryOptions->getPagingStart())
            ->limit($queryOptions->getPagingLength())
        ;

        foreach ($queryOptions->getOrderings() as $ordering) {
            $query->orderBy($ordering['field'], $ordering['direction']);
        }

        $data = $query->get();

        return new QueryResult($totalCount, $filteredCount, $data);
    }

    public function getTodayReceivedAmountForStripeAcc(int $stripeAccId): float
    {
        return $this->getReceivedAmountForStripeAccByDate($stripeAccId, Carbon::now());
    }

    public function getReceivedAmountForStripeAccByDate(int $stripeAccId, Carbon $date): float
    {
        $todayVn = $this->getStartAtEndAtUtc7($date);

        return $this->model->newQuery()
            ->where([
                ['stripe_acc_id', $stripeAccId],
            ])
            ->whereBetween('created_at', [
                $todayVn['startAt'],
                $todayVn['endAt']
            ])
            ->sum('total');
    }

    public function getTotalReceivedAmountForStripeAcc(int $stripeAccId): float
    {
        return $this->model->newQuery()
            ->where([
                ['stripe_acc_id', $stripeAccId],
            ])
            ->sum('total');
    }



    public function getTodayReceivedAmountForAllStripeAccs(): array
    {
        return $this->getReceivedAmountForAllStripeAccsByDate(Carbon::now());
    }

    public function getReceivedAmountForAllStripeAccsByDate(Carbon $date): array
    {
        $todayVn = $this->getStartAtEndAtUtc7($date);

        $queryResult = $this->model->newQuery()
            ->select(
                'stripe_acc_id',
                DB::raw('SUM(`total`) as `received_today_sum`')
            )
            ->whereBetween('created_at', [
                $todayVn['startAt'],
                $todayVn['endAt']
            ])
            ->groupBy('stripe_acc_id')
            ->get();

        $map = [];
        foreach ($queryResult as $record) {
            $map[$record->stripe_acc_id] = $record->received_today_sum;
        }

        return $map;
    }

    public function getTotalReceivedAmountForAllStripeAccs(): array
    {
        $queryResult = $this->model->newQuery()
            ->select(
                'stripe_acc_id',
                DB::raw('SUM(`total`) as `total_received_sum`')
            )
            ->groupBy('stripe_acc_id')
            ->get();

        $map = [];
        foreach ($queryResult as $record) {
            $map[$record->stripe_acc_id] = $record->total_received_sum;
        }

        return $map;
    }



    public function getLatestOrderForStripeAcc(int $stripeAccId): ?MerchizeOrder
    {
        return $this->getLatestOrdersForStripeAcc($stripeAccId, 1)->first();
    }

    public function getLatestOrdersForStripeAcc(int $stripeAccId, int $limit): Collection
    {
        return $this->model->newQuery()
            ->where('stripe_acc_id', $stripeAccId)
            ->take($limit)
            ->orderBy('created_at', 'desc')
            ->get();
    }



    private function getStartAtEndAtUtc7(Carbon $now)
    {
        $hanoiNow               = $now->copy()->addHours(7);
        $hanoiTodayStartAt      = $hanoiNow->copy()->setTime(0, 0, 0);
        $hanoiTodayStartAtAsUtc = $hanoiTodayStartAt->copy()->subHours(7);
        $hanoiTodayEndAtAsUtc   = $hanoiTodayStartAtAsUtc->copy()->addHours(24);

        return [
            'startAt' => $hanoiTodayStartAtAsUtc,
            'endAt'   => $hanoiTodayEndAtAsUtc
        ];
    }
}
