<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\ProxyInfo;
use Amigoo\Database\Models\StripeAccount;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ProxyInfoRepo extends \App\Repositories\BaseRepository
{
    public function __construct(ProxyInfo $model)
    {
        $this->model = $model;
    }

    public function insertNewRecordForStripeAcc(
            StripeAccount $stripeAcc,
            string $ip,
            ?int $port,
            ?string $user,
            ?string $pwd,
            ?string $meta_type,
            ?string $meta_provider
            ) {

        $proxyForStripeAccInserted = !! $stripeAcc->proxyInfo;
        if ($proxyForStripeAccInserted)
            throw new \Exception("Proxy-info record for Stripe-acc [$stripeAcc->email] already inserted");

        $newProxyRecord = new ProxyInfo([
            'ip'       => $ip,
            'port'     => $port,
            'user'     => $user,
            'password' => $pwd,
            'type'     => $meta_type,
            'provider' => $meta_provider,
        ]);
        $stripeAcc->proxyInfo()->save($newProxyRecord);

        return $newProxyRecord;
    }

    public function update(ProxyInfo $proxyInfoRecord, array $newData)
    {
        $proxyInfoRecord->update(
            Arr::only($newData, [
                'ip',
                'port',
                'user',
                'password',
                'type',
                'provider',
            ])
        );
    }

    public function updateLastProcStatus(ProxyInfo $proxyInfoRecord, Carbon $lastProcAt, bool $hasLastProcFailed = false)
    {
        $proxyInfoRecord->last_proc_at         = $lastProcAt;
        $proxyInfoRecord->has_last_proc_failed = $hasLastProcFailed;

        $proxyInfoRecord->save();
    }

    public function getProxyInfoById(int $proxyInfoRecordId)
    {
        return $this->getById($proxyInfoRecordId);
    }

    public function getProxyInfoForStripeAcc(StripeAccount $stripeAcc)
    {
        return $stripeAcc->proxyInfo;
    }
}
