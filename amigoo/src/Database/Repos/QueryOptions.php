<?php

namespace Amigoo\Database\Repos;

class QueryOptions
{
    const DEFAULT_QUERY_LIMIT = 20;

    private $filteringKeyword = null;

    private $filteringOptions = [];

    private $orderings = [];

    private $paging = [
        'start' => 0,
        'length' => self::DEFAULT_QUERY_LIMIT,
    ];


    public function setFilteringKeyword(?string $keyword): QueryOptions
    {
        $this->filteringKeyword = $keyword;
        return $this;
    }

    public function getFilteringKeyword(): ?string
    {
        return $this->filteringKeyword;
    }

    public function addFilteringOption(string $key, $option): QueryOptions
    {
        $this->filteringOptions[$key] = $option;
        return $this;
    }

    public function getFilteringOption(string $key)
    {
        if (isset($this->filteringOptions[$key]) && $this->filteringOptions[$key])
            return $this->filteringOptions[$key];

        return null;
    }

    public function hasFilteringOption(string $key): bool
    {
        return
            isset($this->filteringOptions[$key])
            && $this->filteringOptions[$key];
    }

    public function addOrdering(string $field, string $direction): QueryOptions
    {
        $this->orderings[] = compact('field', 'direction');
        return $this;
    }

    public function getOrderings(): array
    {
        return [...$this->orderings];
    }

    public function setPagingStart(int $start): QueryOptions
    {
        $this->paging['start'] = $start;
        return $this;
    }

    public function getPagingStart(): int
    {
        return $this->paging['start'];
    }

    public function setPagingLength(?int $length): QueryOptions
    {
        $this->paging['length'] = $length;
        return $this;
    }

    public function getPagingLength(): ?int
    {
        return $this->paging['length'];
    }

    public function setNoPaging()
    {
        return $this->setPagingLength(null);
    }

    public function hasPaging()
    {
        return ($this->getPagingLength() != null);
    }
}
