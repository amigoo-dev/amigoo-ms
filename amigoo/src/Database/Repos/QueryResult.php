<?php

namespace Amigoo\Database\Repos;

class QueryResult
{
    public $totalCount;

    public $filteredCount;

    public $data;

    public function __construct(int $totalCount, int $filteredCount, $data)
    {
        $this->totalCount    = $totalCount;
        $this->filteredCount = $filteredCount;
        $this->data          = $data;
    }
}
