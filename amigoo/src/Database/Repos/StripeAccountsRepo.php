<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\Constants\StripeAccountStatus as Status;
use App\Exceptions\GeneralException;
use App\Models\Auth\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class StripeAccountsRepo extends \App\Repositories\BaseRepository
{
    public function __construct(StripeAccount $model)
    {
        $this->model = $model;
    }

    public function create(array $input): StripeAccount
    {
        $acc = $this->model::create($input);

        $creationFailed = ! $acc;
        if ($creationFailed)
            throw new GeneralException(__('amigoo.backend.stripe-accs.errors.creation'));

        return $acc;
    }

    public function update(StripeAccount $acc, array $input)
    {
        $updationFailed = ! $acc->update($input);
        if ($updationFailed)
            throw new GeneralException(__('amigoo.backend.stripe-accs.errors.updation'));
    }

    public function updateStatus(StripeAccount $acc, string $newStatus)
    {
        $acc->status = $newStatus;
        $acc->save();
    }

    public function resetStatusReadyForAccsReachedDailyMax()
    {
        $needResetAccs = $this->getAccountsByStatus(Status::REACHED_DAILY_MAX);
        if ($needResetAccs->isEmpty())
            return;

        $needResetIds = $needResetAccs->pluck('id')->all();
        $this->model->newQuery()
            ->whereIn('id', $needResetIds)
            ->update(['status' => Status::READY]);

        return $needResetAccs;
    }

    /**
     * If $linkableSite is null, the linkable will be removed.
     */
    public function setLinkableWebsite(StripeAccount $acc, ?Website $linkableSite)
    {
        $noSiteSelected = ! $linkableSite;

        if ($noSiteSelected) {
            $acc->linkableWebsites()->sync([]);

        } else {
            $shouldBeDefault = ($linkableSite->linkableStripeAccounts()->count() === 0);

            $acc->linkableWebsites()->sync([
                $linkableSite->id => [
                    'is_default' => $shouldBeDefault
                ]
            ]);
        }
    }

    /**
     * @throws ModelNotFoundException
     */
    public function getAccountByEmail(string $email): StripeAccount
    {
        $acc = $this->model->newQuery()
            ->where('email', $email)
            ->firstOrFail();

        return $acc;
    }

    public function getAccountsByStatus(string $status): Collection
    {
        return $this->model->newQuery()
            ->where('status', $status)
            ->get();
    }

    public function getAccountsNeedRetrievingReservedFunds(): Collection
    {
        return $this->model->newQuery()
            ->with('reservedFund')
            ->whereIn('status', [
                Status::HOLD_90,
                Status::HOLD_120,
            ])
            ->where('updated_at', '<=', Carbon::now()->subDays(1))
            ->whereDoesntHave('reservedFund', function ($query) {
                $query
                    ->where('available_on', '<=', Carbon::today())
                    ->orWhere('updated_at', '>', Carbon::today()->subDays(2))
                    ;
            })
            ->get();
    }

    public function getAccounts(?QueryOptions $queryOptions = null, ?User $moderator = null)
    {
        $query = $this->model->newQuery()
            ->with(
                'linkedWebsites',
                'linkableWebsites',
                'reservedFund',
                'proxyInfo',
            );

        $shouldFilteredByModerator = $moderator
            && ! $moderator->can('viewAny', StripeAccount::class);
        if ($shouldFilteredByModerator) {
            $moderatableSitesIds = $moderator->moderatableWebsites->pluck('id');
            $query->whereHas('linkableWebsites', function($q) use ($moderatableSitesIds) {
                $q->whereIn('amigoo_websites.id', $moderatableSitesIds);
            });
        }

        $totalCount    = $query->count();
        $filteredCount = $totalCount; // If no filter is applied, filtered-count is total-count;

        if ($queryOptions) {
            if ($queryOptions->hasFilteringOption('status')) {
                $query->where('status', $queryOptions->getFilteringOption('status'));
            }
            if ($queryOptions->hasFilteringOption('email')) {
                $emailKeyword = $queryOptions->getFilteringOption('email');
                $query->where('email', 'like', "%{$emailKeyword}%");
            }
            if ($queryOptions->hasFilteringOption('owner_name')) {
                $ownerKeyword = $queryOptions->getFilteringOption('owner_name');
                $query->where('owner_name', 'like', "%{$ownerKeyword}%");
            }
            if ($queryOptions->hasFilteringOption('proxy_info')) {
                $proxyKeyword = $queryOptions->getFilteringOption('proxy_info');
                $splitted     = explode(':', $proxyKeyword);
                $ip           = array_shift($splitted);
                $port         = array_shift($splitted);

                $query->whereHas('proxyInfo', function($q) use ($ip, $port) {
                    if ($ip)
                        $q->where('ip', 'like', "%{$ip}%");
                    if ($port)
                        $q->where('port', $port);
                });
            }

            $filteredCount = $query->count();

            if ($queryOptions->hasPaging())
                $query
                    ->offset($queryOptions->getPagingStart())
                    ->limit($queryOptions->getPagingLength())
                    ;

            $query->select('amigoo_stripe_accounts.*'); // For some joins in ordering;
            foreach ($queryOptions->getOrderings() as $ordering) {
                switch ($ordering['field']) {
                    case 'proxy_info':
                        $query
                            ->leftJoin('amigoo_proxy_info_records', 'amigoo_stripe_accounts.id', '=', 'amigoo_proxy_info_records.stripe_acc_id')
                            ->orderBy('amigoo_proxy_info_records.ip', $ordering['direction'])
                            ;
                        continue 2;
                    default:
                        $query->orderBy($ordering['field'], $ordering['direction']);
                }
            }
        } // End of applying query-options;

        $data = $query->get();

        return new QueryResult($totalCount, $filteredCount, $data);
    }

    public function getAccountsByModerator(User $moderator): Collection
    {
        $moderatableSitesIds = $moderator->moderatableWebsites->pluck('id');

        return $this->model->newQuery()
            ->whereHas('linkableWebsites', function($q) use ($moderatableSitesIds) {
                $q->whereIn('amigoo_websites.id', $moderatableSitesIds);
            })
            ->get();
    }

    public function getAccountsForLinkablesConfiguration(Website $website)
    {
        $linkableAccs =
            $website->linkableStripeAccounts()
                ->with(
                    'linkedWebsites',
                    'linkableWebsites',
                    'reservedFund',
                    'proxyInfo',
                )
                ->get();

        $floatingAccs = $this->getFloatingAccounts();

        return $linkableAccs->merge($floatingAccs);
    }

    /**
     * Floating-accounts are accounts which currently are NOT chosen on any linkables-config.
     */
    public function getFloatingAccounts()
    {
        return $this->model->newQuery()
            ->whereDoesntHave('linkableWebsites')
            ->with(
                'linkedWebsites',
                'linkableWebsites',
                'reservedFund',
                'proxyInfo',
            )
            ->get();
    }

    public function getAccountsForCollectingPayouts()
    {
        return $this->model->newQuery()
            ->whereNotIn('status', [
                Status::PENDING,
                Status::HOLD_90,
                Status::HOLD_120,
                Status::ENDING,
                Status::REFUNDED_ALL,
            ])
            ->get();
    }

    /**
     * Query Stripe accounts which fits these requirements:
     * - linkable to the given site,
     * - AND currently NOT in-used.
     */
    public function getLinkableAccountsForSwitching(Website $site)
    {
        $inUsedAccsIds = $this->getAccountIdsCurrentlyInUsed();

        $query = $site->linkableStripeAccounts()
            ->where('status', Status::READY)
            ->whereNotIn('amigoo_stripe_accounts.id', $inUsedAccsIds)
        ;

        return $query->get();
    }

    public function getAccountIdsCurrentlyInUsed()
    {
        return (new Website)->newQuery()
            ->select('stripe_acc_id')
            ->whereNotNull('stripe_acc_id')
            ->groupBy('stripe_acc_id')
            ->get()
            ->pluck('stripe_acc_id')
            ->toArray();
    }

    /**
     * @throws ModelNotFoundException
     */
    public function getDefaultAccount(Website $site, $throwIfNotFound = true)
    {
        $query = $site->linkableStripeAccounts()->wherePivot('is_default', true);

        $defaultAcc =
            $throwIfNotFound
                ? $query->firstOrFail()
                : $query->first();

        return $defaultAcc;
    }

    public function getDefaultAccountIdWebsitesMap()
    {
        $websites = Website::all();

        $map = [];
        foreach ($websites as $site) {
            $defaultAcc = $this->getDefaultAccount($site, $throwIfNotFound = false);

            if ($defaultAcc) {
                if (! isset($map[$defaultAcc->id])) {
                    $map[$defaultAcc->id] = [];
                }

                array_push($map[$defaultAcc->id], $site);
            }
        }

        return $map;
    }
}
