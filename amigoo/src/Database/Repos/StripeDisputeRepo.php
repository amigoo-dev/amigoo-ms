<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\StripeDispute;
use App\Models\Auth\User;

class StripeDisputeRepo extends \App\Repositories\BaseRepository
{
    public function __construct(StripeDispute $model)
    {
        $this->model = $model;
    }

    public function insert(
            int $stripeAccId,
            string $stripePaymentId,
            float $amount,
            string $currencyCode,
            string $reason,
            ?int $websiteId = null
            ) {

        $dispute = new StripeDispute;
        $dispute->stripe_acc_id     = $stripeAccId;
        $dispute->stripe_payment_id = $stripePaymentId;
        $dispute->amount            = $amount;
        $dispute->currency_code     = $currencyCode;
        $dispute->reason            = $reason;
        $dispute->website_id        = $websiteId;

        $dispute->save();

        return $dispute;
    }

    public function updateDisputeWebsite(StripeDispute $dispute, array $input)
    {
        $dispute->fill($input);

        $dispute->save();
    }

    public function getDisputesByModerator(User $moderator)
    {
        $moderatableSitesIds = $moderator->moderatableWebsites->pluck('id');

        return $this->model->newQuery()
            ->whereIn('website_id', $moderatableSitesIds)
            ->get();
    }
}
