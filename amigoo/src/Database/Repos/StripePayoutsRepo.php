<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\StripePayout;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class StripePayoutsRepo extends \App\Repositories\BaseRepository
{
    public function __construct(StripePayout $model)
    {
        $this->model = $model;
    }

    public function insert(
            StripeAccount $stripeAcc,
            string $stripePOId,
            string $type,
            string $status,
            float $amount,
            string $currencyCode,
            Carbon $arrivalDate,
            Carbon $createdAtOnStripe
            ): StripePayout {
        $newPayout = new StripePayout([
            'stripe_po_id'  => $stripePOId,
            'type'          => $type,
            'status'        => $status,
            'amount'        => $amount,
            'currency_code' => $currencyCode,
            'arrival_date'  => $arrivalDate,
            'created_at'    => $createdAtOnStripe,
        ]);

        $stripeAcc->payouts()->save($newPayout);

        return $newPayout;
    }

    public function update(StripePayout $payout, array $newData)
    {
        $payout->save([
            Arr::only($newData, $payout->getFillable())
        ]);
    }

    public function updateByPOId(string $stripePOId, array $newData)
    {
        $this->model->newQuery()
            ->where('stripe_po_id', $stripePOId)
            ->update($newData)
            ;
    }

    public function poIdExists(string $stripePOId): bool
    {
        $count = $this->model->newQuery()
            ->where('stripe_po_id', $stripePOId)
            ->count()
            ;

        return !! $count;
    }

    public function getOneByPOId(string $stripePOId): ?StripePayout
    {
        return $this->model->newQuery()
            ->where('stripe_po_id', $stripePOId)
            ->first()
            ;
    }

    public function getInsertedPOIdsForStripeAcc(StripeAccount $stripeAcc): array
    {
        return $stripeAcc->payouts()
            ->select('stripe_po_id')
            ->pluck('stripe_po_id')
            ->toArray()
            ;
    }

    public function getPayoutsByStripeAcc(StripeAccount $acc, QueryOptions $queryOptions): QueryResult
    {
        $keyword = trim($queryOptions->getFilteringKeyword());
        $query   = $acc->payouts();

        $totalCount = $acc->payouts()->count();

        if ($keyword !== '')
            $query->where('stripe_po_id', 'LIKE', "%{$keyword}%");

        $filteredCount = $query->count();

        $query
            ->offset($queryOptions->getPagingStart())
            ->limit($queryOptions->getPagingLength())
            ;

        foreach ($queryOptions->getOrderings() as $ordering) {
            $query->orderBy($ordering['field'], $ordering['direction']);
        }

        $data = $query->get();

        return new QueryResult($totalCount, $filteredCount, $data);
    }

    public function countForStripeAcc(StripeAccount $stripeAcc): int
    {
        return $stripeAcc->payouts()->count();
    }

    public function getTotalAmountInPeriod(StripeAccount $stripeAcc, Carbon $from, Carbon $to)
    {
        return $stripeAcc->payouts()
            ->whereBetween('created_at', [$from, $to])
            ->sum('amount')
            ;
    }
}
