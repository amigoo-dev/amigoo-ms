<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\StripeReservedFund;
use Carbon\Carbon;

class StripeReservedFundRepo extends \App\Repositories\BaseRepository
{
    public function __construct(StripeReservedFund $model)
    {
        $this->model = $model;
    }

    public function insert(
            int $stripeAccId,
            float $netAmount,
            string $currencyCode,
            Carbon $availableOn,
            string $reportingCategory,
            string $status
            ) {

        $reservedFund = new StripeReservedFund;
        $reservedFund->stripe_acc_id      = $stripeAccId;
        $reservedFund->amount             = $netAmount;
        $reservedFund->currency_code      = $currencyCode;
        $reservedFund->available_on       = $availableOn;
        $reservedFund->reporting_category = $reportingCategory;
        $reservedFund->status             = $status;
        $reservedFund->created_at         = Carbon::today(); // Set as today (NOT now)
        $reservedFund->updated_at         = Carbon::today(); // Set as today (NOT now)

        $reservedFund->save();

        return $reservedFund;
    }

    public function update(
            int $stripeAccId,
            float $netAmount,
            Carbon $availableOn
            ) {

        $this->model->newQuery()
            ->where('stripe_acc_id', $stripeAccId)
            ->update([
                'amount'       => $netAmount,
                'available_on' => $availableOn,
                'updated_at'   => Carbon::today(), // Must force update this field, and set as today (NOT now)
            ]);
    }
}