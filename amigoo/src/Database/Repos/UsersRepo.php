<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\Website;
use App\Models\Auth\User;

class UsersRepo extends \App\Repositories\Backend\Auth\UserRepository
{
    public function getSuperAdmin(): User
    {
        return $this->model->newQuery()
            ->orderBy('created_at', 'asc')
            ->first();
    }

    public function getWebsiteModeratorsByNotificationType(Website $site, string $notificationType)
    {
        return $site->moderators()
            ->wherePivot('enabled_notifications', 'like', "%|$notificationType|%")
            ->get();
    }
}
