<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\WebsitePaymentMethodSession;
use Amigoo\Database\Models\Constants\PaymentMethod;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class WebsitePaymentMethodSessionRepo extends \App\Repositories\BaseRepository
{
    public function __construct(WebsitePaymentMethodSession $model)
    {
        $this->model = $model;
    }

    public function createSessionWithStripe(Website $site, StripeAccount $acc): WebsitePaymentMethodSession
    {
        $newSession = new WebsitePaymentMethodSession([
            'payment_method' => PaymentMethod::STRIPE,
            'payment_acc_id' => $acc->id,
            'start_at'       => Carbon::now(),
        ]);

        $site->paymentMethodSessions()->save($newSession);

        return $newSession;
    }

    public function endSession(WebsitePaymentMethodSession $session)
    {
        $session->end_at = Carbon::now();

        $session->save();
    }

    public function endCurrentSessionForSite(Website $site)
    {
        $curSession = $this->getCurrentSessionForSite($site);

        if ($curSession == null)
            return null;

        $this->endSession($curSession);

        return $curSession;
    }

    public function getCurrentSessionForSite(Website $site)
    {
        return
            $site->paymentMethodSessions()
                ->whereNull('end_at')
                ->orderBy('start_at', 'desc')
                ->first();
    }

    public function getSessionsForSite(Website $site, QueryOptions $queryOptions)
    {
        $query = $site->paymentMethodSessions()
            ->with('paymentAccount');

        $totalCount = $query->count();

        $query
            ->offset($queryOptions->getPagingStart())
            ->limit($queryOptions->getPagingLength())
            ;

        $query->orderBy('start_at', 'desc');

        $data = $query->get();

        return new QueryResult($totalCount, $totalCount, $data);
    }

    public function getSessionForSiteByTime(Website $site, Carbon $requestedTime)
    {
        return
            $site->paymentMethodSessions()
                ->where('start_at', '<=', $requestedTime)
                ->orderBy('start_at', 'desc')
                ->first();
    }

    public function getCurrentSessionsForStripeAcc(StripeAccount $acc)
    {
        return
            $site->paymentMethodSessions()
                ->whereNull('end_at')
                ->where('payment_method', PaymentMethod::STRIPE)
                ->where('payment_acc_id', $acc->id)
                ->orderBy('start_at', 'desc')
                ->with('website')
                ->get()
                ->unique('website_id');
    }

    public function getLatestSessionForStripeAcc(StripeAccount $acc)
    {
        return
            $acc->paymentMethodSessions()
                ->orderBy('start_at', 'desc')
                ->with('website')
                ->first();
    }
}
