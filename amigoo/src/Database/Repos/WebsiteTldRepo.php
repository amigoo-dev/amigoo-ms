<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\WebsiteTld;

class WebsiteTldRepo extends \App\Repositories\BaseRepository
{
    public function __construct(WebsiteTld $model)
    {
        $this->model = $model;
    }

    public function create(Website $site, string $tld): WebsiteTld
    {
        $newRecord = new WebsiteTld(compact('tld'));

        $site->availableTlds()->save($newRecord);

        return $newRecord;
    }

    public function update(WebsiteTld $tldRecord, string $tld)
    {
        $tldRecord->tld = $tld;
        $tldRecord->save();
    }

    public function delete(WebsiteTld $tldRecord)
    {
        $tldRecord->delete();
    }

    public function getByTld(string $tld): WebsiteTld
    {
        return $this->model->newQuery()
            ->with('website')
            ->where('tld', $tld)
            ->firstOrFail();
    }

    public function exists(string $tld): bool
    {
        $count = $this->model->newQuery()
            ->where('tld', $tld)
            ->count();

        return ($count > 0);
    }
}
