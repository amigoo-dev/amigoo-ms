<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\Website;
use App\Models\Auth\User;

class WebsiteUserModeratablesRepo extends \App\Repositories\BaseRepository
{
    public function setModeratableSitesForUser(User $user, array $moderatableSitesIds)
    {
        $user->moderatableWebsites()->sync($moderatableSitesIds);
    }

    public function updateEnabledNotifications(User $user, Website $moderatableSite, array $selectedNotifications)
    {
        $enabledNotificationsStr =
            empty($selectedNotifications)
                ? null
                : $this->makeEnabledNotificationsString($selectedNotifications);

        $moderatableSite->moderators()->updateExistingPivot($user->id, [
            'enabled_notifications' => $enabledNotificationsStr
        ]);
    }

    private function makeEnabledNotificationsString(array $selectedNotifications): string
    {
        $str = join('|', $selectedNotifications);
        $str = "|$str|";

        return $str;
    }
}