<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\WebsiteTld;
use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Constants\WebsiteType;
use Amigoo\Helpers\TldExtractor;
use App\Exceptions\GeneralException;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;

class WebsitesRepo extends \App\Repositories\BaseRepository
{
    public function __construct(Website $model)
    {
        $this->model = $model;
    }

    public function all(bool $includeInactive = false)
    {
        $query = $this->model->newQuery()
            ->orderBy('name')
            ->with('stripeAccount');

        if ($includeInactive)
            $query->withTrashed();

        return $query->get();
    }

    public function create(string $type, array $input): Website
    {
        $input['auto_switch_after_mins']             = Website::DEFAULT_AUTO_SWITCH_AFTER_MINS;
        $input['change_stripe_status_failed_amount'] = Website::DEFAULT_CHANGE_STRIPE_STATUS_FAILED_AMOUNT;

        $website =
            DB::transaction(function() use ($input, $type) {
                $website = new Website($input);
                $website->type = $type;

                $website->save();

                $tld            = TldExtractor::extract($website->url);
                $firstTldRecord = new WebsiteTld(compact('tld'));
                $website->availableTlds()->save($firstTldRecord);

                return $website;
            });

        return $website;
    }

    public function update(Website $website, array $input)
    {
        $website->update($input);
    }

    public function activate(Website $website)
    {
        $failed = ! $website->restore();
        if ($failed)
            throw new GeneralException(__('amigoo.backend.websites.errors.activation'));
    }

    public function deactivate(Website $website)
    {
        DB::transaction(function () use ($website) {
            // MUST unlink BECAUSE this is SOFT DELETE
            $this->unlinkFromStripeAccount($website);

            $this->removeAllLinkables($website);

            $this->softDelete($website);
        });
    }

    public function linkToStripeAccount(Website $website, StripeAccount $stripeAcc)
    {
        $website->stripeAccount()->associate($stripeAcc);

        $website->save();
    }

    public function linkMultipleWebsitesToStripeAccount(array $websites, StripeAccount $stripeAcc)
    {
        DB::transaction(function() use ($websites, $stripeAcc)  {
            foreach ($websites as $site)
                $this->linkToStripeAccount($site, $stripeAcc);
        });
    }

    public function unlinkFromStripeAccount(Website $website)
    {
        $website->stripeAccount()->dissociate();

        $website->save();
    }

    public function setLinkableStripeAccs(Website $website, array $linkableStripeAccsIds, ?int $newDefaultStripeAccId)
    {
        $linkablesNotEmpty = ! empty($linkableStripeAccsIds);

        $defaultLinkableNotAllowed = $linkablesNotEmpty && ! in_array($newDefaultStripeAccId, $linkableStripeAccsIds);
        if ($defaultLinkableNotAllowed)
            throw new \Exception('The selected default Stripe acc. is not in the list of linkable ones');

        DB::transaction(function() use (
                $website,
                $linkableStripeAccsIds,
                $newDefaultStripeAccId,
                $linkablesNotEmpty
                ) {
            $website->linkableStripeAccounts()->sync($linkableStripeAccsIds);

            if ($linkablesNotEmpty)
                $this->setDefaultAccount($website, $newDefaultStripeAccId);
        });
    }

    public function setDefaultAccount(Website $website, int $newDefaultStripeAccId)
    {
        $curDefaultStripeAcc = $website->linkableStripeAccounts()
            ->wherePivot('is_default', true)
            ->first();

        if (! $curDefaultStripeAcc) {
            $website->linkableStripeAccounts()->updateExistingPivot($newDefaultStripeAccId, ['is_default' => true]);
        } elseif ($curDefaultStripeAcc->id !== $newDefaultStripeAccId) {
            $website->linkableStripeAccounts()->updateExistingPivot($curDefaultStripeAcc->id, ['is_default' => false]);
            $website->linkableStripeAccounts()->updateExistingPivot($newDefaultStripeAccId,   ['is_default' => true]);
        }
    }

    public function getSitesModeratableByUser(User $moderator)
    {
        return $moderator->moderatableWebsites()
            ->with('stripeAccount')
            ->get();
    }

    public function getSitesHaveLinkingStripe()
    {
        return $this->model->newQuery()
            ->whereNotNull('stripe_acc_id')
            ->get()
            ;
    }

    public function getSitesLinkableToStripeAccount(StripeAccount $stripeAcc)
    {
        return $stripeAcc->linkableWebsites;
    }

    public function getSitesByStripeAccount($linkedAcc)
    {
        return $this->model->newQuery()
            ->where('stripe_acc_id', $linkedAcc->id)
            ->with('stripeAccount')
            ->get();
    }

    public function getMerchizeSiteBySecretKey($secretKey)
    {
        return $this->model->newQuery()
            ->where('type', WebsiteType::MERCHIZE)
            ->where('webhook_secret', $secretKey)
            ->with('stripeAccount')
            ->first();
    }

    public function removeAllLinkables(Website $website)
    {
        $website->linkableStripeAccounts()->detach();
    }

    private function softDelete(Website $website)
    {
        // Website model is configured to be soft-deleted.
        return $website->delete();
    }
}
