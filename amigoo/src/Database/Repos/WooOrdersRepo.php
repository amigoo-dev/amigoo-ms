<?php

namespace Amigoo\Database\Repos;

use Amigoo\Database\Models\Constants\OrderStatus;
use Amigoo\Database\Models\Constants\PaymentMethod;
use Amigoo\Database\Models\WooOrder;
use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use App\Models\Auth\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class WooOrdersRepo extends \App\Repositories\BaseRepository
{
    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    public function __construct(WooOrder $model, StripeAccountsRepo $stripeAccsRepo)
    {
        $this->model = $model;

        $this->stripeAccsRepo = $stripeAccsRepo;
    }

    public function insert(
            Website $site,
            string $paymentMethod,
            ?int $stripeAccId,
            int $wooOrderId,
            string $status,
            float $total,
            Carbon $createdAt,
            ?Carbon $paidAt = null
            ): WooOrder {
        if ($this->recordExists($site, $wooOrderId))
            throw new Exceptions\OrderAlreadyInsertedException($site, $wooOrderId);

        $order = new WooOrder;
        $order->website_id       = $site->id;
        $order->payment_method   = $paymentMethod;
        $order->stripe_acc_id    = $stripeAccId;
        $order->woo_order_id     = $wooOrderId;
        $order->status           = $status;
        $order->total            = $total;
        $order->created_at       = $createdAt;
        $order->paid_at          = $paidAt;

        $order->save();

        return $order;
    }

    public function changeStatusFromFailedToProcessingOrCompleted(
            Website $site,
            int $wooOrderId,
            int $stripeAccId,
            string $newStatus,
            Carbon $paidAt
            ): WooOrder {
        $newStatusNotAllowed = ! in_array($newStatus, [OrderStatus::PROCESSING, OrderStatus::COMPLETED]);
        if ($newStatusNotAllowed)
            throw new \Exception("New status [{$newStatus}] is not allowed");

        $order = $this->model->newQuery()
            ->where([
                'website_id'   => $site->id,
                'woo_order_id' => $wooOrderId,
            ])
            ->firstOrFail();

        $order->stripe_acc_id = $stripeAccId;
        $order->paid_at       = $paidAt;
        $order->status        = $newStatus;
        $order->save();

        return $order;
    }

    public function updateMetadata(WooOrder $order, array $data)
    {
        $metadata = Arr::only($data, [
            'order_number',
            'stripe_intent_id',
            'stripe_charge_id',
        ]);

        foreach ($metadata as $field => $val)
            $order->$field = $val;

        $order->save();
    }

    public function getOrdersBySite(Website $site, QueryOptions $queryOptions, User $moderator = null): QueryResult
    {
        $keyword = trim($queryOptions->getFilteringKeyword());
        $query   = $site->wooOrders();

        if ($moderator) {
            $moderatableStripeAccsIds = $this->stripeAccsRepo->getAccountsByModerator($moderator)->pluck('id');
            $query->whereIn('stripe_acc_id', $moderatableStripeAccsIds);
        }

        $totalCount = $query->count();

        if ($keyword !== '')
            $query
                ->where('woo_order_id', 'LIKE', "%$keyword%")
                ->orWhere('order_number', 'LIKE', "%{$keyword}%")
                ;

        $filteredCount = $query->count();

        $query
            ->with('stripeAccount')
            ->offset($queryOptions->getPagingStart())
            ->limit($queryOptions->getPagingLength())
        ;

        foreach ($queryOptions->getOrderings() as $ordering) {
            $query->orderBy($ordering['field'], $ordering['direction']);
        }

        $data = $query->get();

        return new QueryResult($totalCount, $filteredCount, $data);
    }

    public function getOrdersNeedCollectingDataBySite(Website $site)
    {
        return $site->wooOrders()
            ->whereIn('status', [OrderStatus::PROCESSING, OrderStatus::COMPLETED])
            ->where(fn($q) => $q
                ->whereNull('order_number')
                ->orWhereNull('stripe_intent_id')
                ->orWhereNull('stripe_charge_id')
            )
            ->get();
    }

    public function getLatestOrderForSite(Website $site): ?WooOrder
    {
        return $site->wooOrders()
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function getTodayReceivedAmountForSite(int $siteId): float
    {
        $todayVn = $this->getStartAtEndAtUtc7(Carbon::now());

        return $this->model->newQuery()
            ->where([
                ['website_id', $siteId],
            ])
            ->whereIn('status', [OrderStatus::PROCESSING, OrderStatus::COMPLETED])
            ->whereBetween('paid_at', [
                $todayVn['startAt'],
                $todayVn['endAt']
            ])
            ->sum('total');
    }

    public function sumTotalForSite(Website $site, ?string $filteredStatus = null): float
    {
        $query = $site->wooOrders();

        if ($filteredStatus)
            $query->where('status', $filteredStatus);

        return $query->sum('total');
    }

    public function countOrdersBySite(Website $site, ?string $filteredStatus = null): int
    {
        $query = $site->wooOrders();

        if ($filteredStatus)
            $query->where('status', $filteredStatus);

        return $query->count();
    }

    public function getOrdersByStripeAcc(StripeAccount $acc, QueryOptions $queryOptions, User $moderator = null): QueryResult
    {
        $keyword = trim($queryOptions->getFilteringKeyword());
        $query   = $acc->wooOrders();

        if ($moderator) {
            $moderatableSitesIds = $moderator->moderatableWebsites->pluck('id');
            $query->whereIn('website_id', $moderatableSitesIds);
        }

        $totalCount = $acc->wooOrders()->count();

        if ($keyword !== '')
            $query->where('woo_order_id', 'LIKE', "%{$keyword}%");

        $filteredCount = $query->count();

        $query
            ->with('website')
            ->offset($queryOptions->getPagingStart())
            ->limit($queryOptions->getPagingLength())
        ;

        foreach ($queryOptions->getOrderings() as $ordering) {
            $query->orderBy($ordering['field'], $ordering['direction']);
        }

        $data = $query->get();

        return new QueryResult($totalCount, $filteredCount, $data);
    }

    public function getTodayReceivedAmountForStripeAcc(int $stripeAccId): float
    {
        return $this->getReceivedAmountForStripeAccByDate($stripeAccId, Carbon::now());
    }

    public function getReceivedAmountForStripeAccByDate(int $stripeAccId, Carbon $date): float
    {
        $todayVn = $this->getStartAtEndAtUtc7($date);

        return $this->model->newQuery()
            ->where([
                ['stripe_acc_id', $stripeAccId],
                ['payment_method', PaymentMethod::STRIPE],
            ])
            ->whereIn('status', [OrderStatus::PROCESSING, OrderStatus::COMPLETED])
            ->whereBetween('paid_at', [
                $todayVn['startAt'],
                $todayVn['endAt']
            ])
            ->sum('total');
    }

    public function getTotalReceivedAmountForStripeAcc(int $stripeAccId): float
    {
        return $this->model->newQuery()
            ->where([
                ['stripe_acc_id', $stripeAccId],
                ['payment_method', PaymentMethod::STRIPE],
            ])
            ->whereIn('status', [OrderStatus::PROCESSING, OrderStatus::COMPLETED])
            ->sum('total');
    }

    public function getTodayReceivedAmountForAllStripeAccs(): array
    {
        return $this->getReceivedAmountForAllStripeAccsByDate(Carbon::now());
    }

    public function getReceivedAmountForAllStripeAccsByDate(Carbon $date): array
    {
        $todayVn = $this->getStartAtEndAtUtc7($date);

        $queryResult = $this->model->newQuery()
            ->select(
                'stripe_acc_id',
                DB::raw('SUM(`total`) as `received_today_sum`')
            )
            ->where([
                ['payment_method', PaymentMethod::STRIPE],
            ])
            ->whereIn('status', [OrderStatus::PROCESSING, OrderStatus::COMPLETED])
            ->whereBetween('paid_at', [
                $todayVn['startAt'],
                $todayVn['endAt']
            ])
            ->groupBy('stripe_acc_id')
            ->get();

        $map = [];
        foreach ($queryResult as $record) {
            $map[$record->stripe_acc_id] = $record->received_today_sum;
        }

        return $map;
    }

    public function getTotalReceivedAmountForAllStripeAccs(): array
    {
        $queryResult = $this->model->newQuery()
            ->select(
                'stripe_acc_id',
                DB::raw('SUM(`total`) as `total_received_sum`')
            )
            ->where([
                ['payment_method', PaymentMethod::STRIPE],
            ])
            ->whereIn('status', [OrderStatus::PROCESSING, OrderStatus::COMPLETED])
            ->groupBy('stripe_acc_id')
            ->get();

        $map = [];
        foreach ($queryResult as $record) {
            $map[$record->stripe_acc_id] = $record->total_received_sum;
        }

        return $map;
    }

    public function getLatestOrderForStripeAcc(int $stripeAccId): ?WooOrder
    {
        return $this->getLatestOrdersForStripeAcc($stripeAccId, 1)->first();
    }

    public function getLatestOrdersForStripeAcc(int $stripeAccId, int $limit): Collection
    {
        return $this->model->newQuery()
            ->where('stripe_acc_id', $stripeAccId)
            ->take($limit)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function countLatestConsecutiveFailuresForStripeAcc(int $stripeAccId, int $limit): int
    {
        return $this->getLatestOrdersForStripeAcc($stripeAccId, $limit)
            ->filter(fn($order) => $order->has_status_failed)
            ->count();
    }

    private function recordExists(Website $site, int $wooOrderId)
    {
        return $this->model->newQuery()
            ->where([
                ['website_id', $site->id],
                ['woo_order_id', $wooOrderId]
            ])
            ->exists();
    }

    private function getStartAtEndAtUtc7(Carbon $now)
    {
        $hanoiNow = $now->copy()->addHours(7);
        $hanoiTodayStartAt = $hanoiNow->copy()->setTime(0, 0, 0);
        $hanoiTodayStartAtAsUtc = $hanoiTodayStartAt->copy()->subHours(7);
        $hanoiTodayEndAtAsUtc   = $hanoiTodayStartAtAsUtc->copy()->addHours(24);

        return [
            'startAt' => $hanoiTodayStartAtAsUtc,
            'endAt'   => $hanoiTodayEndAtAsUtc
        ];
    }
}
