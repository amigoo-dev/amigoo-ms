<?php

namespace Amigoo\Helpers;

abstract class TldExtractor
{
    public static function extract(string $url): string
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';

        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }

        throw new \Exception("Passed argument is not a URL");
    }
}
