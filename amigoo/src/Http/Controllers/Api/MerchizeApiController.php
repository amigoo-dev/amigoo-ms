<?php

namespace Amigoo\Http\Controllers\Api;

use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Illuminate\Http\Request;

class MerchizeApiController extends \App\Http\Controllers\Controller
{
    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var MerchizeOrdersRepo */
    private $merchizeOrdersRepo;

    public function __construct(WebsitesRepo $websitesRepo, MerchizeOrdersRepo $merchizeOrdersRepo)
    {
        $this->websitesRepo       = $websitesRepo;
        $this->merchizeOrdersRepo = $merchizeOrdersRepo;
    }

    public function getCurrentPaymentGateway(Request $req)
    {
        $siteSecretKey = $req->get('secret_key');
        if (! $siteSecretKey)
            return response()->json([
                'message' => 'The given data was invalid',
                'errors' => [
                    'secret_key' => 'The secret key field is required'
                ],
            ], 422);

        $site = $this->websitesRepo->getMerchizeSiteBySecretKey($siteSecretKey);
        if (! $site)
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $curStripeAcc = $site->stripeAccount;

        if (! $curStripeAcc)
            return response()->json([
                'data' => null
            ]);

        return response()->json([
            'data' => [
                'email'      => $curStripeAcc->email,
                'public_key' => $curStripeAcc->public_key,
                'secret_key' => $curStripeAcc->secret_key,
            ]
        ]);
    }

    public function getOrderDetailsByOrderCode($orderCode, Request $req)
    {
        $siteSecretKey = $req->get('secret_key');
        if (! $siteSecretKey)
            return response()->json([
                'message' => 'The given data was invalid',
                'errors' => [
                    'secret_key' => 'The secret key field is required'
                ],
            ], 422);

        $site = $this->websitesRepo->getMerchizeSiteBySecretKey($siteSecretKey);
        if (! $site)
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $order = $this->merchizeOrdersRepo->getOrderByOrderCode($orderCode);
        if (! $order)
            return response()->json([
                'message' => "Order code {$orderCode} not found"
            ], 404);

        $notOrderOfRequestedSite = ($order->website->id != $site->id);
        if ($notOrderOfRequestedSite)
            return response()->json([
                'message' => 'Forbidden'
            ], 403);

        return response()->json([
            'data' => [
                'stripe' => [
                    'email' => $order->stripeAccount->email
                ]
            ]
        ]);
    }
}
