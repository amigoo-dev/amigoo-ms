<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Database\Repos\WebsiteUserModeratablesRepo;
use Amigoo\Http\Requests\Auth\ConfigModeratableWebsitesRequest;
use App\Models\Auth\User;

class ConfigModeratableWebsitesController extends Controller
{
    public function show(WebsitesRepo $sitesRepo, User $user)
    {
        $websites               = $sitesRepo->all();
        $curModeratableSitesIds = $user->moderatableWebsites->pluck('id');

        return view('amigoo::backend.auth.config-moderatable-websites', compact(
            'user',
            'websites',
            'curModeratableSitesIds',
        ));
    }

    public function update(
            WebsiteUserModeratablesRepo $websiteUserModeratablesRepo,
            ConfigModeratableWebsitesRequest $req,
            User $user
            ){
        $moderatableSitesIds = $req->input('moderatable_websites_ids', []);

        $websiteUserModeratablesRepo->setModeratableSitesForUser($user, $moderatableSitesIds);

        return redirect()->route('admin.auth.user.config-moderatable-websites.show', $user);
    }
}
