<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Http\Requests\Website\ConfigSiteModsNotificationsRequest;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Repos\UsersRepo;
use Amigoo\Database\Repos\WebsiteUserModeratablesRepo;

class ConfigSiteModsNotificationsController extends Controller
{
    /** @var UsersRepo */
    private $usersRepo;

    /** @var WebsiteUserModeratablesRepo */
    private $moderatablesRepo;

    public function __construct(UsersRepo $usersRepo, WebsiteUserModeratablesRepo $moderatablesRepo)
    {
        $this->usersRepo        = $usersRepo;
        $this->moderatablesRepo = $moderatablesRepo;
    }

    public function show(Website $website)
    {
        foreach ($website->moderators as $mod) {
            $mod->enabled_notifications =
                $mod->pivot->enabled_notifications
                    ? collect($this->explodeEnabledNotificationsString($mod->pivot->enabled_notifications))
                    : collect([]);
        }

        return view('amigoo::backend.websites.config-site-mods-notifications', compact('website'));
    }

    public function submit(Website $website, ConfigSiteModsNotificationsRequest $req)
    {
        $userIdToNotificationsMap = $req->input('user_id_to_notifications_map');

        foreach ($website->moderators as $mod) {
            $selectedNotifications =
                isset($userIdToNotificationsMap[$mod->id])
                    ? $userIdToNotificationsMap[$mod->id]
                    : [];

            $this->moderatablesRepo->updateEnabledNotifications($mod, $website, $selectedNotifications);
        }

        return redirect()->route('admin.websites.post.form.config-site-mods-notifications', $website);
    }

    private function explodeEnabledNotificationsString(string $enabledNotificationsStr)
    {
        $separator = '|';
        $trimmed = trim($enabledNotificationsStr, $separator);
        $exploded = explode($separator, $trimmed);

        return $exploded;
    }
}
