<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Amigoo\Database\Repos\QueryOptions;
use Illuminate\Http\Request;

class MerchizeOrdersAjaxController extends Controller
{
    /** @var MerchizeOrdersRepo */
    private $merchizeOrdersRepo;

    public function __construct(MerchizeOrdersRepo $merchizeOrdersRepo)
    {
        $this->merchizeOrdersRepo = $merchizeOrdersRepo;
    }

    public function getOrdersByWebsite(Request $req, Website $website)
    {
        $curUser      = auth()->user();
        $queryOptions = $this->getQueryOptionsFromRequest($req);

        $queryResult =
            $curUser->isAdmin()
                ? $this->merchizeOrdersRepo->getOrdersBySite($website, $queryOptions)
                : $this->merchizeOrdersRepo->getOrdersBySite($website, $queryOptions, $moderator = $curUser);

        $responsePayload = [
            'draw'            => $req->get('draw', 1),
            'recordsTotal'    => $queryResult->totalCount,
            'recordsFiltered' => $queryResult->filteredCount,
            'data'            => $queryResult->data,
        ];

        foreach ($responsePayload['data'] as $order) {
            $stripeAcc = $order->stripeAccount;

            $stripeAcc['_links'] = [
                'details' => route('admin.stripe-accs.get.form.update', $stripeAcc),
            ];
        }

        return $responsePayload;
    }

    public function getOrdersByStripeAcc(Request $req, StripeAccount $acc)
    {
        $curUser      = auth()->user();
        $queryOptions = $this->getQueryOptionsFromRequest($req);

        $queryResult =
            $curUser->isAdmin()
                ? $this->merchizeOrdersRepo->getOrdersByStripeAcc($acc, $queryOptions)
                : $this->merchizeOrdersRepo->getOrdersByStripeAcc($acc, $queryOptions, $moderator = $curUser);

        $responsePayload = [
            'draw'            => $req->get('draw', 1),
            'recordsTotal'    => $queryResult->totalCount,
            'recordsFiltered' => $queryResult->filteredCount,
            'data'            => $queryResult->data,
        ];

        foreach ($responsePayload['data'] as $order) {
            $site = $order->website;

            $site['_links'] = [
                'details' => route('admin.websites.get.form.update', $site),
            ];
        }

        return $responsePayload;
    }

    private function getQueryOptionsFromRequest(Request $req): QueryOptions
    {
        $keyword = $req->input('search.value') ?: '';

        $queryOptions = (new QueryOptions)
            ->setFilteringKeyword($keyword)
            ->setPagingStart($req->get('start', 0))
            ->setPagingLength($req->get('length', QueryOptions::DEFAULT_QUERY_LIMIT))
        ;

        $reqOrderingOptions = $req->get('order', []);
        $colDefs            = $req->get('columns');
        foreach ($reqOrderingOptions as $ordering) {
            $colIdx    = $ordering['column'];
            $fieldName = $colDefs[$colIdx]['name'];
            $direction = $ordering['dir'];
            $queryOptions->addOrdering($fieldName, $direction);
        }

        return $queryOptions;
    }
}
