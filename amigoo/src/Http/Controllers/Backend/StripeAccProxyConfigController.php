<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Repos\ProxyInfoRepo;
use Amigoo\Http\Requests\StripeAcc\SaveProxyConfigRequest;

class StripeAccProxyConfigController extends Controller
{
    /** @var ProxyInfoRepo */
    private $proxyInfoRepo;

    public function __construct(ProxyInfoRepo $proxyInfoRepo)
    {
        $this->proxyInfoRepo = $proxyInfoRepo;
    }

    public function saveConfig(StripeAccount $acc, SaveProxyConfigRequest $req)
    {
        $alreadyInserted = !! $acc->proxyInfo;

        if ($alreadyInserted) {
            $this->proxyInfoRepo->update(
                $acc->proxyInfo,
                $req->all()
            );
        } else {
            $this->proxyInfoRepo->insertNewRecordForStripeAcc(
                $acc,
                $req->get('ip'),
                $req->get('port'),
                $req->get('user'),
                $req->get('password'),
                $req->get('type'),
                $req->get('provider')
            );
        }

        return redirect()->route('admin.stripe-accs.get.form.update', $acc);
    }
}
