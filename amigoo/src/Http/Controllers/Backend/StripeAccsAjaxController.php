<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\StripeReservedFund;
use Amigoo\Database\Repos\QueryOptions;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\StripePayoutsRepo;
use Amigoo\Database\Repos\WebsitesRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class StripeAccsAjaxController extends Controller
{
    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /** @var MerchizeOrdersRepo */
    private $merchizeOrdersRepo;

    /** @var StripePayoutsRepo */
    private $payoutsRepo;

    public function __construct(
        StripeAccountsRepo $stripeAccsRepo,
        WebsitesRepo $websitesRepo,
        WooOrdersRepo $wooOrdersRepo,
        MerchizeOrdersRepo $merchizeOrdersRepo,
        StripePayoutsRepo $payoutsRepo
        ) {
    $this->stripeAccsRepo     = $stripeAccsRepo;
    $this->websitesRepo       = $websitesRepo;
    $this->wooOrdersRepo      = $wooOrdersRepo;
    $this->merchizeOrdersRepo = $merchizeOrdersRepo;
    $this->payoutsRepo        = $payoutsRepo;
}

    public function getStripeAccounts(Request $req)
    {
        $curUser      = auth()->user();
        $queryOptions = $this->getQueryOptionsFromRequest($req);

        $endOfToday    = Carbon::tomorrow();
        $twoDaysBefore = Carbon::today()->sub('2 days');

        $queryResult =
            $curUser->can('viewAny', StripeAccount::class)
                ? $this->stripeAccsRepo->getAccounts($queryOptions)
                : $this->stripeAccsRepo->getAccounts($queryOptions, $moderator = $curUser);

        $accWooTodayReceived      = $this->wooOrdersRepo->getTodayReceivedAmountForAllStripeAccs();
        $accMerchizeTodayReceived = $this->merchizeOrdersRepo->getTodayReceivedAmountForAllStripeAccs();
        $accWooTotalReceived      = $this->wooOrdersRepo->getTotalReceivedAmountForAllStripeAccs();
        $accMerchizeTotalReceived = $this->merchizeOrdersRepo->getTotalReceivedAmountForAllStripeAccs();
        $defaultAccIdSitesMap     = $this->stripeAccsRepo->getDefaultAccountIdWebsitesMap();

        $canViewOrderPaymentsSummary = $curUser->can('viewOrderPaymentsSummaryAndList', StripeAccount::class);
        $cannotViewReservedFund      = ! $curUser->can('viewIndex', StripeReservedFund::class);
        $canViewPayout               = $curUser->can('viewStripePayouts', StripeAccount::class);

        foreach ($queryResult->data as $acc) {

            $acc->_meta = [
                'href' => route('admin.stripe-accs.get.form.update', compact('acc')),
            ];

            foreach ($acc->linkableWebsites as $website)
                $website->_meta = [
                    'href' => route('admin.websites.get.form.update', compact('website')),
                ];

            if ($cannotViewReservedFund)
                $acc->unsetRelation('reservedFund');

            if ($canViewOrderPaymentsSummary) {
                $acc->today_received = 0;
                $acc->total_received = 0;

                if (array_key_exists($acc->id, $accWooTodayReceived))
                    $acc->today_received += $accWooTodayReceived[$acc->id];
                if (array_key_exists($acc->id, $accMerchizeTodayReceived))
                    $acc->today_received += $accMerchizeTodayReceived[$acc->id];

                if (array_key_exists($acc->id, $accWooTotalReceived))
                    $acc->total_received += $accWooTotalReceived[$acc->id];
                if (array_key_exists($acc->id, $accMerchizeTotalReceived))
                    $acc->total_received += $accMerchizeTotalReceived[$acc->id];
            }

            $latestWooOrder      = $this->wooOrdersRepo->getLatestOrderForStripeAcc($acc->id);
            $latestMerchizeOrder = $this->merchizeOrdersRepo->getLatestOrderForStripeAcc($acc->id);
            $acc->latest_order =
                $latestWooOrder > $latestMerchizeOrder
                    ? $latestWooOrder
                    : $latestMerchizeOrder;

            if ($canViewPayout)
                $acc->po_amount_last2days = $this->payoutsRepo->getTotalAmountInPeriod($acc, $twoDaysBefore, $endOfToday);
        }

        return [
            'draw'          => $req->get('draw', 1),
            'recordsTotal'  => $queryResult->totalCount,
            'recordsFilter' => $queryResult->filteredCount,
            'data'          => $queryResult->data,
        ];
    }

    private function getQueryOptionsFromRequest(Request $req): QueryOptions
    {
        $queryOptions = (new QueryOptions)->setNoPaging();

        foreach ($req->get('columns') as $col) {
            switch ($col['name']) {
                case 'email':
                case 'owner_name':
                case 'proxy_info':
                case 'status':
                    $queryOptions->addFilteringOption($col['name'], $col['search']['value']);
                    continue 2;
            }
        }

        $reqOrderingOptions = $req->get('order', []);
        $colDefs            = $req->get('columns');
        foreach ($reqOrderingOptions as $ordering) {
            $colIdx    = $ordering['column'];
            $fieldName = $colDefs[$colIdx]['name'];
            $direction = $ordering['dir'];
            $queryOptions->addOrdering($fieldName, $direction);
        }

        return $queryOptions;
    }
}
