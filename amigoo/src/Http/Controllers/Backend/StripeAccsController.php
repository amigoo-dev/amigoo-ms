<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\StripeReservedFund;
use Amigoo\Database\Models\Constants\StripeAccountStatus as Status;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Amigoo\Database\Repos\ProxyInfoRepo;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\StripePayoutsRepo;
use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Http\Requests\StripeAcc\CreationRequest as StripeAccCreationRequest;
use Amigoo\Http\Requests\StripeAcc\UpdationRequest as StripeAccUpdationRequest;
use Amigoo\Services\WooCommerce\Stripe\AccountSwitcher;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class StripeAccsController extends Controller
{
    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /** @var MerchizeOrdersRepo */
    private $merchizeOrdersRepo;

    /** @var StripePayoutsRepo */
    private $payoutsRepo;

    /** @var ProxyInfoRepo */
    private $proxyInfoRepo;

    public function __construct(
            StripeAccountsRepo $stripeAccsRepo,
            WebsitesRepo $websitesRepo,
            WooOrdersRepo $wooOrdersRepo,
            MerchizeOrdersRepo $merchizeOrdersRepo,
            StripePayoutsRepo $payoutsRepo,
            ProxyInfoRepo $proxyInfoRepo
            ) {
        $this->stripeAccsRepo     = $stripeAccsRepo;
        $this->websitesRepo       = $websitesRepo;
        $this->wooOrdersRepo      = $wooOrdersRepo;
        $this->merchizeOrdersRepo = $merchizeOrdersRepo;
        $this->payoutsRepo        = $payoutsRepo;
        $this->proxyInfoRepo      = $proxyInfoRepo;
    }

    public function getIndex()
    {
        $curUser = auth()->user();

        $queryResult =
            auth()->user()->can('viewAny', StripeAccount::class)
                ? $this->stripeAccsRepo->getAccounts()
                : $this->stripeAccsRepo->getAccounts(null, $curUser);

        $stripeAccs = $queryResult->data;

        $canViewOrderPaymentsSummary = auth()->user()->can('viewOrderPaymentsSummaryAndList', StripeAccount::class);
        $canViewReservedFund         = auth()->user()->can('viewIndex', StripeReservedFund::class);
        $canViewPayout               = auth()->user()->can('viewStripePayouts', StripeAccount::class);


        return view('amigoo::backend.stripe-accs.index', compact(
            'stripeAccs',
            'canViewOrderPaymentsSummary',
            'canViewReservedFund',
            'canViewPayout',
        ));
    }

    public function getCreationForm()
    {
        $acc                         = new StripeAccount;
        $websites                    = $this->websitesRepo->getSitesHaveLinkingStripe()->sortBy('name');
        $curLinkableSiteId           = old('linkable_site_id');
        $curLinkedSite               = $acc->linkedWebsites->first();
        $isCurLinkableSiteDefaultAcc = false;

        return view('amigoo::backend.stripe-accs.create', compact(
            'acc',
            'websites',
            'curLinkedSite',
            'curLinkableSiteId',
            'isCurLinkableSiteDefaultAcc'
        ));
    }

    public function postCreationForm(StripeAccCreationRequest $req)
    {
        $stripeAccInput                           = $req->except('linkable_site_id', 'proxy_info');
        $stripeAccInput['should_wait_for_payout'] = $req->get('should_wait_for_payout', false);

        $acc = $this->stripeAccsRepo->create($stripeAccInput);

        $shouldInsertProxyInfo = $req->input('proxy_info.insert');
        if ($shouldInsertProxyInfo) {
            $this->proxyInfoRepo->insertNewRecordForStripeAcc(
                $acc,
                $req->input('proxy_info.ip'),
                $req->input('proxy_info.port'),
                $req->input('proxy_info.user'),
                $req->input('proxy_info.password'),
                $req->input('proxy_info.type'),
                $req->input('proxy_info.provider')
            );
        }

        $linkableSiteId = $req->get('linkable_site_id');
        if ($linkableSiteId) {
            $linkableSite = $this->websitesRepo->getById($linkableSiteId);
            $this->stripeAccsRepo->setLinkableWebsite($acc, $linkableSite);
        }

        return redirect()->route('admin.stripe-accs.get.form.update', compact('acc'));
    }

    public function getUpdationForm(StripeAccount $acc)
    {
        $websites = $this->websitesRepo->getSitesHaveLinkingStripe()->sortBy('name');

        $curLinkedSite = $acc->linkedWebsites->first();

        $curLinkableSite             = $acc->linkableWebsites()->first();
        $curLinkableSiteId           = $curLinkableSite ? $curLinkableSite->id : null;
        $isCurLinkableSiteDefaultAcc = $curLinkableSite ? $curLinkableSite->linkable->is_default: false;

        $curUser = auth()->user();
        $canViewOrderPaymentsSummary = $curUser->can('viewOrderPaymentsSummaryAndList', StripeAccount::class);
        $todayReceived = null;
        $totalReceived = null;
        if ($canViewOrderPaymentsSummary) {
            $wooOrdersTodayReceived      = $this->wooOrdersRepo->getTodayReceivedAmountForStripeAcc($acc->id);
            $merchizeOrdersTodayReceived = $this->merchizeOrdersRepo->getTodayReceivedAmountForStripeAcc($acc->id);
            $todayReceived               = $wooOrdersTodayReceived + $merchizeOrdersTodayReceived;

            $wooOrdersTotalReceived      = $this->wooOrdersRepo->getTotalReceivedAmountForStripeAcc($acc->id);
            $merchizeOrdersTotalReceived = $this->merchizeOrdersRepo->getTotalReceivedAmountForStripeAcc($acc->id);
            $totalReceived               = $wooOrdersTotalReceived + $merchizeOrdersTotalReceived;
        }

        return view('amigoo::backend.stripe-accs.update', compact(
            'acc',
            'websites',
            'todayReceived',
            'totalReceived',
            'curLinkedSite',
            'curLinkableSite',
            'curLinkableSiteId',
            'isCurLinkableSiteDefaultAcc'
        ));
    }

    public function postUpdationForm(AccountSwitcher $switcher, StripeAccUpdationRequest $req, StripeAccount $acc)
    {
        $redirectBackToForm = redirect()->route('admin.stripe-accs.get.form.update', compact('acc'));

        $input                           = $req->all();
        $input['should_wait_for_payout'] = $req->get('should_wait_for_payout', false);

        $formSelectedStatus   = $req->get('status');
        $isNewStatus          = $formSelectedStatus && ($formSelectedStatus != $acc->status);
        $hasNewStatusNotReady = $isNewStatus && ($formSelectedStatus != Status::READY);

        $this->stripeAccsRepo->update($acc, $input);

        if ($req->has('linkable_site_id')) {
            $linkableSiteId = $req->get('linkable_site_id');
            $linkableSite   = $linkableSiteId ? $this->websitesRepo->getById($linkableSiteId) : null;
            $this->stripeAccsRepo->setLinkableWebsite($acc, $linkableSite);
        }

        if ($hasNewStatusNotReady) {
            $switcherResult = $switcher->switch($acc, $formSelectedStatus, $forceStatusChange = true);

            if ($switcherResult && $switcherResult->hasFailure())
                $this->setFlashDangerForSwitcherFailures($redirectBackToForm, $switcherResult->failures);
        }

        return $redirectBackToForm;
    }

    private function setFlashDangerForSwitcherFailures($resp, $failures)
    {
        $strFailedSites =
            join(', ',
                array_map(
                    fn($failure) => $failure->site->url,
                    $failures
                )
            );

        $resp->withFlashDanger("Could not update some websites' Stripe accounts through API. Please update by hand on {$strFailedSites}");
    }
}
