<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\StripeDispute;
use Amigoo\Database\Models\Constants\StripeDisputeStatus as Status;
use Amigoo\Database\Repos\StripeDisputeRepo;
use Amigoo\Database\Repos\WebsitesRepo;
use Illuminate\Http\Request;

class StripeDisputesController extends Controller
{
    /** @var StripeDisputeRepo */
    private $disputesRepo;

    /** @var WebsitesRepo */
    private $websitesRepo;

    public function __construct(StripeDisputeRepo $disputesRepo, WebsitesRepo $websitesRepo)
    {
        $this->disputesRepo = $disputesRepo;
        $this->websitesRepo = $websitesRepo;
    }

    public function getIndex()
    {
        $curUser = auth()->user();

        $disputes =
            $curUser->can('viewAny', StripeDispute::class)
                ? $this->disputesRepo->all()
                : $this->disputesRepo->getDisputesByModerator($curUser);

        return view('amigoo::backend.stripe-disputes.index', compact('disputes'));
    }

    public function getUpdationForm(StripeDispute $dispute)
    {
        $websites = $this->websitesRepo->all($includeInactive = true);

        return view('amigoo::backend.stripe-disputes.update', compact('dispute', 'websites'));
    }

    public function postUpdationForm(StripeDispute $dispute, Request $request)
    {
        $input = $request->all('website_id', 'status');

        $this->disputesRepo->updateDisputeWebsite($dispute, $input);

        return redirect()->route('admin.stripe-disputes.get.form.update', $dispute);
    }
}
