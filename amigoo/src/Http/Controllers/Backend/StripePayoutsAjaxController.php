<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Repos\QueryOptions;
use Amigoo\Database\Repos\StripePayoutsRepo;
use Amigoo\Services\Stripe\StripePayoutsCollector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StripePayoutsAjaxController extends Controller
{
    /** @var StripePayoutsRepo */
    private $payoutsRepo;

    public function __construct(StripePayoutsRepo $payoutsRepo)
    {
        $this->payoutsRepo = $payoutsRepo;
    }

    public function runCommandRetrieveAllPayouts(StripeAccount $acc, StripePayoutsCollector $payoutsCollector)
    {
        try {
            $ok = $payoutsCollector->collect($acc);

            return response(compact('ok'));

        } catch (\Exception $e) {
            Log::error("Failed to collect payouts manually for {$acc->email}", compact('e'));
            return response([
                'ok'      => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function getPayoutsByStripeAcc(StripeAccount $acc, Request $req)
    {
        $curUser      = auth()->user();
        $queryOptions = $this->getQueryOptionsFromRequest($req);

        $queryResult = $this->payoutsRepo->getPayoutsByStripeAcc($acc, $queryOptions);

        $responsePayload = [
            'draw'            => $req->get('draw', 1),
            'recordsTotal'    => $queryResult->totalCount,
            'recordsFiltered' => $queryResult->filteredCount,
            'data'            => $queryResult->data,
        ];

        return $responsePayload;
    }


    private function getQueryOptionsFromRequest(Request $req): QueryOptions
    {
        $keyword = $req->input('search.value') ?: '';

        $queryOptions = (new QueryOptions)
            ->setFilteringKeyword($keyword)
            ->setPagingStart($req->get('start', 0))
            ->setPagingLength($req->get('length', QueryOptions::DEFAULT_QUERY_LIMIT))
        ;

        $reqOrderingOptions = $req->get('order', []);
        $colDefs            = $req->get('columns');
        foreach ($reqOrderingOptions as $ordering) {
            $colIdx    = $ordering['column'];
            $fieldName = $colDefs[$colIdx]['name'];
            $direction = $ordering['dir'];
            $queryOptions->addOrdering($fieldName, $direction);
        }

        return $queryOptions;
    }
}
