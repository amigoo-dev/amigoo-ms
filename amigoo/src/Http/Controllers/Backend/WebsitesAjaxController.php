<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\WebsiteTld;
use Amigoo\Database\Repos\QueryOptions;
use Amigoo\Database\Repos\WebsitePaymentMethodSessionRepo as PaymentMethodSessionRepo;
use Amigoo\Database\Repos\WebsiteTldRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebsitesAjaxController extends Controller
{
    public function addAvailableTld(WebsiteTldRepo $siteTldRepo, Request $req, Website $website)
    {
        try {
            $tld = trim($req->get('tld'));

            if ($siteTldRepo->exists($tld)) {
                return response([
                    'ok'     => false,
                    'reason' => 'This TLD exists',
                ], 400);
            }

            $tldRecord = $siteTldRepo->create($website, $tld);

            return response([
                'ok' => true,
                'data' => [
                    'tld'       => $tldRecord->tld,
                    'deleteUrl' => route('admin.websites.ajax.available-tlds.delete', compact('website', 'tldRecord')),
                ]
            ]);

        } catch (\Exception $e) {

            Log::error("Could not insert new Website TLD record", compact('e'));
            return response([
                'ok'     => false,
                'reason' => 'Internal server error',
            ]);
        }
    }

    public function removeAvailableTld(WebsiteTldRepo $siteTldRepo, Website $website, WebsiteTld $tldRecord)
    {
        try {
            $siteTldRepo->delete($tldRecord);

            return response(['ok' => true]);

        } catch (\Exception $e) {

            Log::error("Could not delete Website TLD record #{$tld->id} {$tld->tld}", compact('e'));
            return response([
                'ok'     => false,
                'reason' => 'Internal server error',
            ], 500);
        }
    }

    public function getPaymentMethodSessions(Website $website, Request $req, PaymentMethodSessionRepo $paymentMethodSessionsRepo)
    {
        $queryOptions = (new QueryOptions)
            ->setPagingStart($req->get('start', 0))
            ->setPagingLength($req->get('length', QueryOptions::DEFAULT_QUERY_LIMIT))
        ;

        $sessionsQueryResult = $paymentMethodSessionsRepo->getSessionsForSite($website, $queryOptions);

        $responsePayload = [
            'draw'            => $req->get('draw', 1),
            'recordsTotal'    => $sessionsQueryResult->totalCount,
            'recordsFiltered' => $sessionsQueryResult->filteredCount,
            'data'            => $sessionsQueryResult->data,
        ];

        foreach ($responsePayload['data'] as $session) {
            if ($session->payment_method == 'stripe') {
                $stripeAcc = $session->paymentAccount;

                $stripeAcc['_links'] = [
                    'details' => route('admin.stripe-accs.get.form.update', $stripeAcc),
                ];
            }
        }

        return $responsePayload;
    }
}
