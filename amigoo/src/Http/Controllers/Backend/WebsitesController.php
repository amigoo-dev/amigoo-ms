<?php

namespace Amigoo\Http\Controllers\Backend;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\Constants\OrderStatus;
use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Amigoo\Database\Models\Constants\WebsiteType;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\StripePayoutsRepo;
use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Database\Repos\WebsitePaymentMethodSessionRepo;
use Amigoo\Http\Requests\Website\LinkToStripeAccRequest;
use Amigoo\Http\Requests\Website\UnlinkFromStripeAccRequest;
use Amigoo\Http\Requests\Website\ConfigLinkablesRequest;
use Amigoo\Http\Requests\Website\CreationRequest as WebsiteCreationRequest;
use Amigoo\Http\Requests\Website\UpdationRequest as WebsiteUpdationRequest;
use Amigoo\Http\Requests\Website\UpdateAutoSwitchPeriodRequest;
use Amigoo\Http\Requests\Website\UpdateChangeStripeStatusFailedAmountRequest;
use Amigoo\Services\WooCommerce\Client as WooApiClient;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WebsitesController extends Controller
{
    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var WebsitePaymentMethodSessionRepo */
    private $paymentMethodSessionRepo;

    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /** @var MerchizeOrdersRepo */
    private $merchizeOrdersRepo;

    /** @var StripePayoutsRepo */
    private $payoutsRepo;

    public function __construct(
            WebsitesRepo $websitesRepo,
            StripeAccountsRepo $stripeAccsRepo,
            WebsitePaymentMethodSessionRepo $paymentMethodSessionRepo,
            WooOrdersRepo $wooOrdersRepo,
            MerchizeOrdersRepo $merchizeOrdersRepo,
            StripePayoutsRepo $payoutsRepo
            ) {
        $this->websitesRepo             = $websitesRepo;
        $this->stripeAccsRepo           = $stripeAccsRepo;
        $this->paymentMethodSessionRepo = $paymentMethodSessionRepo;
        $this->wooOrdersRepo            = $wooOrdersRepo;
        $this->merchizeOrdersRepo       = $merchizeOrdersRepo;
        $this->payoutsRepo              = $payoutsRepo;
    }

    public function getIndex()
    {
        $signedInUser = Auth::user();

        $websites =
            $signedInUser->isAdmin()
                ? $this->websitesRepo->all($includeInactive = true)
                : $this->websitesRepo->getSitesModeratableByUser($signedInUser);

        $activeSites    = [];
        $nonActiveSites = [];
        foreach ($websites as $site) {
            $site->latestOrder = $this->getLatestOrderForSite($site);

            switch ($site->type) {
                case WebsiteType::WOO:
                    $site->summary = $this->buildSummaryForWooSite($site);
                    break;

                case WebsiteType::MERCHIZE:
                    $site->summary = $this->buildSummaryForMerchizeSite($site);
                    break;
            }

            $site->todayReceivedAmount         = $this->getTodayReceivedAmountForSite($site);
            $site->totalDailyMaxReceivedAmount = $this->sumTotalDailyMaxReceivedAmount($site);
            $site->totalReservedFund           = $this->sumTotalReservedFundAmount($site);

            $isActive = ($site->stripeAccount != null);
            if ($isActive)
                $activeSites[] = $site;
            else
                $nonActiveSites[] = $site;
        }

        return view('amigoo::backend.websites.index', compact('activeSites', 'nonActiveSites'));
    }

    public function getCreationForm()
    {
        return view('amigoo::backend.websites.create', [
            'website' => new Website,
        ]);
    }

    public function postCreationForm(WebsiteCreationRequest $req)
    {
        $input    = $req->all();
        $siteType = $input['type'];

        $website = $this->websitesRepo->create($siteType, $input);
        $input['url'] = rtrim($input['url'], '/');

        return redirect()->route('admin.websites.get.form.update', compact('website'));
    }

    public function getUpdationForm(Website $website)
    {
        $linkableStripeAccs = $this->stripeAccsRepo->getLinkableAccountsForSwitching($website);

        if ($linkableStripeAccs->isEmpty()) {
            $defaultAcc = $this->stripeAccsRepo->getDefaultAccount($website, $throwIfNotFound = false);
            if ($defaultAcc)
                $linkableStripeAccs->push($defaultAcc);
        }

        $linkingStripeAccId = $website->stripe_acc_id;
        if ($linkingStripeAccId) {
            $containsLinkingStripeAcc = $linkableStripeAccs->contains(fn($linkableAcc) => $linkableAcc->id == $linkingStripeAccId);
            if (! $containsLinkingStripeAcc) {
                $curLinkedStripeAcc = $this->stripeAccsRepo->getById($linkingStripeAccId);
                $linkableStripeAccs->prepend($curLinkedStripeAcc);
            }
        }

        switch ($website->type) {
            case WebsiteType::WOO:
                $summary = $this->buildSummaryForWooSite($website);
                break;
            case WebsiteType::MERCHIZE:
                $summary = $this->buildSummaryForMerchizeSite($website);
                break;
        }

        return view('amigoo::backend.websites.update', compact(
            'website',
            'linkableStripeAccs',
            'summary',
        ));
    }

    public function postUpdationForm(WebsiteUpdationRequest $req, Website $website)
    {
        $input = $req->all();
        $input['url'] = rtrim($input['url'], '/');

        $this->websitesRepo->update($website, $input);

        return redirect()->route('admin.websites.get.form.update', compact('website'));
    }

    public function getConfigLinkablesForm(Website $website)
    {
        $stripeAccs            = $this->stripeAccsRepo->getAccountsForLinkablesConfiguration($website);
        $linkableStripeAccs    = $website->linkableStripeAccounts;
        $linkableStripeAccsIds = $linkableStripeAccs->map(fn($acc) => $acc->id);
        $defaultStripeAcc      = $linkableStripeAccs->first(fn($acc) => $acc->linkable->is_default);

        $accWooTodayReceived      = $this->wooOrdersRepo->getTodayReceivedAmountForAllStripeAccs();
        $accMerchizeTodayReceived = $this->merchizeOrdersRepo->getTodayReceivedAmountForAllStripeAccs();
        $accWooTotalReceived      = $this->wooOrdersRepo->getTotalReceivedAmountForAllStripeAccs();
        $accMerchizeTotalReceived = $this->merchizeOrdersRepo->getTotalReceivedAmountForAllStripeAccs();

        $endOfToday    = Carbon::tomorrow();
        $twoDaysBefore = Carbon::today()->sub('2 days');
        foreach ($stripeAccs as $acc) {

            $acc->today_received = 0;
            if (array_key_exists($acc->id, $accWooTodayReceived))
                $acc->today_received += $accWooTodayReceived[$acc->id];
            if (array_key_exists($acc->id, $accMerchizeTodayReceived))
                $acc->today_received += $accMerchizeTodayReceived[$acc->id];

            $acc->total_received = 0;
            if (array_key_exists($acc->id, $accWooTotalReceived))
                $acc->total_received += $accWooTotalReceived[$acc->id];
            if (array_key_exists($acc->id, $accMerchizeTotalReceived))
                $acc->total_received += $accMerchizeTotalReceived[$acc->id];

            $latestWooOrder      = $this->wooOrdersRepo->getLatestOrderForStripeAcc($acc->id);
            $latestMerchizeOrder = $this->merchizeOrdersRepo->getLatestOrderForStripeAcc($acc->id);
            $acc->latest_order =
                $latestWooOrder > $latestMerchizeOrder
                    ? $latestWooOrder
                    : $latestMerchizeOrder;

            $acc->po_amount_last2days = $this->payoutsRepo->getTotalAmountInPeriod($acc, $twoDaysBefore, $endOfToday);
        }

        return view('amigoo::backend.websites.config-linkables', compact(
            'website',
            'stripeAccs',
            'linkableStripeAccs',
            'linkableStripeAccsIds',
            'defaultStripeAcc',
        ));
    }

    public function postConfigLinkablesForm(ConfigLinkablesRequest $req, Website $website)
    {
        $linkableStripeAccsIds = $req->input('linkable_stripe_accs_ids', []);
        $newLinkedStripeAccId  = $req->input('current_stripe_acc_id');
        $newDefaultStripeAccId = $req->input('default_stripe_acc_id');

        $this->websitesRepo->setLinkableStripeAccs($website, $linkableStripeAccsIds, $newDefaultStripeAccId);

        if ($newLinkedStripeAccId) {
            if ($newLinkedStripeAccId != $website->stripe_acc_id) {
                try {
                    $this->_linkToStripeAcc($website, $newLinkedStripeAccId);
                } catch (\Exception $e) {
                    return redirect()->route('admin.websites.get.form.config-linkables', compact('website'))->withFlashDanger($e->getMessage());
                }
            }
        } else {
            $this->_unlinkFromStripeAcc($website);
        }

        return redirect()->route('admin.websites.get.form.config-linkables', $website);
    }

    public function activate(Website $website)
    {
        $this->websitesRepo->activate($website);

        // TODO Currently need to link Stripe account by hand after re-activation.
        return redirect()->route('admin.websites.get.form.update', compact('website'));
    }

    public function deactivate(Website $website)
    {
        DB::transaction(
            function() use ($website) {
                $this->websitesRepo->deactivate($website);
                $this->paymentMethodSessionRepo->endCurrentSessionForSite($website);
            }
        );

        return redirect()->route('admin.websites.get.index');
    }

    public function linkToStripeAcc(LinkToStripeAccRequest $request, Website $website)
    {
        $stripeAccId = $request->get('stripe_acc_id');

        try {
            $this->_linkToStripeAcc($website, $stripeAccId);
        } catch (\Exception $e) {
            return redirect()->route('admin.websites.get.form.update', compact('website'))->withFlashDanger($e->getMessage());
        }

        return redirect()->route('admin.websites.get.form.update', compact('website'));
    }

    public function unlinkFromStripeAcc(UnlinkFromStripeAccRequest $req, Website $website)
    {
        $this->_unlinkFromStripeAcc($website);

        return redirect()->route('admin.websites.get.form.update', compact('website'));
    }

    public function updateAutoSwitchPeriod(Website $website, UpdateAutoSwitchPeriodRequest $req)
    {
        $autoSwitchAfter = $req->get('auto_switch_after_mins');

        $this->websitesRepo->update($website, [
            'auto_switch_after_mins' => $autoSwitchAfter,
        ]);

        return redirect()->route('admin.websites.get.form.update', $website);
    }

    public function updateChangeStripeStatusFailedAmount(Website $website, UpdateChangeStripeStatusFailedAmountRequest $req)
    {
        $changeStripeStatusFailedAmount = $req->get('change_stripe_status_failed_amount');

        $this->websitesRepo->update($website, [
            'change_stripe_status_failed_amount' => $changeStripeStatusFailedAmount,
        ]);

        return redirect()->route('admin.websites.get.form.update', $website);
    }



    private function _linkToStripeAcc(Website $website, int $stripeAccId)
    {
        // Exception for 404 response would be thrown if not found
        $stripeAcc   = $this->stripeAccsRepo->getById($stripeAccId);

        DB::transaction(
            function () use ($website, $stripeAcc) {
                $this->websitesRepo->unlinkFromStripeAccount($website);
                $this->paymentMethodSessionRepo->endCurrentSessionForSite($website);

                $this->websitesRepo->linkToStripeAccount($website, $stripeAcc);
                $this->paymentMethodSessionRepo->createSessionWithStripe($website, $stripeAcc);
            }
        );

        if ($website->needApiCall()) {
            try {
                WooApiClient::fromWebsite($website)
                    ->updatePaymentGatewayStripe(
                        $stripeAcc->public_key,
                        $stripeAcc->secret_key
                    );

            } catch (\Exception $e) {
                Log::error('Could not update website\'s Stripe account');
                Log::error($e);

                throw new \Exception('Could not update website\'s Stripe account through API. Please update by hand on the website.');
            }
        }

    }

    private function _unlinkFromStripeAcc(Website $website)
    {
        DB::transaction(
            function() use ($website) {
                $this->websitesRepo->unlinkFromStripeAccount($website);
                $this->paymentMethodSessionRepo->endCurrentSessionForSite($website);
            }
        );
    }

    private function getLatestOrderForSite(Website $site)
    {
        switch ($site->type) {
            case WebsiteType::WOO:
                return $this->wooOrdersRepo->getLatestOrderForSite($site);
            case WebsiteType::MERCHIZE:
                return $this->merchizeOrdersRepo->getLatestOrderForSite($site);
        }
    }

    private function buildSummaryForWooSite(Website $site)
    {
        return [
            OrderStatus::PROCESSING => [
                'count' => $this->wooOrdersRepo->countOrdersBySite($site, OrderStatus::PROCESSING),
                'total' => $this->wooOrdersRepo->sumTotalForSite($site, OrderStatus::PROCESSING),
            ],
            OrderStatus::COMPLETED => [
                'count' => $this->wooOrdersRepo->countOrdersBySite($site, OrderStatus::COMPLETED),
                'total' => $this->wooOrdersRepo->sumTotalForSite($site, OrderStatus::COMPLETED),
            ],
            OrderStatus::FAILED => [
                'count' => $this->wooOrdersRepo->countOrdersBySite($site, OrderStatus::FAILED),
                'total' => $this->wooOrdersRepo->sumTotalForSite($site, OrderStatus::FAILED),
            ],
        ];
    }

    private function buildSummaryForMerchizeSite(Website $site)
    {
        return [
            'count' => $this->merchizeOrdersRepo->countOrdersBySite($site),
            'total' => $this->merchizeOrdersRepo->sumTotalForSite($site),
        ];
    }

    private function getTodayReceivedAmountForSite(Website $site)
    {
        switch ($site->type) {
            case WebsiteType::WOO:
                return $this->wooOrdersRepo->getTodayReceivedAmountForSite($site->id);
            case WebsiteType::MERCHIZE:
                return $this->merchizeOrdersRepo->getTodayReceivedAmountForSite($site->id);
        }
    }

    private function sumTotalDailyMaxReceivedAmount(Website $site): float
    {
        return $site->linkableStripeAccounts
            ->filter(fn($acc) =>
                in_array($acc->status, [
                    StripeAccountStatus::READY,
                    StripeAccountStatus::REACHED_DAILY_MAX,
                ])
            )
            ->sum('daily_max_received_amount');
    }

    private function sumTotalReservedFundAmount(Website $site): ?array
    {
        $reservedFunds = $site->linkableStripeAccounts
            ->filter(fn($acc) =>
                in_array($acc->status, [
                    StripeAccountStatus::HOLD_90,
                    StripeAccountStatus::HOLD_120,
                ])
            )
            ->filter(fn($acc) => $acc->reservedFund)
            ->map(fn($acc) => $acc->reservedFund);

        if ($reservedFunds->isEmpty())
            return null;

        $availableCurrencyCodes = $reservedFunds->pluck('currency_code')->unique();

        $map = [];
        foreach ($availableCurrencyCodes as $currencyCode) {
            $map[$currencyCode] =
                $reservedFunds
                    ->filter(fn($fund) => $fund->currency_code == $currencyCode)
                    ->sum('amount');
        }

        return $map;
    }
}
