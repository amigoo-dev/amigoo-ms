<?php

namespace Amigoo\Http\Controllers\Webhook\Mailgun;

use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Services\Notification\StripeAccIssueNotifier;
use Amigoo\Services\WooCommerce\Stripe\AccountSwitcher as StripeAccSwitcher;
use Amigoo\Services\Stripe\StripeAccStatusEmailMapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InboundWebhookControllerForStripeAccStatus extends \App\Http\Controllers\Controller
{
    public function __invoke(
            StripeAccountsRepo $stripeAccsRepo,
            StripeAccSwitcher $switcher,
            StripeAccIssueNotifier $notifier,
            Request $req
            ) {
        Log::debug('Received an email\n'.print_r($req->all(), true));

        try {
            $stripeAccEmail  = $this->extractStripeAccEmailFromRequest($req);
            $mailContentHtml = $req->get('stripped-html');

            $newStatus = StripeAccStatusEmailMapper::getMatchingStatusForContentHtml($mailContentHtml);
            if ($newStatus) {
                Log::info("Matched new status for {$stripeAccEmail}: {$newStatus}");
                $stripeAcc = $stripeAccsRepo->getAccountByEmail($stripeAccEmail);
                $switcher->switch($stripeAcc, $newStatus);

                $notifier->notify($stripeAcc);
            }

        } catch (\Exception $e) {
            // TODO NOTIFY EXCEPTION (SHOULD BE DONE BY USING JOBS).
            Log::error($e);
        }

        return response(null, 200, ['Content-Type' => 'application/json']);
    }

    private function extractStripeAccEmailFromRequest(Request $req)
    {
        $emailTo = $req->get('To');

        $emailExtractionRegex = '/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/';

        if (preg_match_all($emailExtractionRegex, $emailTo, $matches)) {
            $matches = $matches[0];
            return end($matches);
        }

        throw new \Exception('Failed to extract Stripe-acc email from request');
    }
}