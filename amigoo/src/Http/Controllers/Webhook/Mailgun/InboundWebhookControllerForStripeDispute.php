<?php

namespace Amigoo\Http\Controllers\Webhook\Mailgun;

use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\StripeDisputeRepo;
use Amigoo\Services\Notification\StripeDisputeNotifier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InboundWebhookControllerForStripeDispute extends \App\Http\Controllers\Controller
{
    public function __invoke(
            StripeAccountsRepo $stripeAccsRepo,
            StripeDisputeRepo $disputeRepo,
            StripeDisputeNotifier $notifier,
            Request $req
        ){
        Log::debug('Received an email for Stripe-dispute\n'.print_r($req->all(), true));

        try {
            $stripeAccEmail = $this->extractStripeAccEmailFromRequest($req);
            $mailSubject    = $req->get('subject');

            $data             = $this->examineMailSubject($mailSubject);
            $isMailForDispute = !!$data;
            if ($isMailForDispute) {
                $stripeAcc       = $stripeAccsRepo->getAccountByEmail($stripeAccEmail);
                $curLinkableSite = $stripeAcc->linkableWebsites->first();

                $dispute = $disputeRepo->insert(
                    $stripeAcc->id,
                    $data['stripePaymentId'],
                    $data['amount'],
                    $data['currencyCode'],
                    $data['reason'],
                    $curLinkableSite ? $curLinkableSite->id : null
                );

                $notifier->notify($dispute);
            }

        } catch (\Exception $e) {
            // TODO NOTIFY EXCEPTION (SHOULD BE DONE BY USING JOBS).
            Log::error($e);
        }

        return response(null, 200, ['Content-Type' => 'application/json']);
    }

    private function extractStripeAccEmailFromRequest(Request $req)
    {
        $emailTo = $req->get('To');

        $emailExtractionRegex = '/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/';

        if (preg_match_all($emailExtractionRegex, $emailTo, $matches)) {
            $matches = $matches[0];
            return end($matches);
        }

        throw new \Exception('Failed to extract Stripe-acc email from request');
    }

    private function examineMailSubject(string $mailSubject): ?array
    {
        $regex = '/.(\d*\.?\d*) ([A-Z]{3}) payment disputed as (.*) \((.*)\)/';

        $match = preg_match($regex, $mailSubject, $matches);

        if ( ! $match)
            return null;

        return [
            'amount'          => $matches[1],
            'currencyCode'    => $matches[2],
            'reason'          => $matches[3],
            'stripePaymentId' => $matches[4],
        ];
    }
}