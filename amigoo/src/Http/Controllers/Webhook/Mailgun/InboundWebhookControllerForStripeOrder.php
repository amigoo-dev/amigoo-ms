<?php

namespace Amigoo\Http\Controllers\Webhook\Mailgun;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Amigoo\Database\Models\Constants\WebsiteType;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\WebsitePaymentMethodSessionRepo;
use Amigoo\Services\WooCommerce\Stripe\AccountSwitcher as StripeAccSwitcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InboundWebhookControllerForStripeOrder extends \App\Http\Controllers\Controller
{
    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var WebsitePaymentMethodSessionRepo */
    private $paymentMethodSessionRepo;

    /** @var MerchizeOrdersRepo */
    private $merchizeOrdersRepo;

    /** @var StripeAccSwitcher */
    private $stripeAccSwitcher;

    public function __construct(
            StripeAccountsRepo $stripeAccsRepo,
            WebsitePaymentMethodSessionRepo $paymentMethodSessionRepo,
            MerchizeOrdersRepo $merchizeOrdersRepo,
            StripeAccSwitcher $stripeAccSwitcher
            ) {
        $this->stripeAccsRepo           = $stripeAccsRepo;
        $this->paymentMethodSessionRepo = $paymentMethodSessionRepo;
        $this->merchizeOrdersRepo       = $merchizeOrdersRepo;
        $this->stripeAccSwitcher        = $stripeAccSwitcher;
    }

    public function __invoke(Request $req) {
        Log::debug('Received an email for Stripe-order\n'.print_r($req->all(), true));

        try {
            $stripeAccEmail  = $this->extractStripeAccEmailFromRequest($req);
            $mailContentHtml = $req->get('stripped-html');
            $mailSubject     = $req->get('subject');

            $dataFromEmail = $this->examineEmail($mailSubject, $mailContentHtml);
            Log::debug([
                'extractedData' => compact(
                    'stripeAccEmail',
                    'mailSubject',
                    'dataFromEmail',
                    'mailContentHtml',
                ),
            ]);
            if ($dataFromEmail) {
                $stripeAcc = $this->stripeAccsRepo->getAccountByEmail($stripeAccEmail);
                if (! $stripeAcc)
                    throw new \Exception("Stripe-acc [{$stripeAccEmail}] not found");

                $latestSession = $this->paymentMethodSessionRepo->getLatestSessionForStripeAcc($stripeAcc);
                if (! $latestSession)
                    throw new \Exception("Latest session of Stripe-acc [{$stripeAccEmail}] not found");

                $latestLinkedSite = $latestSession->website;
                if ($latestLinkedSite->isWooSite() == false) {
                    $this->merchizeOrdersRepo->insert(
                        $latestLinkedSite->id,
                        $stripeAcc->id,
                        $dataFromEmail['order_code'],
                        $dataFromEmail['customer_email'],
                        $dataFromEmail['total'],
                        $dataFromEmail['currency_code']
                    );

                    $newStatusByReachedDailyMax = $this->selectNewStatusIfReachedDailyMax($stripeAcc);
                    if ($newStatusByReachedDailyMax) {
                        $this->stripeAccSwitcher->switch($stripeAcc, $newStatusByReachedDailyMax);
                    }
                }
            }

        } catch (\Exception $e) {
            // TODO NOTIFY EXCEPTION (SHOULD BE DONE BY USING JOBS).
            Log::error($e);
        }

        return response(null, 200, ['Content-Type' => 'application/json']);
    }

    private function extractStripeAccEmailFromRequest(Request $req)
    {
        $emailTo = $req->get('To');

        $emailExtractionRegex = '/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/';

        if (preg_match_all($emailExtractionRegex, $emailTo, $matches)) {
            $matches = $matches[0];
            return end($matches);
        }

        throw new \Exception('Failed to extract Stripe-acc email from request');
    }

    private function examineEmail(string $subject, string $html)
    {
        $data = [
            // NULLABLE fields for a very-first mail
            'order_code'     => null,
            'customer_email' => null,
        ];

        $cleanContent = self::decodeHtml($html);

        // For mail subject
        $regexForSubject1 = '/Payment of (.)(\d*\.?\d*) from (.+) for (.+)/';
        $regexForSubject2 = '/Payment of .(\d*\.?\d*) ([A-Z]{3}) \(?from ([^\)]+)\)?/';

        if ($matchSubject = preg_match($regexForSubject1, $subject, $matches)) {
            $data['total'] = floatval($matches[2]);

            $currencySign = $matches[1];
            switch ($currencySign) {
                case '$': $data['currency_code'] = 'USD'; break;
                case '€': $data['currency_code'] = 'EUR'; break;
                default:
                    throw new Error("Currency sign ${$currencySign} not supported");
            }
        } elseif ($matchSubject = preg_match($regexForSubject2, $subject, $matches)) {
            $data['total']         = floatval($matches[1]);
            $data['currency_code'] = $matches[2];
        } else {
            return null;
        }

        // For a very-first
        $regexForTheFirstMail = '/You\'ve just received your first payment through Stripe/';

        // For MERCHIZE
        $regexForOrderCode      = '/code — ([A-Z]{2}-\d{5}-\d{5})/';
        $regexForCustomerEmail1 = '/email — ([a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-\.]+\.[a-zA-Z]{2,5})/';
        // For SHOPBASE
        $regexForShopBaseOrderSource = '/order_source — ShopBase/';
        $regexForCustomerEmail2      = '/customer_email — ([a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-\.]+\.[a-zA-Z]{2,5})/';
        // For WOO
        $regexForWooOrderId     = '/order_id — (\d+)/';
        $regexForCustomerEmail3 = '/customer_email — ([a-zA-Z0-9_\-\.]+@[a-zA-Z0-9_\-\.]+\.[a-zA-Z]{2,5})/';

        // Collect nothing else when this is a very-first mail
        $isVeryFirstMail = preg_match($regexForTheFirstMail, $cleanContent);
        if ($isVeryFirstMail)
            return $data;

        // For MERCHIZE
        $forMerchize = preg_match($regexForOrderCode, $cleanContent, $matches);
        if ($forMerchize) {
            $data['order_code'] = $matches[1];

            preg_match($regexForCustomerEmail1, $cleanContent, $matches);
            $data['customer_email'] = $matches[1];

            return $data;
        }

        // For SHOPBASE
        $forShopBase = preg_match($regexForShopBaseOrderSource, $cleanContent);
        if ($forShopBase) {
            preg_match($regexForCustomerEmail2, $cleanContent, $matches);
            $data['customer_email'] = $matches[1];

            return $data;
        }

        // For WOO
        $forWoo = preg_match($regexForWooOrderId, $cleanContent, $matches);
        if ($forWoo) {
            $data['order_code'] = 'WOO-'.$matches[1];

            preg_match($regexForCustomerEmail3, $cleanContent, $matches);
            $data['customer_email'] = $matches[1];

            return $data;
        }

        return null;
    }

    private function decodeHtml(string $html): string
    {
        return
            str_replace("\xc2\xa0", ' ',
                preg_replace(
                    '/\s+/', ' ',
                    html_entity_decode(
                        strip_tags($html),
                        ENT_QUOTES
                    )
                )
            );
    }

    private function selectNewStatusIfReachedDailyMax(StripeAccount $acc)
    {
        $todayReceivedAmount =
            $this->merchizeOrdersRepo->getTodayReceivedAmountForStripeAcc($acc->id);

        $reachedDailyMaxReceivedAmount =
            $todayReceivedAmount > $acc->daily_max_received_amount;

        if ($reachedDailyMaxReceivedAmount)
            return $acc->should_wait_for_payout
                ? StripeAccountStatus::WAITING_PAYOUT
                : StripeAccountStatus::REACHED_DAILY_MAX;

        return null;
    }
}