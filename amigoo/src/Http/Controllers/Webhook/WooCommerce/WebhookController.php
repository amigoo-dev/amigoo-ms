<?php

namespace Amigoo\Http\Controllers\Webhook\WooCommerce;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Repos\WebsiteTldRepo;
use Amigoo\Helpers\TldExtractor;
use Amigoo\Services\WooCommerce\WebhookHandlers\HandlerInterface;
use Amigoo\Services\WooCommerce\WebhookHandlers\HandlerMapper as WebhookHandlerMapper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends \App\Http\Controllers\Controller
{
    /** @var WebsiteTldRepo */
    private $websiteTldRepo;

    /** @var WebhookHandlerMapper */
    private $webhookHandlerMapper;

    public function __construct(
            WebsiteTldRepo $websiteTldRepo,
            WebhookHandlerMapper $webhookHandlerMapper
        ) {
        $this->websiteTldRepo       = $websiteTldRepo;
        $this->webhookHandlerMapper = $webhookHandlerMapper;
    }

    public function __invoke(Request $req)
    {
        // TODO AUTH

        if ($this->isWooSiteRequestForWebhookVerification($req)) {
            $reqOrig = parse_url($req->headers->get('origin'), PHP_URL_HOST);
            Log::info("Received a request for webhook-verification from Woo site {$reqOrig}");

            return response('OK', 200);
        }

        Log::debug('Received a Woo event '.print_r([
            'headers' => $this->getWooCustomHeaders($req),
            'payload' => $req->all(),
        ], true));

        try {
            $siteTld = $this->getSiteTldFromRequest($req);
            $site    = $this->websiteTldRepo->getByTld($siteTld)->website;
        } catch (ModelNotFoundException $e) {
            Log::error("Source site not found: {$siteTld}", compact('e'));
            return response("Source site not found: {$siteTld}", 200);
        }

        try {
            $topic               = $this->getEventTopicFromRequest($req);
            $webhookHandler      = $this->webhookHandlerMapper->getHandler($topic);
            $handlerNotAvailable = ! $webhookHandler;
            if ($handlerNotAvailable)
                return response("Topic '{$topic}' not supported", 200);

            $webhookHandler->handle($site, $req->all());

        } catch (\Exception $e) {
            Log::error($e);
            return response("Internal server error", 200);
        }
    }

    private function isWooSiteRequestForWebhookVerification(Request $req): bool
    {
        $isFormUrlEncodedRequest = ($req->header('Content-Type') == 'application/x-www-form-urlencoded');
        $hasWebhookIdParam       = $req->has('webhook_id');

        return ($isFormUrlEncodedRequest && $hasWebhookIdParam);
    }

    private function getWooCustomHeaders(Request $req)
    {
        return [
            'x-wc-webhook-delivery-id' => $req->header('x-wc-webhook-delivery-id'),
            'x-wc-webhook-event' => $req->header('x-wc-webhook-event'),
            'x-wc-webhook-id' => $req->header('x-wc-webhook-id'),
            'x-wc-webhook-resource' => $req->header('x-wc-webhook-resource'),
            'x-wc-webhook-signature' => $req->header('x-wc-webhook-signature'),
            'x-wc-webhook-source' => $req->header('x-wc-webhook-source'),
            'x-wc-webhook-topic' => $req->header('x-wc-webhook-topic'),
        ];
    }

    private function getSiteTldFromRequest(Request $req): string
    {
        return TldExtractor::extract($req->headers->get('x-wc-webhook-source'));
    }

    private function getEventTopicFromRequest(Request $req): string
    {
        return $req->headers->get('x-wc-webhook-topic');
    }
}
