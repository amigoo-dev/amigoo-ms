<?php

namespace Amigoo\Http\Requests\Auth;

class ConfigModeratableWebsitesRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'moderatable_websites_ids' => [
                'array',
            ],
            'moderatable_websites_ids.*' => [
                'required',
                'integer',
                'exists:amigoo_websites,id',
            ],
        ];
    }
}
