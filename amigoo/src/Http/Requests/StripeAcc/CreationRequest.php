<?php

namespace Amigoo\Http\Requests\StripeAcc;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Illuminate\Validation\Rule;

class CreationRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accId = $this->route('acc', new StripeAccount)->id;

        $rules = [
            'status' => [
                Rule::in(StripeAccountStatus::getAvailableStatusesValues()),
            ],
            'owner_name' => [
                'required',
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('amigoo_stripe_accounts')->ignore($accId),
            ],
            'country_code' => [
                'required',
            ],
            'public_key' => [
                'required',
            ],
            'secret_key' => [
                'required',
            ],
            'daily_max_received_amount' => [
                'required',
                'numeric',
                'gt:0',
            ],
            'should_wait_for_payout' => [
                'boolean',
            ],

            'linkable_site_id' => [
                'nullable',
                'int',
                'exists:amigoo_websites,id',
            ],

            'proxy_info.insert' => [
                'sometimes',
                'boolean',
            ],
            'proxy_info.ip' => [
                'nullable',
                'required_if:proxy_info.insert,1',
            ],
            'proxy_info.port' => [
                'nullable',
                'integer',
                'min:0',
                'max:65535',
            ],
            'proxy_info.user' => [],
            'proxy_info.password' => [
                'required_with:proxy_info.user',
            ],
            'proxy_info.type' => [
                'nullable'
            ],
            'proxy_info.provider' => [
                'nullable'
            ],
        ];

        return $rules;
    }
}
