<?php

namespace Amigoo\Http\Requests\StripeAcc;

class SaveProxyConfigRequest extends \Illuminate\Foundation\Http\FormRequest
{
    public function rules()
    {
        return [
            'ip' => [
                'required',
            ],
            'port' => [
                'nullable',
                'integer',
                'min:0',
                'max:65535',
            ],
            'user' => [],
            'password' => [
                'required_with:user',
            ],
        ];
    }
}
