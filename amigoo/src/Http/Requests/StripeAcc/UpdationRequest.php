<?php

namespace Amigoo\Http\Requests\StripeAcc;

use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Illuminate\Validation\Rule;

class UpdationRequest extends CreationRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $creationRules = parent::rules();

        $rules = $this->removeRulesForProxyInfoInputs($creationRules);
        $rules = $this->prefixSometimesToEveryRule($rules);

        return $rules;
    }

    /**
     * Insert `sometimes` rule at the first position of every rule.
     */
    private function prefixSometimesToEveryRule(array $rules): array
    {
        return array_map(
            fn(array $rule) => ['sometimes', ...$rule],
            $rules
        );
    }

    private function removeRulesForProxyInfoInputs(array $rules): array
    {
        return array_filter(
            $rules,
            fn(string $key) => ! preg_match('/^proxy_info/', $key),
            ARRAY_FILTER_USE_KEY
        );
    }
}
