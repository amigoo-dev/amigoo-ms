<?php

namespace Amigoo\Http\Requests\Website;

use Illuminate\Validation\Rule;

class ConfigLinkablesRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'linkable_stripe_accs_ids' => [
                'array',
            ],
            'linkable_stripe_accs_ids.*' => [
                'required',
                'integer',
                'exists:amigoo_stripe_accounts,id',
            ],
            'current_stripe_acc_id' => [
                'integer',
                'in_array:linkable_stripe_accs_ids.*',
            ],
            'default_stripe_acc_id' => [
                Rule::requiredIf(fn() => $this->has('linkable_stripe_accs_ids')),
                'integer',
                'in_array:linkable_stripe_accs_ids.*',
            ],
        ];
    }
}
