<?php

namespace Amigoo\Http\Requests\Website;

use Amigoo\Database\Models\Constants\NotificationType;
use Illuminate\Validation\Rule;

class ConfigSiteModsNotificationsRequest extends \Illuminate\Foundation\Http\FormRequest
{
    public function rules()
    {
        return [
            'user_id_to_notifications_map' => [
                'array',
            ],
            'user_id_to_notifications_map.*' => [
                'array',
            ],
            'user_id_to_notifications_map.*.*' => [
                Rule::in(NotificationType::getAvailableValues()),
            ],
        ];
    }
}
