<?php

namespace Amigoo\Http\Requests\Website;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Models\Constants\WebsiteType;
use Illuminate\Validation\Rule;

class CreationRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $websiteId = $this->route('website', new Website)->id;

        return [
            'type' => [
                'required',
                Rule::in(WebsiteType::getAvailableStatusesValues()),
            ],
            'name' => [
                'required',
                Rule::unique('amigoo_websites')->ignore($websiteId),
            ],
            'url' => [
                'required',
                'url',
            ],
            'api_consumer_key' => [
                Rule::requiredIf(function () {
                    $needle   = $this->request->get('type');
                    $haystack = [WebsiteType::WOO];

                    return in_array($needle, $haystack);
                }),
            ],
            'api_consumer_secret' => [
                Rule::requiredIf(function () {
                    $needle   = $this->request->get('type');
                    $haystack = [WebsiteType::WOO];

                    return in_array($needle, $haystack);
                }),
            ],
            'webhook_secret' => [
                'required',
            ],
        ];
    }
}
