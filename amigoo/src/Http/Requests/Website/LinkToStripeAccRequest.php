<?php

namespace Amigoo\Http\Requests\Website;

class LinkToStripeAccRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stripe_acc_id' => [
                'required',
                'exists:amigoo_stripe_accounts,id',
            ]
        ];
    }
}
