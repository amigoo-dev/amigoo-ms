<?php

namespace Amigoo\Http\Requests\Website;

use Amigoo\Database\Models\Website;
use Illuminate\Validation\Rule;

class UpdateAutoSwitchPeriodRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'auto_switch_after_mins' => [
                'required',
                'integer',
                'gt:0',
            ]
        ];
    }
}
