<?php

namespace Amigoo\Http\Requests\Website;

use Amigoo\Database\Models\Website;
use Illuminate\Validation\Rule;

class UpdateChangeStripeStatusFailedAmountRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'change_stripe_status_failed_amount' => [
                'required',
                'integer',
                'gte:1',
            ]
        ];
    }
}
