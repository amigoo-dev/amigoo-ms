<?php

namespace Amigoo\Http\Requests\Website;

use Amigoo\Database\Models\Constants\WebsiteType;
use Illuminate\Validation\Rule;

class UpdationRequest extends CreationRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $creationRules = parent::rules();
        $updationRules = $this->prefixSometimesToEveryRule($creationRules);

        return $updationRules;
    }

    /**
     * Insert `sometimes` rule at the first position of every rule.
     */
    private function prefixSometimesToEveryRule(array $creationRules): array
    {
        $rules = array_map(
            fn(array $rule) => ['sometimes', ...$rule],
            $creationRules
        );

        $rules['api_consumer_key'] = [
            Rule::requiredIf(function () {
                $site     = $this->route('website');
                $needle   = $site->type;
                $haystack = [WebsiteType::WOO];

                return in_array($needle, $haystack);
            }),
        ];
        $rules['api_consumer_secret'] = [
            Rule::requiredIf(function () {
                $site     = $this->route('website');
                $needle   = $site->type;
                $haystack = [WebsiteType::WOO];

                return in_array($needle, $haystack);
            }),
        ];

        return $rules;
    }
}
