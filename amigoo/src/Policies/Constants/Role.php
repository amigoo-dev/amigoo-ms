<?php

namespace Amigoo\Policies\Constants;

abstract class Role
{
    const WEBSITE_MODERATOR    = 'website-moderator';
    const STRIPE_ACC_MODERATOR = 'stripe-account-moderator';
}
