<?php

namespace Amigoo\Policies;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Policies\Constants\Role;
use App\Models\Auth\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StripeAccountPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ($user->hasRole(Role::STRIPE_ACC_MODERATOR))
            return null;

        if ($user->hasRole(Role::WEBSITE_MODERATOR))
            return null;

        return false;
    }

    /**
     * Determine whether the user can view any amigoo database models websites.
     *
     * @param  User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if ($this->isStripeAccModerator($user))
            return true;

        return null;
    }

    /**
     * Determine whether the user can view the amigoo database models website.
     *
     * @param  User           $user
     * @param  StripeAccount  $stripeAcc
     * @return mixed
     */
    public function view(User $user, StripeAccount $stripeAcc)
    {
        if ($this->isStripeAccModerator($user))
            return true;

        return $this->isModeratorOfSiteLinkableToStripeAcc($user, $stripeAcc);
    }

    public function viewIndex(User $user)
    {
        return true; // ADMIN or Website-moderator or Stripe-account-moderator
    }

    /**
     * Determine whether the user can create amigoo database models websites.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->isStripeAccModerator($user);
    }

    /**
     * Determine whether the user can update the amigoo database models website.
     *
     * @param  User           $user
     * @param  StripeAccount  $stripeAcc
     * @return mixed
     */
    public function update(User $user)
    {
        return $this->isStripeAccModerator($user);
    }

    public function viewProxyConfig(User $user)
    {
        return $this->isStripeAccModerator($user);
    }

    public function saveProxyConfig(User $user)
    {
        return $this->isStripeAccModerator($user);
    }

    public function viewStripePayouts(User $user, ?StripeAccount $stripeAcc = null)
    {
        if ($stripeAcc)
            return $this->isModeratorOfSiteLinkableToStripeAcc($user, $stripeAcc);

        return $user->hasRole(Role::WEBSITE_MODERATOR);
    }

    public function viewOrderPaymentsSummaryAndList(User $user)
    {
        return $user->hasRole(Role::WEBSITE_MODERATOR);
    }


    private function isStripeAccModerator(User $user): bool
    {
        return $user->hasRole(Role::STRIPE_ACC_MODERATOR);
    }

    private function isModeratorOfSiteLinkableToStripeAcc(User $user, StripeAccount $stripeAcc)
    {
        $linkableSites = $stripeAcc->linkableWebsites;

        foreach ($linkableSites as $site)
            if ($user->can('update', $site))
                return true;

        return false;
    }
}
