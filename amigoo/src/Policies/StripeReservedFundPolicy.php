<?php

namespace Amigoo\Policies;

use Amigoo\Policies\Constants\Role;
use App\Models\Auth\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StripeReservedFundPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        // IF NOT ADMIN, AT LEAST MUST BE Website-moderator
        if (! $user->hasRole(Role::WEBSITE_MODERATOR))
            return false;
    }

    public function viewIndex(User $user)
    {
        return true; // ADMIN or Website-moderator
    }
}
