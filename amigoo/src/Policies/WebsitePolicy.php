<?php

namespace Amigoo\Policies;

use Amigoo\Database\Models\Website;
use Amigoo\Policies\Constants\Role;
use App\Models\Auth\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class WebsitePolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        // IF NOT ADMIN, AT LEAST MUST BE Website-moderator
        if (! $user->hasRole(Role::WEBSITE_MODERATOR))
            return false;
    }

    /**
     * Determine whether the user can view any amigoo database models websites.
     *
     * @param  User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return null;
    }

    /**
     * Determine whether the user can view the amigoo database models website.
     *
     * @param  User     $user
     * @param  Website  $site
     * @return mixed
     */
    public function view(User $user, Website $site)
    {
        return $this->isModeratorOfSite($user, $site);
    }

    public function viewIndex(User $user)
    {
        return true; // ADMIN or Website-moderator
    }

    /**
     * Determine whether the user can create amigoo database models websites.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create()
    {
        return false; // ADMIN ONLY
    }

    /**
     * Determine whether the user can update the amigoo database models website.
     *
     * @param  User     $user
     * @param  Website  $site
     * @return mixed
     */
    public function update(User $user, Website $site)
    {
        return $this->isModeratorOfSite($user, $site);
    }

    /**
     * Determine whether the user can delete the amigoo database models website.
     *
     * @param  User     $user
     * @param  Website  $site
     * @return mixed
     */
    public function delete(User $user, Website $site)
    {
        return $this->isModeratorOfSite($user, $site);
    }

    /**
     * Determine whether the user can restore the amigoo database models website.
     *
     * @param  User     $user
     * @param  Website  $site
     * @return mixed
     */
    public function restore(User $user, Website $site)
    {
        return false; // ADMIN ONLY
    }

    /**
     * Determine whether the user can permanently delete the amigoo database models website.
     *
     * @param  User     $user
     * @param  Website  $site
     * @return mixed
     */
    public function forceDelete(User $user, Website $site)
    {
        return false; // ADMIN ONLY
    }

    public function configLinkableStripeAccs(User $user, ?Website $site = null)
    {
        if ($site)
            return $this->isModeratorOfSite($user, $site);

        return $user->hasRole(Role::WEBSITE_MODERATOR);
    }


    private function isModeratorOfSite(User $user, Website $site)
    {
        $siteModerators = $site->moderators;
        $isThisSiteMod  = $siteModerators->contains(fn($mod) => $mod->id === $user->id);

        return $isThisSiteMod;
    }
}
