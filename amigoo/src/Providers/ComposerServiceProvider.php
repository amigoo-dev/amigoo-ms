<?php

namespace Amigoo\Providers;

use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        View::addNamespace('amigoo', __DIR__.'/../../resources/views/');
    }
}
