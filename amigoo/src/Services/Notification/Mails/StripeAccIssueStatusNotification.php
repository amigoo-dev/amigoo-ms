<?php

namespace Amigoo\Services\Notification\Mails;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Carbon\Carbon;
use Illuminate\Mail\Mailable;
use NumberFormatter;

class StripeAccIssueStatusNotification extends Mailable
{
    /** @var StripeAccount */
    protected $stripeAcc;

    /** @var Website */
    protected $website;

    /** @var float */
    protected $todayReceived;

    /** @var float */
    protected $totalReceived;

    /** @var Carbon */
    protected $latestOrderCreatedAt;

    public function __construct(
            StripeAccount $stripeAcc,
            ?Website $website,
            float $todayReceived,
            float $totalReceived,
            ?Carbon $latestOrderCreatedAt
            ) {
        $this->stripeAcc            = $stripeAcc;
        $this->website              = $website;
        $this->todayReceived        = $todayReceived;
        $this->totalReceived        = $totalReceived;
        $this->latestOrderCreatedAt = $latestOrderCreatedAt;
    }

    public function build()
    {
        $stripeAcc = $this->stripeAcc;
        $website   = $this->website;

        $subject = "Stripe [{$stripeAcc->email}] has changed to [{$stripeAcc->status}]";

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        $formattedTodayReceived = $fmt->formatCurrency($this->todayReceived, 'USD');
        $formattedTotalReceived = $fmt->formatCurrency($this->totalReceived, 'USD');

        $latestOrderAt = $this->latestOrderCreatedAt;

        $viewData = compact(
            'stripeAcc', 'website',
            'formattedTodayReceived',
            'formattedTotalReceived',
            'latestOrderAt'
            );

        return $this
            ->view('amigoo::mail.stripe-acc-issue-status-notification')
            ->subject($subject)
            ->with($viewData);
    }
}
