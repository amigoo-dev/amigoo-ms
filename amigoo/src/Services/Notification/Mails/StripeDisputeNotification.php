<?php

namespace Amigoo\Services\Notification\Mails;

use Amigoo\Database\Models\StripeDispute;
use Carbon\Carbon;
use Illuminate\Mail\Mailable;
use NumberFormatter;

class StripeDisputeNotification extends Mailable
{
    /** @var StripeDispute */
    protected $dispute;

    public function __construct(StripeDispute $dispute)
    {
        $this->dispute = $dispute;
    }

    public function build()
    {
        $dispute = $this->dispute;

        $subject = "New dispute of Stripe-acc. [{$dispute->stripeAccount->email}] on website [{$dispute->website->name}]";

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        $formattedAmount = $fmt->formatCurrency($dispute->amount, $dispute->currency_code);

        $viewData = compact('dispute', 'formattedAmount');

        return $this
            ->view('amigoo::mail.stripe-dispute-notification')
            ->subject($subject)
            ->with($viewData);
    }
}
