<?php

namespace Amigoo\Services\Notification;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Constants\NotificationType;
use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\MerchizeOrdersRepo;
use Amigoo\Database\Repos\UsersRepo;
use Amigoo\Services\Notification\Mails\StripeAccIssueStatusNotification;
use Illuminate\Support\Facades\Mail;

class StripeAccIssueNotifier
{
    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /** @var MerchizeOrdersRepo */
    private $merchizeOrdersRepo;

    /** @var UsersRepo */
    private $usersRepo;

    public function __construct(
            WebsitesRepo $websitesRepo,
            WooOrdersRepo $wooOrdersRepo,
            MerchizeOrdersRepo $merchizeOrdersRepo,
            UsersRepo $usersRepo
            ) {
        $this->websitesRepo       = $websitesRepo;
        $this->wooOrdersRepo      = $wooOrdersRepo;
        $this->merchizeOrdersRepo = $merchizeOrdersRepo;
        $this->usersRepo          = $usersRepo;
    }

    public function notify(StripeAccount $stripeAcc)
    {
        // Can by NULL, in case of a floating Stripe-account (no linkable site)
        $website = $this->websitesRepo->getSitesLinkableToStripeAccount($stripeAcc)->first();

        $wooTodayReceived = $this->wooOrdersRepo->getTodayReceivedAmountForStripeAcc($stripeAcc->id);
        $wooTotalReceived = $this->wooOrdersRepo->getTotalReceivedAmountForStripeAcc($stripeAcc->id);
        $merchizeTodayReceived = $this->merchizeOrdersRepo->getTodayReceivedAmountForStripeAcc($stripeAcc->id);
        $merchizeTotalReceived = $this->merchizeOrdersRepo->getTotalReceivedAmountForStripeAcc($stripeAcc->id);
        $todayReceived = $wooTodayReceived + $merchizeTodayReceived;
        $totalReceived = $wooTotalReceived + $merchizeTotalReceived;

        $wooLatestOrder      = $this->wooOrdersRepo->getLatestOrderForStripeAcc($stripeAcc->id);
        $merchizeLatestOrder = $this->merchizeOrdersRepo->getLatestOrderForStripeAcc($stripeAcc->id);
        $latestOrderAt       = null;
        if ($wooLatestOrder)
            $latestOrderAt = $wooLatestOrder->created_at;
        if ($merchizeLatestOrder && $merchizeLatestOrder->created_at->greaterThan($wooLatestOrder))
            $latestOrderAt = $merchizeLatestOrder->created_at;

        $superAdmin = $this->usersRepo->getSuperAdmin();
        $siteMods      = $website ? $this->usersRepo->getWebsiteModeratorsByNotificationType($website, NotificationType::STRIPE_ACC_ISSUE_STATUS) : [];
        $siteModEmails = $siteMods ? $siteMods->map(fn($mod) => $mod->email)->all() : [];

        $mail = new StripeAccIssueStatusNotification(
            $stripeAcc,
            $website,
            $todayReceived,
            $totalReceived,
            $latestOrderAt
        );

        Mail::to($superAdmin->email)
            ->cc($siteModEmails)
            ->send($mail);
    }
}
