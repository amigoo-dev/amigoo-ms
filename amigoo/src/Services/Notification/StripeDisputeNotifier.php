<?php

namespace Amigoo\Services\Notification;

use Amigoo\Database\Models\StripeDispute;
use Amigoo\Database\Models\Constants\NotificationType;
use Amigoo\Database\Repos\UsersRepo;
use Amigoo\Services\Notification\Mails\StripeDisputeNotification;
use Illuminate\Support\Facades\Mail;

class StripeDisputeNotifier
{
    /** @var UsersRepo */
    private $usersRepo;

    public function __construct(UsersRepo $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function notify(StripeDispute $dispute)
    {
        // Can by NULL, in case of a floating Stripe-account (no linkable site)
        // when the mail of dispute is received.
        $website = $dispute->website;

        $superAdmin = $this->usersRepo->getSuperAdmin();
        $siteMods      = $website ? $this->usersRepo->getWebsiteModeratorsByNotificationType($website, NotificationType::STRIPE_DISPUTE) : [];
        $siteModEmails = $siteMods ? $siteMods->map(fn($mod) => $mod->email)->all() : [];

        $mail = new StripeDisputeNotification($dispute);

        Mail::to($superAdmin->email)
            ->cc($siteModEmails)
            ->send($mail);
    }
}
