<?php

namespace Amigoo\Services\Stripe;

use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Illuminate\Support\Facades\Log;

abstract class StripeAccStatusEmailMapper
{
    const WAITING_VERIFY_PHONE_PATTERN    = '/https:\/\/dashboard.stripe.com\/phone-verification\?source=email/i';

    const WAITING_VERIFY_ID_PATTERN_1     = '/https:\/\/dashboard.stripe.com\/account\/verifications/i';
    const WAITING_VERIFY_ID_PATTERN_2     = '/we need to have you upload a scan or photograph of a government-issued photo ID via your Dashboard/i';

    const WAITING_VERIFY_IDENTITY_PATTERN = '/https:\/\/dashboard.stripe.com\/identity-verification/i';

    const WAITING_VERIFY_TAX_PATTERN      = '/https:\/\/dashboard.stripe.com\/taxid/i';

    const WAITING_VERIFY_SITE_PATTERN     = '/Please take a minute to update your website on your Stripe account: https:\/\/dashboard.stripe.com\/settings\/account/i';

    const WAITING_SUBMIT_W8_PATTERN       = '/unless you complete a Form W-8 for your business legal entity/i';

    const NEED_UPDATE_BANK_PATTERN        = '/payouts will be sent until you update your bank account to another active checking account/i';

    const WAITING_VERIFY_DOC_PATTERN      = '/We\'re writing to request some additional information that we couldn\'t verify using your account/i';

    const WAITING_VERIFY_DOMAIN_PATTERN   = '/we need to confirm that you have control over the domain/i';

    const ENDING_PATTERN_1                = '/after reviewing your submitted information and website, it does seem like your business presents a higher level of risk than we can currently support/i';
    const ENDING_PATTERN_2                = '/due to the high level of risk on your account, we are ending service effective immediately/i';
    const ENDING_PATTERN_3                = '/we\'re sorry that we won\'t be able to support your business here at stripe/i';

    const REFUNDED_ALL_PATTERN            = '/Refunds on card payments will be issued in 5–7 business days, although they may take longer to appear on the cardholder/i';

    const HOLD_90_PATTERN                 = '/your account balance will be placed on reserve for the next 90 days/i';

    const HOLD_120_PATTERN                = '/additionally, because of the elevated dispute risk on your account, your account balance will be placed on reserve for the next 120 days/i';


    private static function getAvailableCases(): array
    {
        return [
            [
                'status'  => StripeAccountStatus::WAITING_VERIFY_PHONE,
                'pattern' => self::WAITING_VERIFY_PHONE_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::WAITING_VERIFY_ID,
                'pattern' => [
                    self::WAITING_VERIFY_ID_PATTERN_1,
                    self::WAITING_VERIFY_ID_PATTERN_2,
                ],
            ],
            [
                'status'  => StripeAccountStatus::WAITING_VERIFY_IDENTITY,
                'pattern' => self::WAITING_VERIFY_IDENTITY_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::WAITING_VERIFY_TAX,
                'pattern' => self::WAITING_VERIFY_TAX_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::WAITING_VERIFY_SITE,
                'pattern' => self::WAITING_VERIFY_SITE_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::WAITING_SUBMIT_W8,
                'pattern' => self::WAITING_SUBMIT_W8_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::NEED_UPDATE_BANK,
                'pattern' => self::NEED_UPDATE_BANK_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::WAITING_VERIFY_DOC,
                'pattern' => self::WAITING_VERIFY_DOC_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::WAITING_VERIFY_DOMAIN,
                'pattern' => self::WAITING_VERIFY_DOMAIN_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::ENDING,
                'pattern' => [
                    self::ENDING_PATTERN_1,
                    self::ENDING_PATTERN_2,
                    self::ENDING_PATTERN_3,
                ],
            ],
            [
                'status'  => StripeAccountStatus::REFUNDED_ALL,
                'pattern' => self::REFUNDED_ALL_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::HOLD_90,
                'pattern' => self::HOLD_90_PATTERN,
            ],
            [
                'status'  => StripeAccountStatus::HOLD_120,
                'pattern' => self::HOLD_120_PATTERN,
            ],
        ];
    }

    public static function getMatchingStatusForContentHtml(string $html)
    {
        $cleanContent   = self::decodeHtml($html);
        $availableCases = self::getAvailableCases();
        $status         = null;

        foreach ($availableCases as $case) {
            $hasMultiplePatterns = is_array($case['pattern']);

            $result =
                $hasMultiplePatterns
                    ? self::contentMatchAnyPattern($cleanContent, $case['pattern'])
                    : self::contentMatchPattern($cleanContent, $case['pattern']);

            if ($result['matched']) {
                $status = $case['status'];

                if ($status == StripeAccountStatus::ENDING
                        && $result['pattern'] == self::ENDING_PATTERN_2
                        && self::contentMatchPattern($cleanContent, self::HOLD_120_PATTERN)['matched']) {

                    $status = StripeAccountStatus::HOLD_120;
                    Log::debug('MATCHED ENDING -> 120 DAYS');
                }

                break;
            }
        }

        return $status;
    }

    private static function decodeHtml(string $html): string
    {
        return
            str_replace("\xc2\xa0", ' ',
                preg_replace(
                    '/\s+/', ' ',
                    html_entity_decode(
                        strip_tags($html),
                        ENT_QUOTES
                    )
                )
            );
    }

    private static function contentMatchAnyPattern(string $content, array $patterns)
    {
        foreach ($patterns as $pattern) {
            $result = self::contentMatchPattern($content, $pattern);

            if ($result['matched'])
                return $result;
        }

        return ['matched' => false];
    }

    private static function contentMatchPattern(string $content, string $pattern)
    {
        $matched = preg_match($pattern, $content) === 1;

        return $matched
            ? compact('matched', 'pattern')
            : compact('matched');
    }
}
