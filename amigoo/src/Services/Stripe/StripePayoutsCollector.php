<?php

namespace Amigoo\Services\Stripe;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\ProxyInfo;
use Amigoo\Database\Repos\ProxyInfoRepo;
use Amigoo\Database\Repos\StripePayoutsRepo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Stripe\StripeClient;
use Stripe\HttpClient\CurlClient as StripeCurlClient;
use Stripe\ApiRequestor as StripeApiRequestor;

class StripePayoutsCollector
{
    /** @var StripePayoutsRepo */
    private $payoutsRepo;

    /** @var ProxyInfoRepo */
    private $proxyInfoRepo;

    public function __construct(StripePayoutsRepo $payoutsRepo, ProxyInfoRepo $proxyInfoRepo)
    {
        $this->payoutsRepo   = $payoutsRepo;
        $this->proxyInfoRepo = $proxyInfoRepo;
    }

    public function collect(StripeAccount $stripeAcc, ?Carbon $createdAtAfter = null)
    {
        $stripe = new StripeClient($stripeAcc->secret_key);

        if (! $stripeAcc->proxyInfo)
            throw new \Exception("Proxy for Stripe-acc [{$stripeAcc->email}] not yet configured");
        $this->setupCurlOptionsForStripeClient($stripeAcc->proxyInfo);

        $insertedPOIds = $this->payoutsRepo->getInsertedPOIdsForStripeAcc($stripeAcc);
        $queryOptions  = $this->createApiQueryOptions($createdAtAfter);

        if ($createdAtAfter)
            Log::info("Collecting payouts for {$stripeAcc->email} after {$createdAtAfter->toDateTimeString()}");
        else
            Log::info("Collecting payouts for {$stripeAcc->email}");

        $count = [
            'inserted' => 0,
            'updated'  => 0,
        ];
        $failed = false;
        try {
            $payoutsCollection = $stripe->payouts->all($queryOptions);
            foreach ($payoutsCollection->autoPagingIterator() as $payoutData) {
                $poId = $payoutData['id'];

                $alreadyInserted = in_array($poId, $insertedPOIds);
                try {
                    if ($alreadyInserted) {
                        $this->updatePayout($payoutData);
                        ++ $count['updated'];
                    } else {
                        $this->insertNewPayout($stripeAcc, $payoutData);
                        ++ $count['inserted'];
                    }
                } catch (\Exception $e) {
                    Log::error("Failed when DB insert/update payout {$poId} of {$stripeAcc->email}", compact('e'));
                }
            }
        } catch (\Exception $e) {
            Log::error("Failed while iterating payouts collection of {$stripeAcc->email}", compact('e'));
            $failed = true;

        } finally {
            if ($count['inserted'])
                Log::info("Inserted {$count['inserted']} new records for {$stripeAcc->email}");
            if ($count['updated'])
                Log::info("Updated {$count['updated']} records for {$stripeAcc->email}");

            $this->proxyInfoRepo->updateLastProcStatus($stripeAcc->proxyInfo, Carbon::now(), $failed);

            return ! $failed;
        }

    }

    private function createApiQueryOptions(?Carbon $createdAtAfter)
    {
        $options = [
            'limit' => 50
        ];

        if ($createdAtAfter)
            $options['created'] = ['gte' => $createdAtAfter->getTimestamp()];

        return $options;
    }

    private function setupCurlOptionsForStripeClient(?ProxyInfo $proxyConfig)
    {
        $curlOptions = null;

        if ($proxyConfig) {
            $curlOptions = [
                CURLOPT_PROXY     => $proxyConfig->ip,
                CURLOPT_PROXYPORT => $proxyConfig->port,
            ];

            if ($proxyConfig->user) {
                $usr = $proxyConfig->user;

                if ($proxyConfig->password) {
                    $curlOptions[CURLOPT_PROXYUSERPWD] = "{$usr}:{$proxyConfig->password}";
                } else {
                    $curlOptions[CURLOPT_PROXYUSERNAME] = $usr;
                }
            }
        }

        $curlClient = new StripeCurlClient($curlOptions);
        $curlClient->setTimeout(300);
        StripeApiRequestor::setHttpClient($curlClient);
    }

    private function updatePayout($payoutData)
    {
        $poId       = $payoutData['id'];
        $realAmount = $payoutData['amount'] / 100;

        $this->payoutsRepo->updateByPOId($poId, [
            'type'          => $payoutData['type'],
            'status'        => $payoutData['status'],
            'amount'        => $realAmount,
            'currency_code' => $payoutData['currency'],
            'arrival_date'  => new Carbon($payoutData['arrival_date']),
        ]);
    }

    private function insertNewPayout(StripeAccount $stripeAcc, $payoutData)
    {
        $poId        = $payoutData['id'];
        $realAmount  = $payoutData['amount'] / 100;

        $this->payoutsRepo->insert(
            $stripeAcc,
            $poId,
            $payoutData['type'],
            $payoutData['status'],
            $realAmount,
            $payoutData['currency'],
            new Carbon($payoutData['arrival_date']),
            new Carbon($payoutData['created'])
        );
    }
}
