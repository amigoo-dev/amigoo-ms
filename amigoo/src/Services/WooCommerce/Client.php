<?php

namespace Amigoo\Services\WooCommerce;

use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Automattic\WooCommerce\Client as WooApi;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use Illuminate\Support\Facades\Log;

class Client
{
    /** @var WooApi */
    private $wooApi;

    public function __construct(string $url, string $consumerKey, string $consumerSecret)
    {
        $this->wooApi = new WooApi(
            $url,
            $consumerKey,
            $consumerSecret,
            [
                'wp_api'            => true,
                'version'           => 'wc/v3',
                'query_string_auth' => true,
            ]
        );
    }

    /**
     * Instantiate a new client from a Website record
     */
    public static function fromWebsite(Website $site): Client
    {
        return new Client(
            $site->url,
            $site->api_consumer_key,
            $site->api_consumer_secret
        );
    }

    public function getOrderDetails(int $orderId): \stdClass
    {
        try {
            $endpoint = "orders/{$orderId}";

            $orderDetails = $this->wooApi->get($endpoint);

            return $orderDetails;

        } catch (\Exception $e) {
            throw new WooApiException("Order details request failed for #{$orderId}", null, $e);
        }
    }

    public function updatePaymentGatewayStripe(string $newPublicKey, string $newSecretKey)
    {
        try {
            $endpoint = 'payment_gateways/stripe';
            $requestPayload = [
                'enabled' => true,
                'settings' => [
                    'publishable_key' => $newPublicKey,
                    'secret_key'      => $newSecretKey,
                ],
            ];

            $paymentGatewayDetails = $this->wooApi->put($endpoint, $requestPayload);
            $this->checkReplacementResult($paymentGatewayDetails, $newPublicKey, $newSecretKey);

        } catch (\Exception $e) {
            // TODO: Improve this!
            if ($e instanceof \Automattic\WooCommerce\HttpClient\HttpClientException) {
                if ($e->getResponse()->getCode() == 200) {
                    return;
                }
            }

            throw new WooApiException($e->getMessage(), null, $e);
        }
    }

    private function checkReplacementResult(\stdClass $paymentGatewayDetails, string $newPublicKey, string $newSecretKey)
    {
        $failed = ! $paymentGatewayDetails
            || $paymentGatewayDetails->settings->publishable_key->value != $newPublicKey
            || $paymentGatewayDetails->settings->secret_key->value != $newSecretKey;

        if ($failed)
            throw new WooApiException('Public-key & secret-key updation failed');
    }
}
