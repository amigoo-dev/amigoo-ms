<?php

namespace Amigoo\Services\WooCommerce;

use Amigoo\Database\Models\Website;
use Amigoo\Database\Repos\WooOrdersRepo;
use Illuminate\Support\Facades\Log;

class OrderDataCollector
{
    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    private $beforeCollectingHandler;
    private $afterSingleQueryDoneHandler;

    public function __construct(WooOrdersRepo $wooOrdersRepo)
    {
        $this->wooOrdersRepo = $wooOrdersRepo;
    }

    public function setBeforeCollectingHandler($handler)
    {
        $this->beforeCollectingHandler = $handler;
    }

    public function setAfterSingleQueryDoneHandler($handler)
    {
        $this->afterSingleQueryDoneHandler = $handler;
    }

    public function collectForSite(Website $site)
    {
        $client = new Client($site->url, $site->api_consumer_key, $site->api_consumer_secret);
        $orders = $this->wooOrdersRepo->getOrdersNeedCollectingDataBySite($site);

        $this->callHandlerBeforeCollecting($orders);
        foreach ($orders as $order) {
            try {
                $orderDetails = $client->getOrderDetails($order->woo_order_id);
                $stripeIntentId = $this->getStripeIntentIdFromOrderDetails($orderDetails);
                $stripeChargeId = $orderDetails->transaction_id;

                $this->wooOrdersRepo->updateMetadata($order, [
                    'order_number'     => $orderDetails->number,
                    'stripe_intent_id' => $stripeIntentId,
                    'stripe_charge_id' => $stripeChargeId,
                ]);

                $this->callHandlerAfterSingleQueryDone($order);

            } catch (\Exception $e) {
                $prevE = $e->getPrevious();

                if (!$prevE || \preg_match('/^cURL Error/', $prevE->getMessage())) {
                    throw $e;
                } else {
                    Log::error("Failed to collect meta-data for order [{$order->woo_order_id}] of site [{$site->name}]", compact('e'));
                    $this->callHandlerAfterSingleQueryDone($order);
                }
            }
        }
    }

    private function getStripeIntentIdFromOrderDetails(\stdClass $orderDetails): ?string
    {
        $stripeIntentIdRecord =
            collect($orderDetails->meta_data)
                ->first(fn($data) => $data->key == '_stripe_intent_id');

        return $stripeIntentIdRecord ? $stripeIntentIdRecord->value : null;
    }

    private function callHandlerBeforeCollecting($orders)
    {
        if ($this->beforeCollectingHandler)
            ($this->beforeCollectingHandler)($orders);
    }

    private function callHandlerAfterSingleQueryDone($order)
    {
        if ($this->afterSingleQueryDoneHandler)
            ($this->afterSingleQueryDoneHandler)($order);
    }
}
