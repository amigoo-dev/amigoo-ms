<?php

namespace Amigoo\Services\WooCommerce\Stripe;

use Amigoo\Database\Models\Constants\StripeAccountStatus as Status;
use Amigoo\Database\Models\Constants\WebsiteType;
use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\WebsitesRepo;
use Amigoo\Database\Repos\WebsitePaymentMethodSessionRepo;
use Amigoo\Services\WooCommerce\Client as WooApiClient;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Log;

class AccountSwitcher
{
    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var WebsitesRepo */
    private $websitesRepo;

    /** @var WebsitePaymentMethodSessionRepo */
    private $paymentMethodSessionRepo;

    public function __construct(
            StripeAccountsRepo $stripeAccsRepo,
            WebsitesRepo $websitesRepo,
            WebsitePaymentMethodSessionRepo $paymentMethodSessionRepo
            ) {
        $this->stripeAccsRepo           = $stripeAccsRepo;
        $this->websitesRepo             = $websitesRepo;
        $this->paymentMethodSessionRepo = $paymentMethodSessionRepo;
    }

    public function switch(StripeAccount $accShouldBeSwitched, string $newStatus = null, bool $forceStatusUpdate = false): ?SwitcherResult
    {
        if ($newStatus == Status::READY)
            throw new \Exception("Account is not allowed to be switched if new status is [READY]");

        $apiResult          = null; // This variable is put here for exception to use if thrown.
        $curStatus          = $accShouldBeSwitched->status;
        $newStatusProvided  = ($newStatus !== null);
        try {
            if ($newStatusProvided) {
                $shouldUpdateStatus = $forceStatusUpdate || $this->shouldStatusUpdate($curStatus, $newStatus);
                if ($shouldUpdateStatus) {
                    $this->stripeAccsRepo->updateStatus($accShouldBeSwitched, $newStatus);
                } else {
                    Log::warning("Skip changing status from [{$curStatus}] to [{$newStatus}]");
                }
            } else {
                Log::warning("Skip changing status for no new status provided");
            }

            $sitesShouldBeSwitched = $this->websitesRepo->getSitesByStripeAccount($accShouldBeSwitched);
            if ($sitesShouldBeSwitched->isEmpty()) {
                Log::debug('Sites should be switched: N/A');
                return null;
            }
            Log::debug("Sites should be switched: {$sitesShouldBeSwitched->map(fn($acc) => $acc->url)->join(', ')}");

            $siteNewAccMap       = $this->getRandomUsableAccountsOrDefaultAccount($sitesShouldBeSwitched->all());
            $sitesNeedApiCall    = $sitesShouldBeSwitched->filter(fn($site) => $site->needApiCall());
            $sitesNotNeedApiCall = $sitesShouldBeSwitched->filter(fn($site) => $site->notNeedApiCall());

            // CURRENTLY, ONLY WooCommerce sites need API call;
            $apiResult = $this->callWooApiUpdatePaymentGatewayStripe($sitesNeedApiCall->all(), $siteNewAccMap);
            Log::debug('Sites updated: '.join(', ', array_map(fn($site) => $site->url, $apiResult->updatedSites)));

            $sitesToUpdateDb = [
                ...$apiResult->updatedSites,
                ...$sitesNotNeedApiCall,
            ];
            DB::transaction(function () use ($sitesToUpdateDb, $siteNewAccMap, $accShouldBeSwitched, $newStatus) {
                foreach ($sitesToUpdateDb as $updatedSite) {
                    $this->websitesRepo->unlinkFromStripeAccount($updatedSite);
                    $this->paymentMethodSessionRepo->endCurrentSessionForSite($updatedSite);

                    $accForNewSession = $siteNewAccMap[$updatedSite->id];
                    $this->websitesRepo->linkToStripeAccount($updatedSite, $accForNewSession);
                    $this->paymentMethodSessionRepo->createSessionWithStripe($updatedSite, $accForNewSession);
                }
            });

            // TODO REPLACE THIS BLOCK BY NOTIFICATION FOR FAILED REPLACEMENT (SHOULD BE DONE BY USING JOBS).
            if ($apiResult->hasFailure())
                $this->logFailures($apiResult->failures);

            return $apiResult;

        } catch (\Exception $e) {
            throw new SwitcherException($apiResult, $e);
        }
    }

    private function shouldStatusUpdate(string $curStatus, string $newStatus): bool
    {
        if (in_array($newStatus, [Status::REACHED_DAILY_MAX, Status::WAITING_PAYOUT]))
            if ($curStatus != Status::READY)
                return false;

        return true;
    }

    private function getRandomUsableAccountsOrDefaultAccount(array $sitesShouldBeSwitched): array
    {
        $siteNewAccMap = [];

        foreach ($sitesShouldBeSwitched as $site) {
            $usableAccs = $this->stripeAccsRepo->getLinkableAccountsForSwitching($site);

            if ($usableAccs->isEmpty()) {
                try {
                    $siteNewAccMap[$site->id] = $this->stripeAccsRepo->getDefaultAccount($site);
                    Log::debug("Default Stripe account is selected for {$site->name}");

                } catch (ModelNotFoundException $e) {
                    Log::error("No default account is configured for {$site->name}");
                }

            } else {
                $randomAcc                = $usableAccs->random();
                $siteNewAccMap[$site->id] = $randomAcc;
                Log::debug("Stripe acc [{$randomAcc->email}] is selected for {$site->name}");
            }
        }

        return $siteNewAccMap;
    }

    private function callWooApiUpdatePaymentGatewayStripe(array $sitesShouldBeSwitched, array $siteNewAccMap): SwitcherResult
    {
        $result = new SwitcherResult;

        foreach ($sitesShouldBeSwitched as $site) {
            $wooApiClient     = WooApiClient::fromWebsite($site);
            $accForNewSession = $siteNewAccMap[$site->id];

            try {
                $wooApiClient->updatePaymentGatewayStripe($accForNewSession->public_key, $accForNewSession->secret_key);
                $result->addUpdatedSite($site);

            } catch (\Exception $e) {
                $result->addFailure($site, $e);
            }
        }

        return $result;
    }

    private function logFailures(array $failures)
    {
        $failedSiteIds = array_map(fn($failure) => $failure->site->id, $failures);
        Log::error('Some websites have not been updated replacement: ' . join(', ', $failedSiteIds));

        array_walk($failures,
            fn($failure) =>
                Log::error(
                    "Error from website #{$failure->site->id}: {$failure->exception->getMessage()}\n" .
                    $failure->exception->getTraceAsString()
                )
        );
    }
}
