<?php

namespace Amigoo\Services\WooCommerce\Stripe;

use Amigoo\Database\Models\Website;

class Failure
{
    /** @var Website */
    public $site;

    /** @var \Exception */
    public $exception;

    public function __construct(Website $site, \Exception $e)
    {
        $this->site      = $site;
        $this->exception = $e;
    }
}
