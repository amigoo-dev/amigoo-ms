<?php

namespace Amigoo\Services\WooCommerce\Stripe;

class SwitcherException extends \Exception
{
    /** @var SwitcherResult */
    public $switcherResult;

    /**
     * @var SwitcherResult|null $switcherResult
     * @var \Exception          $prevE
     */
    public function __construct($switcherResult, \Exception $prevE)
    {
        parent::__construct(null, null, $prevE);

        $this->switcherResult = $switcherResult;
    }
}
