<?php

namespace Amigoo\Services\WooCommerce\Stripe;

use Amigoo\Database\Models\Website;

class SwitcherResult
{
    public $updatedSites = [];

    public $failures = [];

    public function addUpdatedSite(Website $site)
    {
        $this->updatedSites[] = $site;
    }

    public function addFailure(Website $site, \Exception $e)
    {
        $this->failures[] = new Failure($site, $e);
    }

    public function hasFailure()
    {
        return ! empty($this->failures);
    }
}
