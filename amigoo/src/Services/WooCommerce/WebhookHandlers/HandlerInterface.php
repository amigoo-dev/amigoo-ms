<?php

namespace Amigoo\Services\WooCommerce\WebhookHandlers;

use Amigoo\Database\Models\Website;

interface HandlerInterface
{
    function handle(Website $site, array $requestPayload);
}
