<?php

namespace Amigoo\Services\WooCommerce\WebhookHandlers;

class HandlerMapper
{
    /**
     * @return HandlerInterface|null
     */
    public function getHandler(string $topic)
    {
        $map = $this->getHandlersMap();

        if (isset($map[$topic])) {
            $handlerClass = $map[$topic];
            return resolve($handlerClass);
        }

        return null;
    }

    private function getHandlersMap(): array
    {
        return [
            'action.woocommerce_order_status_failed'     => OrderStatusFailedHandler::class,
            'action.woocommerce_order_status_processing' => OrderStatusProcessingOrCompletedHandler::class,
            'action.woocommerce_order_status_completed'  => OrderStatusProcessingOrCompletedHandler::class,
        ];
    }
}
