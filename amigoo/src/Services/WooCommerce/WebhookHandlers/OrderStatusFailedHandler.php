<?php

namespace Amigoo\Services\WooCommerce\WebhookHandlers;

use Amigoo\Database\Models\Constants\OrderStatus;
use Amigoo\Database\Models\Constants\PaymentMethod;
use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\WebsitePaymentMethodSessionRepo as SessionRepo;
use Amigoo\Services\WooCommerce\Client as WooApiClient;
use Amigoo\Services\WooCommerce\Stripe\AccountSwitcher as StripeAccSwitcher;
use Carbon\Carbon;

class OrderStatusFailedHandler implements HandlerInterface
{
    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /** @var SessionRepo */
    private $sessionRepo;

    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var StripeAccSwitcher */
    private $stripeAccSwitcher;

    public function __construct(
            WooOrdersRepo $wooOrdersRepo,
            SessionRepo $sessionRepo,
            StripeAccountsRepo $stripeAccsRepo,
            StripeAccSwitcher $stripeAccSwitcher
            ) {
        $this->wooOrdersRepo     = $wooOrdersRepo;
        $this->sessionRepo       = $sessionRepo;
        $this->stripeAccsRepo    = $stripeAccsRepo;
        $this->stripeAccSwitcher = $stripeAccSwitcher;
    }

    public function handle(Website $site, array $requestPayload)
    {
        $wooApiClient = WooApiClient::fromWebsite($site);

        $wooOrderId     = $this->getOrderIdFromPayload($requestPayload);
        $orderDetails   = $wooApiClient->getOrderDetails($wooOrderId);
        $orderTotal     = $this->getTotalFromOrderDetails($orderDetails);
        $paymentMethod  = $this->getPaymentMethodFromOrderDetails($orderDetails);
        $orderCreatedAt = $this->getDateCreatedFromOrderDetails($orderDetails);

        $orderSession = $this->sessionRepo->getSessionForSiteByTime($site, $orderCreatedAt);
        if ($orderSession == null)
            throw new \Exception('Website Payment Method Session not found');

        if ($paymentMethod == PaymentMethod::STRIPE) {

            if ($orderSession->payment_method != PaymentMethod::STRIPE)
                throw new \Exception("Session found not linking to a Stripe acc. by site {$site->name}'s order #{$wooOrderId}'s at paid time");

            $stripeAcc = $this->stripeAccsRepo->getById($orderSession->payment_acc_id);

            $this->wooOrdersRepo->insert($site, $paymentMethod, $stripeAcc->id, $wooOrderId, OrderStatus::FAILED, $orderTotal, $orderCreatedAt);

            if ($this->reachedMaxConsecutiveFailures($site, $stripeAcc)) {
                $this->stripeAccSwitcher->switch($stripeAcc, StripeAccountStatus::FAILED_TOO_MUCH);
            }
            // else {
            //     $this->stripeAccSwitcher->switch($stripeAcc);
            // }

        } else {
            throw new \Exception("Payment method [{$paymentMethod}] not supported");
        }
    }

    private function reachedMaxConsecutiveFailures(Website $site, StripeAccount $acc)
    {
        $latestConsecutiveFailuresAmount =
            $this->wooOrdersRepo->countLatestConsecutiveFailuresForStripeAcc($acc->id, $site->change_stripe_status_failed_amount);

        return ($latestConsecutiveFailuresAmount >= $site->change_stripe_status_failed_amount);
    }

    private function getOrderIdFromPayload(array $payload)
    {
        return $payload['arg'];
    }

    private function getTotalFromOrderDetails(\stdClass $orderDetails): float
    {
        return floatval($orderDetails->total);
    }

    private function getPaymentMethodFromOrderDetails(\stdClass $orderDetails): string
    {
        return $orderDetails->payment_method;
    }

    private function getDateCreatedFromOrderDetails(\stdClass $orderDetails): Carbon
    {
        return new Carbon($orderDetails->date_created_gmt);
    }
}
