<?php

namespace Amigoo\Services\WooCommerce\WebhookHandlers;

use Amigoo\Database\Models\Constants\OrderStatus;
use Amigoo\Database\Models\Constants\PaymentMethod;
use Amigoo\Database\Models\Constants\StripeAccountStatus;
use Amigoo\Database\Models\StripeAccount;
use Amigoo\Database\Models\Website;
use Amigoo\Database\Repos\WooOrdersRepo;
use Amigoo\Database\Repos\StripeAccountsRepo;
use Amigoo\Database\Repos\WebsitePaymentMethodSessionRepo as SessionRepo;
use Amigoo\Database\Repos\Exceptions\OrderAlreadyInsertedException;
use Amigoo\Services\WooCommerce\Client as WooApiClient;
use Amigoo\Services\WooCommerce\Stripe\AccountSwitcher as StripeAccSwitcher;
use Carbon\Carbon;

class OrderStatusProcessingOrCompletedHandler implements HandlerInterface
{
    /** @var WooOrdersRepo */
    private $wooOrdersRepo;

    /** @var SessionRepo */
    private $sessionRepo;

    /** @var StripeAccountsRepo */
    private $stripeAccsRepo;

    /** @var StripeAccSwitcher */
    private $stripeAccSwitcher;

    public function __construct(
            WooOrdersRepo $wooOrdersRepo,
            SessionRepo $sessionRepo,
            StripeAccountsRepo $stripeAccsRepo,
            StripeAccSwitcher $stripeAccSwitcher
            ) {
        $this->wooOrdersRepo     = $wooOrdersRepo;
        $this->sessionRepo       = $sessionRepo;
        $this->stripeAccsRepo    = $stripeAccsRepo;
        $this->stripeAccSwitcher = $stripeAccSwitcher;
    }

    public function handle(Website $site, array $requestPayload)
    {
        $wooApiClient = WooApiClient::fromWebsite($site);

        $wooOrderId     = $this->getOrderIdFromPayload($requestPayload);
        $orderDetails   = $wooApiClient->getOrderDetails($wooOrderId);
        $orderStatus    = $this->getStatusFromOrderDetails($orderDetails);
        $orderTotal     = $this->getTotalFromOrderDetails($orderDetails);
        $paymentMethod  = $this->getPaymentMethodFromOrderDetails($orderDetails);
        $orderCreatedAt = $this->getDateCreatedFromOrderDetails($orderDetails);
        $orderPaidAt    = $this->getDatePaidFromOrderDetails($orderDetails);
        $orderNumber    = $orderDetails->number;
        $stripeIntentId = $this->getStripeIntentIdFromOrderDetails($orderDetails);
        $stripeChargeId = $orderDetails->transaction_id;

        $orderSession = $this->sessionRepo->getSessionForSiteByTime($site, $orderPaidAt);
        if ($orderSession == null)
            throw new \Exception('Website Payment Method Session not found');

        if ($paymentMethod == PaymentMethod::STRIPE) {

            if ($orderSession->payment_method != PaymentMethod::STRIPE)
                throw new \Exception("Session found not linking to a Stripe acc. by site {$site->name}'s order #{$wooOrderId}'s at paid time");

            $stripeAcc = $this->stripeAccsRepo->getById($orderSession->payment_acc_id);

            try {
                $order = $this->wooOrdersRepo->insert($site, $paymentMethod, $stripeAcc->id, $wooOrderId, $orderStatus, $orderTotal, $orderCreatedAt, $orderPaidAt);
            } catch (OrderAlreadyInsertedException $e) {
                $order = $this->wooOrdersRepo->changeStatusFromFailedToProcessingOrCompleted($e->site, $e->wooOrderId, $stripeAcc->id, $orderStatus, $orderPaidAt);
            }

            $this->wooOrdersRepo->updateMetadata($order, [
                'order_number'     => $orderNumber,
                'stripe_intent_id' => $stripeIntentId,
                'stripe_charge_id' => $stripeChargeId,
            ]);

            $newStatusByReachedDailyMax = $this->selectNewStatusIfReachedDailyMax($stripeAcc);
            if ($newStatusByReachedDailyMax) {
                $this->stripeAccSwitcher->switch($stripeAcc, $newStatusByReachedDailyMax);

            } elseif ($stripeAcc->status == StripeAccountStatus::FAILED_TOO_MUCH) {
                $this->stripeAccsRepo->updateStatus($stripeAcc, StripeAccountStatus::READY);
            }

        } else {
            throw new \Exception("Payment method [{$paymentMethod}] not supported");
        }
    }

    private function selectNewStatusIfReachedDailyMax(StripeAccount $acc)
    {
        $todayReceivedAmount =
            $this->wooOrdersRepo->getTodayReceivedAmountForStripeAcc($acc->id);

        $reachedDailyMaxReceivedAmount =
            $todayReceivedAmount > $acc->daily_max_received_amount;

        if ($reachedDailyMaxReceivedAmount)
            return $acc->should_wait_for_payout
                ? StripeAccountStatus::WAITING_PAYOUT
                : StripeAccountStatus::REACHED_DAILY_MAX;

        return null;
    }

    private function getOrderIdFromPayload(array $payload): int
    {
        return $payload['arg'];
    }

    private function getStatusFromOrderDetails(\stdClass $orderDetails): string
    {
        return $orderDetails->status;
    }

    private function getTotalFromOrderDetails(\stdClass $orderDetails): float
    {
        return floatval($orderDetails->total);
    }

    private function getPaymentMethodFromOrderDetails(\stdClass $orderDetails): string
    {
        return $orderDetails->payment_method;
    }

    private function getDateCreatedFromOrderDetails(\stdClass $orderDetails): Carbon
    {
        return new Carbon($orderDetails->date_created_gmt);
    }

    private function getDatePaidFromOrderDetails(\stdClass $orderDetails): Carbon
    {
        return new Carbon($orderDetails->date_paid_gmt);
    }

    private function getStripeIntentIdFromOrderDetails(\stdClass $orderDetails): ?string
    {
        $stripeIntentIdRecord =
            collect($orderDetails->meta_data)
                ->first(fn($data) => $data->key == '_stripe_intent_id');

        return $stripeIntentIdRecord ? $stripeIntentIdRecord->value : null;
    }
}
