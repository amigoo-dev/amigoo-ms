<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Amigoo\Console\Commands\AutoSwitchStripeAccounts::class,
        \Amigoo\Console\Commands\CollectOrderDataForSite::class,
        \Amigoo\Console\Commands\CollectReservedFundsFor90And120DaysHoldStripeAccounts::class,
        \Amigoo\Console\Commands\CollectStripePayoutsDaily::class,
        \Amigoo\Console\Commands\ResetStripeStatusReady::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
