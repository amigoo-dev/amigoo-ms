<?php

namespace App\Models\Auth\Traits\Relationship;

use Amigoo\Database\Models\Website;

use App\Models\Auth\PasswordHistory;
use App\Models\Auth\SocialAccount;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }




    public function moderatableWebsites()
    {
        return $this->belongsToMany(Website::class, 'amigoo_website_user_moderatables', 'user_id', 'website_id');
    }
}
