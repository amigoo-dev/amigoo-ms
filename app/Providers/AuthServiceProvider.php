<?php

namespace App\Providers;

use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

/**
 * Class AuthServiceProvider.
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Amigoo\Database\Models\Website::class            => \Amigoo\Policies\WebsitePolicy::class,
        \Amigoo\Database\Models\StripeAccount::class      => \Amigoo\Policies\StripeAccountPolicy::class,
        \Amigoo\Database\Models\StripeDispute::class      => \Amigoo\Policies\StripeDisputePolicy::class,
        \Amigoo\Database\Models\StripeReservedFund::class => \Amigoo\Policies\StripeReservedFundPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();

        // Implicitly grant "Admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user) {
            return $user->hasRole(config('access.users.admin_role')) ? true : null;
        });
    }
}
