## Laravel Boilerplate (Current: Laravel 6.*) ([Demo](http://134.209.123.206/))

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-boilerplate)
[![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-boilerplate) 
<br/>
[![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://github.styleci.io/repos/30171828)
<br/>
![GitHub contributors](https://img.shields.io/github/contributors/rappasoft/laravel-boilerplate.svg)
![GitHub stars](https://img.shields.io/github/stars/rappasoft/laravel-boilerplate.svg?style=social)

### Demo Credentials

**User:** admin@admin.com  
**Password:** secret

## How to Work with Amigoo Migration

`php artisan migrate:xxx --path=amigoo/migrations`

eg.:

- `php artisan migrate --path=amigoo/migrations`
- `php artisan migrate:refresh --path=amigoo/migrations step=1`