<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            {{-- Wordpress Sites Index --}}
            @can('viewIndex', Amigoo\Database\Models\Website::class)
                <li class="nav-item {{
                    active_class(Route::is('admin/websites*'), 'open')
                }}">
                    <a href="{{ route('admin.websites.get.index') }}" class="nav-link {{
                        active_class(Route::is('admin/websites*'), 'active')
                    }}">
                        <i class="nav-icon fab fa-wordpress"></i>
                        Wordpress Sites
                    </a>
                </li>
            @endcan

            {{-- Stripe Accounts Index --}}
            @can('viewIndex', Amigoo\Database\Models\StripeAccount::class)
                <li class="nav-item {{
                    active_class(Route::is('admin/stripe-accs*'), 'open')
                }}">
                    <a href="{{ route('admin.stripe-accs.get.index') }}" class="nav-link {{
                        active_class(Route::is('admin/stripe-accs*'), 'active')
                    }}">
                        <i class="nav-icon fab fa-cc-stripe"></i>
                        Stripe Accounts
                    </a>
                </li>
            @endcan

            {{-- Stripe Account Creation Form --}}
            @can('create', Amigoo\Database\Models\StripeAccount::class)
                <li class="nav-item {{
                    active_class(Route::is('admin/stripe-accs/new'), 'open')
                }}">
                    <a href="{{ route('admin.stripe-accs.get.form.create') }}" class="nav-link {{
                        active_class(Route::is('admin/stripe-accs/new'), 'active')
                    }}">
                        <i class="nav-icon fab fa-cc-stripe"></i>
                        Add New Stripe Acc.
                    </a>
                </li>
            @endcan

            {{-- Stripe Disputes Index --}}
            @can('viewIndex', Amigoo\Database\Models\StripeDispute::class)
                <li class="nav-item {{
                    active_class(Route::is('admin/stripe-disputes*'), 'open')
                }}">
                    <a href="{{ route('admin.stripe-disputes.get.index') }}" class="nav-link {{
                        active_class(Route::is('admin/stripe-disputes*'), 'active')
                    }}">
                        <i class="nav-icon fab fa-cc-stripe"></i>
                        Stripe Disputes
                    </a>
                </li>
            @endcan

            @if ($logged_in_user->isAdmin())
                <li class="nav-title">
                    @lang('menus.backend.sidebar.system')
                </li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Route::is('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/log-viewer*'), 'open')
                }}">
                        <a class="nav-link nav-dropdown-toggle {{
                            active_class(Route::is('admin/log-viewer*'))
                        }}" href="#">
                        <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer'))
                        }}" href="{{ route('log-viewer::dashboard') }}">
                                @lang('menus.backend.log-viewer.dashboard')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer/logs*'))
                        }}" href="{{ route('log-viewer::logs.list') }}">
                                @lang('menus.backend.log-viewer.logs')
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
